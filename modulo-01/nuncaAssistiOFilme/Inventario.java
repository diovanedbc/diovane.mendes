import java.util.ArrayList;

public class Inventario extends Object{
    private ArrayList<Item> itens;
       
    public Inventario(){
        itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens(){
        return this.itens;
    }
    
    public void adicionar(Item item){
        itens.add(item);
    }
    
    public Item obterItem(int posicao){
        return this.itens.get(posicao);
    }
    
    public void remove(int posicao){
        itens.remove(posicao);
    }
    
    public String getDescricoesItens(){
        String descricao = "";
        
        for(int i=0; i<itens.size(); i++){
            if(itens.get(i) != null){
                if(i != 0) {
                    descricao += ",";
                }
                descricao += itens.get(i).getDescricao();
            }
        }
        
        return descricao;
    }
    
    public Item itemMaiorQuantidade(){
        Item umItem = new Item(0, "");
        
        for(Item item : itens){
            if(item.getQuantidade() > umItem.getQuantidade()){
                umItem = item;
            }
        }
        
        return umItem;
    }

    public Item buscaItemPorNome(String descricao){
        Item item = null;
        for(Item i : itens){
            if(descricao.equals(i.getDescricao())){
                item = i;
                break;
            }
        }
        return item;
    }

    public ArrayList<Item> inverter(){
        ArrayList<Item> listaInvertida = new ArrayList<>();

        for(int i=itens.size()-1; i>=0; i--){
            listaInvertida.add(itens.get(i));
        }
        return listaInvertida;
    }

    public void ordenarItens(){
        ArrayList<Item> listaOrdenadaAsc = new ArrayList<>();
        Item apoio = null;

        Item[] arrayOrdenadoAsc = new Item[itens.size()];

        for(int i=0; i<itens.size(); i++){
            arrayOrdenadoAsc[i] = itens.get(i);
        }

        for(int i=0; i<arrayOrdenadoAsc.length; i++){
            for(int j=0; j<arrayOrdenadoAsc.length; j++){
                if(arrayOrdenadoAsc[i].getQuantidade() < arrayOrdenadoAsc[j].getQuantidade()){
                    apoio = arrayOrdenadoAsc[i];
                    arrayOrdenadoAsc[i] = arrayOrdenadoAsc[j];
                    arrayOrdenadoAsc[j] = apoio;
                }
            }
        }

        for(int i=0; i<itens.size(); i++){
            listaOrdenadaAsc.add(arrayOrdenadoAsc[i]);
        }

        itens = listaOrdenadaAsc;
    }

    public void ordenarItens(TipoOrdenacao tipo){
        if(tipo == TipoOrdenacao.ASC){
            ordenarItens();
        }else{
            ArrayList<Item> listaOrdenadaDesc = new ArrayList<>();
            Item apoio = null;

            Item[] arrayOrdenadoDesc = new Item[itens.size()];

            for(int i=0; i<itens.size(); i++){
                arrayOrdenadoDesc[i] = itens.get(i);
            }

            for(int i=0; i<arrayOrdenadoDesc.length; i++){
                for(int j=0; j<arrayOrdenadoDesc.length; j++){
                    if(arrayOrdenadoDesc[i].getQuantidade() > arrayOrdenadoDesc[j].getQuantidade()){
                        apoio = arrayOrdenadoDesc[i];
                        arrayOrdenadoDesc[i] = arrayOrdenadoDesc[j];
                        arrayOrdenadoDesc[j] = apoio;
                    }
                }
            }

            for(int i=0; i<itens.size(); i++){
                listaOrdenadaDesc.add(arrayOrdenadoDesc[i]);
            }

            itens = listaOrdenadaDesc;
        }
    }

    public boolean constaItem(Item item){
        for(Item i : itens){
            if(i.equals(item))
                return true;
        }
        return false;
    }
}