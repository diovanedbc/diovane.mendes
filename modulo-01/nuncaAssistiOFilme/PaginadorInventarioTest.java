import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class PaginadorInventarioTest {

    @Test
    public void deveRetornarListaComItensPulados(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(1, "Sword"));
        inventario.adicionar(new Item(3, "Mastermind Shield"));
        inventario.adicionar(new Item(37, "Healing Potion"));
        inventario.adicionar(new Item(68, "Arrow"));

        PaginadorInventario paginadorInventario = new PaginadorInventario(inventario);
        paginadorInventario.pular(2);

        ArrayList<Item> listaPulada = new ArrayList<>();
        listaPulada.add(new Item(37, "Healing Potion"));
        listaPulada.add(new Item(68, "Arrow"));

//        assertEquals(listaPulada, paginadorInventario.lista);
    }

    @Test
    public void deveRetornarListaComItensLimitadosEPulados(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Sword");
        Item doisItem = new Item(3, "Mastermind Shield");
        Item tresItem = new Item(37, "Healing Potion");
        Item quatroItem = new Item(68, "Arrow");
        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);
        inventario.adicionar(quatroItem);

        PaginadorInventario paginadorInventario = new PaginadorInventario(inventario);

        paginadorInventario.pular(1);

        assertEquals(doisItem, paginadorInventario.limitar(2).get(0));
        assertEquals(tresItem, paginadorInventario.limitar(2).get(1));
    }
}
