import static org.junit.Assert.*;
import org.junit.Test;

public class ElfoDaLuzTest{
    @Test
    public void deveInicializarComEspadaDeGalvorn(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Níniel");
        Item item = new Item(1, "Espada de Galvorn");
        
        assertNotNull(elfoDaLuz.getInventario().constaItem(item));
    }
    
    @Test
    public void naoDevePerderEspadaDeGalvorn(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Celebrindal");
        
        elfoDaLuz.perderItem(new Item(1, "Espada de Galvorn"));
        
        assertNotNull(elfoDaLuz.getInventario().buscaItemPorNome("Espada de Galvorn"));
    }
    
    @Test
    public void devePerderItemANaoSerEspadaDeGalvorn(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Galadriel");
        
        elfoDaLuz.perderItem(new Item(1, "Escudo de latão"));
        
        assertNull(elfoDaLuz.getInventario().buscaItemPorNome("Escudo de latão"));
    }
    
    @Test
    public void quantidadeDeAtaqueIgualImparPerde21DeVida(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Lúthien");
        
        elfoDaLuz.atacarComEspada();
        
        assertEquals(79, elfoDaLuz.getVida(), 0.1);
    }
    
    @Test
    public void quantidadeDeAtaqueIgualParGanha10DeVida(){
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Lúthien");
        
        elfoDaLuz.atacarComEspada();
        elfoDaLuz.atacarComEspada();
        
        assertEquals(89, elfoDaLuz.getVida(), 0.1);
    }
}