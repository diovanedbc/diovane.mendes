import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Arrays;

public class Exercito{
    private TreeMap<Status,ArrayList<Elfo>> exercitoDeElfos = new TreeMap();
    
    public Exercito(){
        
    }
    
    public TreeMap getExercito(){
        return exercitoDeElfos;
    }
    
    public boolean verificaTipoElfo(Elfo elfo){
        return elfo.getClass().getName().equals("ElfoVerde") ||
                elfo.getClass().getName().equals("ElfoNoturno");
    }
    
    public boolean verificaStatusAdicionado(Elfo elfo){
        return exercitoDeElfos.containsKey(elfo.getStatus());
    }
    
    public void alistamento(Elfo elfo){
        if(verificaTipoElfo(elfo)){
            if(!verificaStatusAdicionado(elfo)){
                ArrayList<Elfo> listaElfo = new ArrayList();
                listaElfo.add(elfo);
                
                exercitoDeElfos.put(elfo.getStatus(), listaElfo);
            }else{
                exercitoDeElfos.get(elfo.status).add(elfo);
            }
        }
    }
    
    public ArrayList<Elfo> pesquisa(Status status){
        return exercitoDeElfos.get(status);
    }
}