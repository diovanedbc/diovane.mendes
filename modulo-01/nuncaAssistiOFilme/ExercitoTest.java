import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Arrays;

public class ExercitoTest{
    @Test
    public void deveAlistarElfoVerde(){
        Exercito exercito = new Exercito();
        ElfoVerde elfo = new ElfoVerde("Dirwen");       

        exercito.alistamento(elfo);
        
        assertTrue(exercito.pesquisa(Status.RECEM_CRIADO).contains(elfo));
    }
    
    @Test
    public void deveAlistarElfoNoturno(){
        Exercito exercito = new Exercito();
        ElfoNoturno elfo = new ElfoNoturno("Dirwen");       

        exercito.alistamento(elfo);
        
        assertTrue(exercito.pesquisa(Status.RECEM_CRIADO).contains(elfo));
    }
    
    @Test
    public void naoDeveAlistarElfoDaLuz(){
        Exercito exercito = new Exercito();
        ElfoDaLuz elfo = new ElfoDaLuz("Dirwen");       

        exercito.alistamento(elfo);
        
        assertTrue(exercito.getExercito().isEmpty());
    }
    
    @Test
    public void naoDeveAlistarElfo(){
        Exercito exercito = new Exercito();
        Elfo elfo = new Elfo("Dirwen");       

        exercito.alistamento(elfo);
        
        assertTrue(exercito.getExercito().isEmpty());
    }
    
    @Test
    public void deveRenderizarListaComDeterminadoStatus(){
        Exercito exercito = new Exercito();
        ElfoNoturno umElfo = new ElfoNoturno("Dirwen1");
        ElfoNoturno doisElfo = new ElfoNoturno("Dirwen2");
        ElfoNoturno tresElfo = new ElfoNoturno("Dirwen3");
        ElfoNoturno quatroElfo = new ElfoNoturno("Dirwen4");
        
        umElfo.setStatus(Status.MORTO);
        doisElfo.setStatus(Status.SOFREU_DANO);
        tresElfo.setStatus(Status.MORTO);
        
        exercito.alistamento(umElfo);
        exercito.alistamento(doisElfo);
        exercito.alistamento(tresElfo);
        exercito.alistamento(quatroElfo);
        
        assertEquals(Arrays.asList(umElfo,tresElfo), exercito.pesquisa(Status.MORTO));
    }
}
