public class ElfoVerde extends Elfo{
    public ElfoVerde(String nome){
        super(nome);
    }

    public boolean verificarItens(Item item){
        return item.getDescricao().equals("Espada de aço valiriano") ||
                item.getDescricao().equals("Arco de Vidro") ||
                item.getDescricao().equals("Flecha de Vidro");
    }

    public void ganharItem(Item item){
        if(verificarItens(item))
        this.inventario.adicionar(item);
    }

    public void perderItem(Item item){
        if(verificarItens(item)) {
            this.inventario.getItens().remove(item);
        }
    }

    protected void ganhaXp(){
        xp += 2;
    }
}
