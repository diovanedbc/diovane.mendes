public class Elfo extends Personagem{
    protected int xp = 0;
    private static int qtdElfos;
    
    public Elfo(String nome){
        super(nome);
        this.status = Status.RECEM_CRIADO;
        this.vida = 100;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        Elfo.qtdElfos++;
    }

    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }

    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }
    
    public int getXp(){
        return this.xp;
    }
    
    public Item getFlecha(){
        return this.inventario.getItens().get(1);
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
        
    public int getQuantidadeFlecha(){
        return getFlecha().getQuantidade();
    }

    public boolean podeAtirarFlecha(){
        return getQuantidadeFlecha() > 0;
    }
    
    public void atiraFlechaTresVezes(Dwarf dwarf){
        for(int i=0; i<3; i++){
            atirarFlecha(dwarf);
        }
    }
    
    public void atirarFlecha(Dwarf dwarf){
        if(podeAtirarFlecha()){
            getFlecha().setQuantidade(getQuantidadeFlecha() - 1);
            this.ganhaXp();
            dwarf.perderVida();
            this.perderVida();
        }
    }

    protected void ganhaXp(){
        xp++;
    }
    
    public String imprimirResumo(){
        return "D:";
    }
    
    public boolean equals(Object object){
        Elfo elfo = (Elfo)object;
        if(nome == elfo.nome && 
            status.equals(elfo.status) && 
            vida == elfo.vida)
            return true;
        return false;
    }
    
    @Override
    public String toString(){
        return "Nome: "+ nome + " Status: " + status;
    }
}