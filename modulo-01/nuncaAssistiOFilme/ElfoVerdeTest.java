import static org.junit.Assert.*;
import org.junit.Test;

public class ElfoVerdeTest {

    @Test
    public void naoDeveGanharItemComDeterminadaDescricao(){
        ElfoVerde elfoVerde = new ElfoVerde("Isciira");

        elfoVerde.ganharItem(new Item(5, "Escudo titanio"));

        assertNull( elfoVerde.getInventario().buscaItemPorNome("Escudo titanio"));
    }

    @Test
    public void deveGanharItemComDeterminadaDescricao(){
        ElfoVerde elfoVerde = new ElfoVerde("Fieryat");

        elfoVerde.ganharItem(new Item(5, "Arco de Vidro"));

        assertEquals("Arco de Vidro", elfoVerde.getInventario().buscaItemPorNome("Arco de Vidro").getDescricao());
    }

    @Test
    public void naoDevePerderItemComDeterminadaDescricao(){
        ElfoVerde elfoVerde = new ElfoVerde("Isciira");
        Item item = new Item(5, "Escudo titanio");

        elfoVerde.inventario.adicionar(item);
        elfoVerde.perderItem(new Item(5, "Escudo titanio"));

        assertTrue(elfoVerde.getInventario().constaItem(item));
    }

    @Test
    public void devePerderItemComDeterminadaDescricao(){
        ElfoVerde elfoVerde = new ElfoVerde("Fieryat");

        Item umItem = new Item(5, "Arco de Vidro");
        Item doisItem = new Item(89, "Arco de Vidro");

        elfoVerde.getInventario().adicionar(umItem);
        elfoVerde.getInventario().adicionar(doisItem);

        elfoVerde.perderItem(doisItem);

        assertFalse(elfoVerde.getInventario().constaItem(doisItem));
    }

    @Test
    public void deveGanharDoisDeXP(){
        ElfoVerde elfoVerde = new ElfoVerde("Fieryat");

        elfoVerde.atirarFlecha(new Dwarf("Otaerhyn"));

        assertEquals(2, elfoVerde.getXp());
    }

    @Test
    public void deveGanharSeisDeXPAtirandoTresFlechas(){
        ElfoVerde elfoVerde = new ElfoVerde("Fieryat");

        elfoVerde.atirarFlecha(new Dwarf("Otaerhyn"));

        assertEquals(2, elfoVerde.getXp());
    }

    @Test
    public void deveGanharQuatroDeXPAtirandoDuasFlechas(){
        ElfoVerde elfoVerde = new ElfoVerde("Fieryat");

        elfoVerde.atirarFlecha(new Dwarf("Otaerhyn"));
        elfoVerde.atirarFlecha(new Dwarf("Ilyrana"));

        assertEquals(4, elfoVerde.getXp());
    }
}
