public class Dwarf extends Personagem{
    private boolean equipado = false;
    
    public Dwarf(String nome){
        super(nome);
        this.vida = 110.0;
        this.inventario.adicionar(new Item(1, "Escudo"));
    }  
    
    public boolean podePerdeVida(){
        return this.vida > 0;
    }
    
    public void perderVida(){        
        if(podePerdeVida()){
            this.vida -= this.equipado ? 5.0 : 10.0;
            this.status = Status.SOFREU_DANO;
        }else{
            this.status = Status.MORTO;
        }
    }
    
    public boolean equiparEscudo(){
        return this.equipado = true;
    }
    
    public String imprimirResumo(){
        return "D:";
    }
}