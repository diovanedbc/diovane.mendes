import static org.junit.Assert.*;
import org.junit.Test;

public class DwarfTest{    
    @Test
    public void deveInicializarVidaComCentoEDez(){
        Dwarf umDwarf = new Dwarf("Reidmouk Aleminer");
        
        assertEquals(110.0, umDwarf.getVida(), 0.01);
    }
    
    @Test
    public void aoSerAtingidoDevePerderVida(){
        Dwarf umDwarf = new Dwarf("Thraggog Warbasher");
        
        umDwarf.perderVida();
        
        assertEquals(100.0, umDwarf.getVida(), 0.01);
    }
    
    @Test
    public void aoSerAtingidoDevePerderVidaDuasVezes(){
        Dwarf umDwarf = new Dwarf("Dolgrumir Silvertoe");
        
        umDwarf.perderVida();
        umDwarf.perderVida();
        
        assertEquals(90.0, umDwarf.getVida(), 0.01);
    }
    
    @Test
    public void aoSerAtingidoDevePerderVidaDozesVezes(){
        Dwarf umDwarf = new Dwarf("Dolgrumir Silvertoe");
        
        for(int i=0; i<12; i++) 
            umDwarf.perderVida();
        
        assertEquals(0.0, umDwarf.getVida(), 0.01);
    }
    
    @Test
    public void devePerderVidaENaoMorrer(){
        Dwarf dwarf = new Dwarf("Aleminggog Silvertoe");
        
        dwarf.perderVida();
        
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void deveMudarStatusComVidaMenorAUm(){
        Dwarf dwarf = new Dwarf("Aleminerumir Silvertoe");
        
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void deveRetornar95aoLevar3DanosComEscudo(){
        Dwarf dwarf = new Dwarf("Aleminerumir Orshabal");
        
        dwarf.equiparEscudo();
        dwarf.perderVida();
        dwarf.perderVida();
        dwarf.perderVida();
        
        assertEquals(95, dwarf.getVida(), 0.01);        
    }
}