public abstract class Personagem{
    protected String nome;
    protected Status status; 
    protected double vida;   
    protected int quantidadeAtaque = 0;
    protected Inventario inventario = new Inventario();
    
    public Personagem(String nome){
        this.nome = nome;
        this.status = Status.RECEM_CRIADO;
    }

    public String getNome(){
        return this.nome;
    } 

    public void setNome(String nome){
        this.nome = nome;
    }     
    
    public Status getStatus(){
        return this.status;
    }  
    
    public void setStatus(Status status){
        this.status = status;
    }
    
    public double getVida(){
        return this.vida;
    }
    
    public void setVida(double vida){
        this.vida = vida;
    }  
    
    public Inventario getInventario(){
        return this.inventario;
    } 

    public void setInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public void ganharItem(Item item){
        this.inventario.adicionar(item);
    }
    
    public void perderItem(Item item){
        this.inventario.getItens().remove(item);
    }
    
    protected void perderVida(){
        this.vida -= 0;
    }
    
    protected void ganharVida(){
    }
    
    public boolean verificarItens(Item item){
        return true;
    }
    
    public abstract String imprimirResumo();
}