import java.util.ArrayList;

public class PaginadorInventario {
    private Inventario inventario;
     ArrayList<Item> lista;

    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
        this.lista = this.inventario.getItens();
    }

    public void pular(int marcador){
        ArrayList<Item> listaPulada = new ArrayList<>();

        for(int i=marcador; i<lista.size(); i++){
            listaPulada.add(lista.get(i));
        }

        lista = listaPulada;
    }

    public ArrayList<Item> limitar(int marcador){
        ArrayList<Item> listaLimitada = new ArrayList<>();

        for(int i=0; i<marcador; i++){
            listaLimitada.add(lista.get(i));
        }
        return lista = listaLimitada;
    }
}
