import static org.junit.Assert.*;
import org.junit.Test;

public class DadoFalsoTest {
    @Test
    public void dadoFalsoDeveSortearNumeroInformado() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(5);
        assertEquals(5, dadoFalso.sortear());
    }
    
    @Test
    public void dadoFalsoSemSimularDeveSortear0() {
        DadoFalso dadoFalso = new DadoFalso();
        assertEquals(0, dadoFalso.sortear());
    }
}