
public class ElfoDaLuz extends Elfo{
    public ElfoDaLuz(String nome){
        super(nome);
        this.inventario.adicionar(new Item(1, "Espada de Galvorn"));
    }
    
    public boolean verificarItens(Item item){
        return item.getDescricao().equals("Espada de Galvorn");
    }
    
    public void perderItem(Item item){
        if(!verificarItens(item)) {
            this.inventario.getItens().remove(item);
        }
    }
    
    public void atacarComEspada(){
        this.quantidadeAtaque++;
        
        if(quantidadeAtaque%2 == 1){
            this.perderVida();
        }else{
            this.ganharVida();
        }
    }
    
    protected void perderVida(){
        this.vida -= 21;
    }
    
    protected void ganharVida(){
        this.vida += 10;
    }
}