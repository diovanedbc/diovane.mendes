public class Item extends Object{
    private int quantidade;
    private String descricao;
    
    public Item(int quantidade, String descricao){
        this.quantidade= quantidade;
        this.descricao = descricao;
    }
    
    public int getQuantidade(){
        return this.quantidade;
    }
    
    public String getDescricao(){
        return this.descricao;
    }
    
    public void setQuantidade(int quantidade){
        this.quantidade = quantidade;
    }
    
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    
    @Override
    public String toString(){
        return "Quantidade: " + this.quantidade + 
                " Descricao: " + this.descricao;
    }
    
    public boolean equals(Object object){
        Item item = (Item)object;
        if(quantidade == item.quantidade && descricao.equals(item.descricao))
            return true;
        return false;
    }
}