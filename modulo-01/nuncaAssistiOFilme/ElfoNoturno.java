public class ElfoNoturno extends Elfo{
    public ElfoNoturno(String nome){
        super(nome);
    }

    protected void ganhaXp(){
        xp += 3;
    }
    
    protected void perderVida(){
        this.vida -= 15;
    }
}
