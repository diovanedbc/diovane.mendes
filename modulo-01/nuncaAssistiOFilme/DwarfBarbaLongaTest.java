import static org.junit.Assert.*;
import org.junit.Test;

public class DwarfBarbaLongaTest {
    private final double DELTA = 1e-1;

    @Test
    public void dwarfDevePerderVida66Percent() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(4);
        Dwarf dwarf = new DwarfBarbaLonga("Balin", dadoFalso);
        dwarf.perderVida();
        assertEquals(100.0, dwarf.getVida(), 0.1);
    }

    @Test
    public void dwarfNaoDevePerderVida33Percent() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(5);
        Dwarf dwarf = new DwarfBarbaLonga("Balin", dadoFalso);
        dwarf.perderVida();
        assertEquals(110.0, dwarf.getVida(), 0.1);
    }
}