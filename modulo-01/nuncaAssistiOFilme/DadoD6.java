import java.util.Random;

public class DadoD6 implements Sorteador {
    private Random random = new Random();
    private static final int NUMERO_FACES = 6;
    
    public void setSeed(int seed) {
        this.random.setSeed(seed);
    }
    
    public int sortear() {
        Random random = new Random();
        return random.nextInt(6) + 1;
    }
}