import static org.junit.Assert.*;
import org.junit.Test;

public class ElfoNoturnoTest {

    @Test
    public void deveRetornarTresDeXP(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Kaylessa");

        elfoNoturno.atirarFlecha(new Dwarf("Qildor"));

        assertEquals(3, elfoNoturno.getXp());
    }

    @Test
    public void deveRetornarSeisDeXPAtirandoDuasFlechas(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Kaylessa");

        elfoNoturno.atirarFlecha(new Dwarf("Qildor"));
        elfoNoturno.atirarFlecha(new Dwarf("Nopos"));

        assertEquals(6, elfoNoturno.getXp());
    }

    @Test
    public void devePerderQuinzeDeVida(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Molostroi");

        elfoNoturno.atirarFlecha(new Dwarf("TroiLops"));

        assertEquals(85, elfoNoturno.getVida(), 0.1);
    }

    @Test
    public void devePerderQuinzeDeVidaAoAtirarFlecha(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Molostroi");

        elfoNoturno.atirarFlecha(new Dwarf("Raibyr"));

        assertEquals(85, elfoNoturno.getVida(), 0.1);
    }
}
