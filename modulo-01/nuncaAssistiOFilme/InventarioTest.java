import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest{    
    @Test
    public void deveAdicionarItem(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Arco");
        
        inventario.adicionar(umItem);
        
        assertEquals(umItem, inventario.getItens().get(0));
    }
    
    @Test
    public void deveRetornarItemDaPosicaoSolicitada(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        
        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        
        assertEquals(doisItem, inventario.obterItem(1));
    }
    
    @Test
    public void deveRemoverItemDaPosicaoSolicitada(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        
        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.remove(1);
        
        assertEquals(1, inventario.getItens().size());
    }
    
    @Test
    public void deveRetornarDescricaoDosItens(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        
        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        
        assertEquals("Potion,Rune", inventario.getDescricoesItens());
    }
    
    @Test
    public void deveRetornarItemComMaiorQuantidade(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor");
        
        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);
        
        assertEquals(doisItem, inventario.itemMaiorQuantidade());
    }

    @Test
    public void deveRetornarItemPelaDescricao(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor");

        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);

        assertEquals(doisItem, inventario.buscaItemPorNome("Rune"));
    }

    @Test
    public void deveRetornarListaComOrdemInvertidaSemAlterarOriginal(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor");

        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);

        ArrayList<Item> listaInvertida = new ArrayList<>();
        listaInvertida.add(tresItem);
        listaInvertida.add(doisItem);
        listaInvertida.add(umItem);

        ArrayList<Item> listaOriginal = new ArrayList<>();
        listaOriginal.add(umItem);
        listaOriginal.add(doisItem);
        listaOriginal.add(tresItem);

        assertEquals(listaInvertida, inventario.inverter());
        assertEquals(listaOriginal, inventario.getItens());
    }

    @Test
    public void deveRetornarInventarioOrdenadoMenorAoMaiorPeloValorQuantidadeDoItem(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor");
        
        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);

        inventario.ordenarItens();

        assertEquals(umItem, inventario.getItens().get(0));
        assertEquals(tresItem, inventario.getItens().get(1));
        assertEquals(doisItem, inventario.getItens().get(2));
    }

    @Test
    public void deveRetornarInventarioOrdenadoMaiorAoMenorPeloValorQuantidadeDoItem(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor"); 
        
        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);

        inventario.ordenarItens(TipoOrdenacao.DESC);

        assertEquals(doisItem, inventario.getItens().get(0));
        assertEquals(tresItem, inventario.getItens().get(1));
        assertEquals(umItem, inventario.getItens().get(2));
    }

    @Test
    public void deveRetornarTrueComItemNoInventario(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");

        inventario.adicionar(umItem);

        assertTrue(inventario.constaItem(umItem));
    }

    @Test
    public void deveRetornarFalseComItemSemEstarNoInventario(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(1, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor");

        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);

        assertFalse(inventario.constaItem(umItem));
    }
}