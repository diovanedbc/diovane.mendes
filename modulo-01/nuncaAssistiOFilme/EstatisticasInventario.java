public class EstatisticasInventario {
    private Inventario inventario;

    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }

    public double calcularMedia(){
        double media = 0;

        for(Item i : inventario.getItens()){
            media += i.getQuantidade();
        }

        return media/inventario.getItens().size();
    }

    public double calcularMediana(){
        int tamanhoInventario = inventario.getItens().size();
        double mediana = 0;

        if(tamanhoInventario%2 == 0){
            mediana = inventario.getItens().get(tamanhoInventario/2).getQuantidade() +
                    inventario.getItens().get(tamanhoInventario/2-1).getQuantidade();

            mediana = mediana/2;
        }else{
            mediana = inventario.getItens().get((int) Math.ceil(tamanhoInventario/2)).getQuantidade();
        }

        return mediana;
    }

    public int qtdItensAcimaDaMedia(){
        int contador = 0;

        for(Item i : inventario.getItens()){
            if(i.getQuantidade() > calcularMedia()){
                contador++;
            }
        }
        return contador;
    }
}
