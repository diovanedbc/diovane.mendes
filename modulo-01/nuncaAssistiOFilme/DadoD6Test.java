import static org.junit.Assert.*;
import org.junit.Test;

public class DadoD6Test {
    @Test
    public void jogarDoisDadosComMesmaSeedGeraMesmoNumero() {
        DadoD6 umDado = new DadoD6();
        umDado.setSeed(3);

        DadoD6 doisDado = new DadoD6();
        doisDado.setSeed(3);

        assertEquals(umDado.sortear(), doisDado.sortear());
        assertEquals(umDado.sortear(), doisDado.sortear());
        assertEquals(umDado.sortear(), doisDado.sortear());
        assertEquals(umDado.sortear(), doisDado.sortear());
        assertEquals(umDado.sortear(), doisDado.sortear());
        assertEquals(umDado.sortear(), doisDado.sortear());
        assertEquals(umDado.sortear(), doisDado.sortear());
    }
    
    @Test
    public void jogarDoisDadosComSeedsDiferentesGeraNumerosDiferentes() {
        DadoD6 umDado = new DadoD6();
        umDado.setSeed(3);

        DadoD6 doisDado = new DadoD6();
        doisDado.setSeed(3000);

        assertNotEquals(umDado.sortear(), doisDado.sortear());
        assertNotEquals(umDado.sortear(), doisDado.sortear());
        assertNotEquals(umDado.sortear(), doisDado.sortear());
        assertNotEquals(umDado.sortear(), doisDado.sortear());
        assertNotEquals(umDado.sortear(), doisDado.sortear());
        assertNotEquals(umDado.sortear(), doisDado.sortear());
        assertNotEquals(umDado.sortear(), doisDado.sortear());
    }
}