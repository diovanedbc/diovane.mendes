import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class EstatisticasInventarioTest {
    @Test
    public void deveRetornarMediaDeQuantidadeDeItens(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(2, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor");

        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);

        assertEquals(5.66, estatisticasInventario.calcularMedia(), 1);
    }

    @Test
    public void deveRetornarMedianaDeQuantidadeDeItensQuandoTamanhoForPar(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(2, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(1, "Armor");
        Item quatroItem = new Item(5, "Bots");

        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);
        inventario.adicionar(quatroItem);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);

        assertEquals(5, estatisticasInventario.calcularMediana(), 1);
    }

    @Test
    public void deveRetornarMedianaDeQuantidadeDeItensQuandoTamanhoForImpar(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(2, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(6, "Armor");

        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);

        assertEquals(9, estatisticasInventario.calcularMediana(), 1);
    }

    @Test
    public void deveRetornarQuantidadeDeItensAcimaDaMediaDeQuantidades(){
        Inventario inventario = new Inventario();
        Item umItem = new Item(2, "Potion");
        Item doisItem = new Item(9, "Rune");
        Item tresItem = new Item(1, "Armor");
        Item quatroItem = new Item(5, "Bots");
        Item cincoItem = new Item(11, "Helmet");

        inventario.adicionar(umItem);
        inventario.adicionar(doisItem);
        inventario.adicionar(tresItem);
        inventario.adicionar(quatroItem);
        inventario.adicionar(cincoItem);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);

        assertEquals(2, estatisticasInventario.qtdItensAcimaDaMedia());
    }
}
