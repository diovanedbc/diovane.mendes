import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Test;

public class ElfoTest{
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentaXp(){
        // Arrange
        Elfo umElfo = new Elfo("Totodaio");
        // Act
        umElfo.atirarFlecha(new Dwarf("Valulir Giantmane"));
        // Assert
        assertEquals(1, umElfo.getXp());
        assertEquals(1, umElfo.getQuantidadeFlecha());
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentaXp(){
        Elfo umElfo = new Elfo("Totodaio");

        umElfo.atiraFlechaTresVezes(new Dwarf("Hofus Noblemace"));

        assertEquals(2, umElfo.getXp());
        assertEquals(0, umElfo.getQuantidadeFlecha());
    }
    
    @Test
    public void elfoDeveInicializarComDuasFlechas(){
        Elfo umElfo = new Elfo("Totodaio");
        
        assertEquals(2, umElfo.getQuantidadeFlecha());
    }

    @Test
    public void elfoDeveNascerComStatusRecemCriado(){
        Elfo elfo = new Elfo("Novin");

        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }

    @Test
    public void elfoDeveNascerComVidaCem(){
        Elfo elfo = new Elfo("Novin");

        assertEquals(100.0, elfo.getVida(), 0.1);
    }    
    
    public void criarUmElfoIncrementaContadorUmaVez() {
        new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }

    @Test
    public void criarDoisElfosIncrementaContadorDuasVezes() {
        new Elfo("Legolas");
        new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }
 
    @Test
    public void naoCriarElfoNaoIncrementaContador() {
        new Dwarf("Balin");
        assertEquals(0, Elfo.getQtdElfos());        
    }
}
