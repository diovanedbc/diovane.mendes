import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Arrays;

public class AgendaContatosTest{
    @Test
    public void deveAdicionarContato(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionarContato("Alfredo", 995087090L);
        
        assertFalse(agenda.getLista().isEmpty());
    }
    
    @Test
    public void deveObterTelefonePeloNome(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionarContato("Alfredo", 995087090L);
        agenda.adicionarContato("Maria", 985087090L);
        agenda.adicionarContato("Eduarda", 975087090L);
        agenda.adicionarContato("Diogo", 965087090L);
        
        assertEquals(new Long(985087090), agenda.telefonePeloNome("Maria"));
    }
    
    @Test
    public void deveObterNomePeloTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionarContato("Alfredo", 995087090L);
        agenda.adicionarContato("Maria", 985087090L);
        agenda.adicionarContato("Eduarda", 975087090L);
        agenda.adicionarContato("Diogo", 965087090L);
        
        assertEquals("Eduarda", agenda.nomePeloTelefone(new Long(975087090L)));
    }
    
    @Test
    public void deveObterListaEmFormatoCSVOrdenadoPeloNome(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionarContato("Alfredo", 995087090L);
        agenda.adicionarContato("Maria", 985087090L);
        agenda.adicionarContato("Eduarda", 975087090L);
        agenda.adicionarContato("Diogo", 965087090L);
        
        String resultado = "Alfredo;995087090\n" +
                            "Diogo;965087090\n" +
                            "Eduarda;975087090\n" +
                            "Maria;985087090\n";                         ;
        
        assertEquals(resultado, agenda.csv());
    }
}