import java.util.TreeMap;
import java.util.Set;

public class AgendaContatos{
    private String nome;
    private Long telefone;
    private TreeMap<String,Long> lista;
    
    public AgendaContatos(){
        this.lista = new TreeMap();
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public Long getTelefone(){
        return this.telefone;
    }
    
    public void setTelefone(Long telefone){
        this.telefone = telefone;
    }
    
    public TreeMap<String,Long> getLista(){
        return this.lista;
    }
    
    public void adicionarContato(String nome, Long telefone){
        this.lista.put(nome, telefone);
    }
    
    public Long telefonePeloNome(String nome){        
        for (String chave : chaves()){
            if(chave.equals(nome)){
                return this.lista.get(chave);
            }
        }
        return null;
    }
    
    public String nomePeloTelefone(Long telefone){          
        for (String chave : chaves()){
            if(this.lista.get(chave).equals(telefone)){
                return chave;
            }
        }
        return "";
    }
    
    public String csv(){     
        String resultado = "";        
        for (String chave : chaves()){
            resultado += chave+";"+this.lista.get(chave)+"\n";
        }
        return resultado;
    }
    
    public Set<String> chaves(){
        return this.lista.keySet();
    }
}