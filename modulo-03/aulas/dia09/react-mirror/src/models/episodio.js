export default class Episodio{
  constructor( nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido ){
    this.nome = nome
    this.duracao = duracao
    this.temporada = temporada
    this.ordemEpisodio = ordemEpisodio
    this.thumbUrl = thumbUrl
    this.qtdVezesAssistido = qtdVezesAssistido || 0
  }

  get _duracao(){
    return `Duração: ${ this.duracao }min`
  }

  get temporadaEpisodio(){
    const temporada = this.temporada
    const ordemEpisodio = this.ordemEpisodio
    return `T${ temporada >=10 ?  temporada : temporada.toString().padStart(2, '0') }:Ep${ ordemEpisodio >=10 ? ordemEpisodio  : ordemEpisodio.toString().padStart(2, '0') }` 
  }

  avaliar( nota ){
    this.nota = parseInt( nota )
    this.assistido = true
  }

  validarNota( nota ){
    nota = parseInt( nota )
    return nota >= 1 && nota <= 5
  }
}