import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './mensagemFlash.css'

export default class MensagemFlash extends Component{ 
  constructor( props ){
    super( props )
    this.idsTimeouts = []
    this.animacao = ''
  }

  fechar = () => {
    this.props.atualizarMensagem( false )
  }

  componentDidUpdate( prevProps ){
    const { deveExibirMensagem, segundos } = this.props

    if( prevProps.deveExibirMensagem !== deveExibirMensagem ){
      this.idsTimeouts.forEach( clearTimeout )

      const novoIdTimeout = setTimeout( () => {
        this.fechar()
      }, segundos * 1000)

      this.idsTimeouts.push( novoIdTimeout )
    }
  }

  render(){
    const { cor, deveExibirMensagem, mensagem} = this.props
    if ( this.animacao || deveExibirMensagem ) {
      this.animacao = deveExibirMensagem ? 'fade-in' : 'fade-out'
    }
    return <span onClick={ this.fechar } className={ `flash ${ cor } ${ this.animacao }` }> { mensagem } </span>
  }
}

MensagemFlash.propTypes = {
  atualizarMensagem: PropTypes.func.isRequired,
  cor: PropTypes.string,
  deveExibirMensagem: PropTypes.bool.isRequired,
  mensagem: PropTypes.string.isRequired,
  segundos: PropTypes.number
}

MensagemFlash.defaultProps = {
  cor: 'verde',
  segundos: 3
}