import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './meuInputNumero.css'

export default class MeuInputNumero extends Component{
  
  perderFoco = evt => {
    if( this.props.obrigatorio && !evt.target.value ){
      this.props.atualizarErro( true )
    }else{
      this.props.atualizarErro( false )
    }
  }

  render(){
    const { titulo, placeholder, visivel, deveExibirErro } = this.props

    return(
      <div>
        {
          titulo && <span> { titulo } </span>
        }
        {
          visivel && <input className={ `${ visivel } centralizarTexto` } type="number" placeholder={ placeholder } onBlur={ this.perderFoco } />
        }
        {
          deveExibirErro && <span className="mensagem-erro"> * Obrigatório </span>
        }
      </div>
    )
  }
}

MeuInputNumero.propType = {
  titulo: PropTypes.string,
  placeholder: PropTypes.string,
  visivel: PropTypes.bool.isRequired,
  obrigatorio: PropTypes.bool,
  atualizarErro: PropTypes.func.isRequired,
  deveExibirErro: PropTypes.bool.isRequired
}

MeuInputNumero.defaulProps = {
  obrigatorio: false
}
