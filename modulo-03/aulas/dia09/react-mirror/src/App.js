import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUI from './components/episodioUI'
import MensagemFlash from './components/shared/mensagemFlash'
import MeuInputNumero from './components/shared/meuInputNumero';

class App extends Component{

  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      exibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio,
      exibirMensagem: false
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }
    
  geraCampoDeNota = () => {
    if(this.state.episodio.qtdVezesAssistido > 0){
      return(
        <div>
          <span> Qual a sua nota para este episódio? </span>
          <input type='number' placeholder='1 a 5' onBlur={ this.registrarNota }/>
        </div>
      )
    }
    return undefined
  }

  exibeMensagem = ( mensagem, cor, segundos ) => {
    this.setState({
      exibirMensagem: true,
      mensagem: mensagem,
      cor: cor,
      segundos: segundos
    })
  }

  registrarNota = evt => {
    const { episodio } = this.state

    if( episodio.validarNota( evt.target.value ) ){

      episodio.avaliar( evt.target.value )
      this.exibeMensagem( 'Nota registrada com sucesso!', undefined, 2 )
      
    }else{

      this.exibeMensagem( 'Informar uma nota válida (entre 1 e 5)', 'vermelho', 5 )

    }
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  atualizarErro = deveExibirErro => {
    this.setState({
      deveExibirErro
    })
  }
 
  render(){
    const { episodio, exibirMensagem, mensagem, cor, segundos, deveExibirErro } = this.state
    
    return (
      <div className='App'>
        <header className='App-header'>

        <MensagemFlash
          atualizarMensagem={ this.atualizarMensagem }
          deveExibirMensagem={ exibirMensagem } 
          mensagem={ mensagem }
          cor={ cor }
          segundos={ segundos }
        />

        <EpisodioUI episodio={ episodio } />

        <MeuInputNumero 
          placeholder={ '1 a 5' }
          titulo={ 'Qual a sua nota para este episódio?' }
          obrigatorio={ true }
          atualizarErro={ this.atualizarErro }
          deveExibirErro={ deveExibirErro }
          visivel={ episodio.qtdVezesAssistido }
        />
        
        <button onClick={ this.sortear }> Próximo </button>

        { episodio.qtdVezesAssistido >= 10 ? '' : <button onClick={ this.marcarComoAssistido }> Já assisti! </button> }
        </header>
      </div>
    );
  }
}

export default App;
