import React, { Component } from 'react'

export default class EpisodioUI extends Component{
  render(){
    const { episodio } = this.props

    return (
      <>
        <h2> { episodio.nome } </h2>
        <span> { episodio._duracao } </span>
        <span> { episodio.temporadaEpisodio } </span>
        <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
        <span> { episodio.assistido ? `Assistido ${episodio.qtdVezesAssistido} vez(es)` : 'Não assistido'}</span>
      </>
    )
  }
}