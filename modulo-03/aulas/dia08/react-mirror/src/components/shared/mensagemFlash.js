import React, { Component } from 'react'
import './mensagemFlash.css'

export default class MensagemFlash extends Component{ 
  fechar = () => {
    this.props.atualizarMensagem( false )
  }

  tempoAlerta( tempo ){
    setTimeout( () => {
      this.props.atualizarMensagem( false )
    }, tempo * 1000)
  }

  render(){
    const { cor, deveExibirMensagem, mensagem, segundos } = this.props
    return <span onClick={ this.fechar } onBlurCapture={ this.tempoAlerta( segundos ) } className={ `flash ${ cor || 'verde' } ${ deveExibirMensagem ? '' : 'invisivel' }` }> { mensagem } </span>
  }
}