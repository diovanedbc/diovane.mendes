import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUI from './components/episodioUI'
import MensagemFlash from './components/shared/mensagemFlash'

class App extends Component{

  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      notaRegistrada: false,
      notaInvalida: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }
    
  geraCampoDeNota = () => {
    if(this.state.episodio.qtdVezesAssistido > 0){
      return(
        <div>
          <span> Qual a sua nota para este episódio? </span>
          <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) }/>
        </div>
      )
    }
    return undefined
  }

  registrarNota = evt => {
    const { episodio } = this.state
    if( evt.target.value < 0 || evt.target.value > 5 ){

      this.setState( {
        episodio, notaInvalida: true
      })
      


    }else{
      episodio.avaliar( evt.target.value )

      this.setState( {
        episodio, notaRegistrada: true
      })
      // setTimeout( () => {
      //   this.setState( {
      //     deveExibirMensagem: false
      //   } )
      // }, 5000 ) 
    }
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      notaRegistrada: devoExibir,
      notaInvalida: devoExibir
    })
  }
 
  render(){
    const { episodio, notaRegistrada, notaInvalida } = this.state
    
    return (
      <div className="App">
        <header className="App-header">
        <MensagemFlash 
          atualizarMensagem={ this.atualizarMensagem } 
          cor='' 
          deveExibirMensagem={ notaRegistrada } 
          mensagem="Nota registrada com sucesso!" 
          segundos={ 3 }
        />

        <MensagemFlash 
          atualizarMensagem={ this.atualizarMensagem } 
          cor='vermelho' 
          deveExibirMensagem={ notaInvalida } 
          mensagem="Informar uma nota válida (entre 1 e 5)"
          segundos={ 5 } 
        />

        <EpisodioUI episodio={ episodio } />
        { this.geraCampoDeNota() }
        
        <button onClick={ this.sortear.bind( this ) }> Próximo </button>
        { episodio.qtdVezesAssistido >= 10 ? '' : <button onClick={ this.marcarComoAssistido.bind( this ) }> Já assisti! </button> }
        </header>
      </div>
    );
  }
}

export default App;
