//Exercicio 3

let somaPares = (array) =>{
    let resultado = 0;
    for (let i = 0; i < array.length; i++) {
        if(i%2 === 0)
            resultado += array[i];
    }
    return resultado;
}

console.log(somaPares([1, 56, 4.34, 6, -2]));

