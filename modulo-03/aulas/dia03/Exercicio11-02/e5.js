// Exercicio 05

let valorFracao = (valor) =>{
    let fracao = valor.toString().split(".",valor)[1]
    
    if(fracao.toString().length > 2){
        fracao = (parseInt((parseInt(fracao) / 100) + 1)).toString()
        if(parseInt(fracao) < 10){
            fracao = "0" + fracao
        }
    }
    
    return fracao
}

let novoValor = (inteiro, fracao) =>{
    let novoValor = ""
    let count = 0

    for(let i = inteiro.length - 1 ; i >=0 ; i-- ){
        if(count === 3){
            count = 0
            novoValor = inteiro.charAt(i) + "." + novoValor
        }else{
            novoValor = inteiro.charAt(i) + novoValor
        }
        count++
    }    

    return novoValor += "," + fracao
}





let imprimirBRL = (valor) =>{
    let cifra = "R$ "
    if(valor === 0){
        return "R$ 0,00"
    }else if(valor < 0){
        valor *= -1
        cifra = "-R$ "
    }
    
    let inteiro = parseInt(valor).toString()

    let fracao = valorFracao(valor)

    return cifra + novoValor(inteiro, fracao)
}


let valor0 = 0
let valor1 = 3498.99
let valor2 = 2313477.0135
let valor3 = -2313477.0135

console.log(imprimirBRL(valor0))
console.log(imprimirBRL(valor1))
console.log(imprimirBRL(valor2))
console.log(imprimirBRL(valor3))