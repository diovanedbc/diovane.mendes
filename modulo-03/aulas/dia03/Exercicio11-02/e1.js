//Exercicio 01
let parametros = {
    raio:2,
    tipo:'A'
}

let  calcularCirculo = (objeto) => {
    if(objeto.tipo==='A'){
        return 3.14 * (objeto.raio*objeto.raio) 
    }

    if(objeto.tipo==='C'){
        return 3.14 * (objeto.raio*2)
    }
}

console.log(calcularCirculo(parametros));