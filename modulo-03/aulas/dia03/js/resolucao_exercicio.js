var moedas = ( function() {

  // o que é privado, vai aqui
  function imprimirMoeda( params ) {

    function arredondar(numero, precisao = 2) {
      const fator = Math.pow( 10, precisao )
      return Math.ceil( numero * fator ) / fator
    }
  
    const {
      numero,
      qtdCasasMilhares,
      separadorDecimal,
      separadorMilhar,
      colocarMoeda,
      colocarNegativo
    } = params
  
    var stringBuffer = []
    var parteDecimal = arredondar(Math.abs(numero)%1)
    var parteInteira = Math.trunc(numero)
    var parteInteiraString = Math.abs(parteInteira).toString()
    var parteInteiraTamanho = parteInteiraString.length
  
    var c = 1
    while (parteInteiraString.length > 0) {
        if ( c % qtdCasasMilhares === 0 ) {
            stringBuffer.push( `${ separadorMilhar }${ parteInteiraString.slice( parteInteiraTamanho - c ) }` )
            parteInteiraString = parteInteiraString.slice( 0, parteInteiraTamanho - c )
        } else if ( parteInteiraString.length < qtdCasasMilhares ) {
            stringBuffer.push( parteInteiraString )
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push( parteInteiraString )
  
    var decimalStr = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    const numeroFormatado = `${ stringBuffer.reverse().join('') }${ separadorDecimal }${ decimalStr }`
    return parteInteira >= 0 ? colocarMoeda( numeroFormatado ) : colocarNegativo( colocarMoeda( numeroFormatado ) )
  }

  // o que é público, vem aqui
  return {
    imprimirBRL: numero =>
      imprimirMoeda({
        numero,
        qtdCasasMilhares: 3,
        separadorMilhar: '.',
        separadorDecimal: ',',
        colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
        colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
      }),
    imprimirGBP: numero =>
      imprimirMoeda({
        numero,
        qtdCasasMilhares: 3,
        separadorMilhar: ',',
        separadorDecimal: '.',
        colocarMoeda: numeroFormatado => `£ ${ numeroFormatado }`,
        colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
      }),
    imprimirFR: numero =>
      imprimirMoeda({
        numero,
        qtdCasasMilhares: 3,
        separadorMilhar: '.',
        separadorDecimal: ',',
        // numeroFormatado => "3.498,99"
        colocarMoeda: numeroFormatado => `${ numeroFormatado } €`,
        // numeroFormatado => "3.498,99 €"
        colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
      })
  }

})()

console.log(moedas.imprimirBRL(0)) // "R$ 0,00"
console.log(moedas.imprimirBRL(3498.99)) // “R$ 3.498,99”
console.log(moedas.imprimirBRL(-3498.99)) // “-R$ 3.498,99”
console.log(moedas.imprimirBRL(2313477.0135)) // “R$ 2.313.477,02”

console.log(moedas.imprimirGBP(0)) // "£ 0.00"
console.log(moedas.imprimirGBP(3498.99)) // “£ 3,498.99”
console.log(moedas.imprimirGBP(-3498.99)) // “-£ 3,498.99”
console.log(moedas.imprimirGBP(2313477.0135)) // “£ 2,313,477.02”

console.log(moedas.imprimirFR(0))
console.log(moedas.imprimirFR(3498.99))
console.log(moedas.imprimirFR(-3498.99))
console.log(moedas.imprimirFR(2313477.0135))