import axios from 'axios'
import ListaEpisodios from './listaEpisodios'

export default class EpisodiosApi {
  buscar() {
    return axios.get( 'http://localhost:9000/api/episodios' )
  }
  
  registrarNota( { nota, episodioId } ) {
    return axios.post( 'http://localhost:9000/api/notas', { nota, episodioId } )
  }
  
  // buscarNotaPorIdEp( episodioId ) {
  //   return axios.get( `http://localhost:9000/api/notas?episodioId=${ episodioId }`)
  // }

  buscarTodasNotas(){
    return axios.get( 'http://localhost:9000/api/notas' )
  }

  buscarDetalhes(){
    return axios.get( 'http://localhost:9000/api/detalhes' )
  }

  buscarEpisodiosComNotas() {
    return new Promise( resolve => {
      const requisicoes = [ this.buscarTodasNotas(), this.buscar() ]
      Promise.all( requisicoes ).then( resultados => {
        const todasNotas = resultados[ 0 ].data
        const listaEpisodios = new ListaEpisodios( resultados[ 1 ].data )
        const episodiosNotas = listaEpisodios.retornaTodos.map( e => ( { episodio: e, nota: (todasNotas.find(n => n.episodioId === e.id) || {}).nota } ) )
        resolve( episodiosNotas )
      } )
    } )
  }

}

// this.episodiosApi.buscarTodasNotas()
//       .then(n => {
//         this.setState({
//           notas: n.data
//         })
//       })

//     this.episodiosApi.buscar()
//       .then(episodiosDoServidor => {
//         this.listaEpisodios = new ListaEpisodios(episodiosDoServidor.data)

//         let episodiosNotas = this.listaEpisodios.retornaTodos.map(e => {
//           return {
//             episodio: e,
//             nota: this.state.notas.filter(n => n.episodioId === e.id)
//               .map(n => n.nota)[0]
//           }
//         })

//         this.setState({
//           lista: episodiosNotas
//         })
//       })