import Episodio from "./episodio"

function sortear(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

export default class ListaEpisodios{
    constructor( episodiosDoServidor ){
      this.todos = episodiosDoServidor.map( e => new Episodio( e.id, e.nome, e.duracao, e.temporada, e.ordemEpisodio, e.thumbUrl, e.qtdVezesAssistido ))
    }

    get avaliados() {
      return this.todos.filter( e => e.nota )
    }
    
    get episodioAleatorio(){
      const indice = sortear( 0, this.todos.length )
      return this.todos[ indice ]
    }

    get retornaTodos(){
      return this.todos
    }

    marcarComoAssistido( episodio ){
      const episodioParaMarcar = this.todos.find( e => e.nome === episodio.nome )
      episodioParaMarcar.assistido = true
      episodioParaMarcar.qtdVezesAssistido++
    }
}