import {BrowserRouter as Router, Route} from 'react-router-dom';
import React, { Component } from 'react';
import './App.css';
import PaginaInicial from './components/paginaInicial';
import ListaAvaliacoes from './components/listaAvaliacoes';
import TelaDetalheEpisodio from './components/telaDetalheEpisodio'
import TodosEpisodios from './components/todosEpisodios';

class App extends Component{

  constructor( props ){
    super( props )
    this.state = {
      exibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }  
 
  render(){    
    return (
      <div className='App'>
        <Router>
          <>
            <header className='App-header'>
              <Route exact path="/" component={ PaginaInicial }/>
              <Route exact path="/avaliacoes" component={ ListaAvaliacoes }/>
              <Route exact path="/episodios" component={ TodosEpisodios }/>
              <Route exact path="/episodio/:id" component={ TelaDetalheEpisodio }/>
            </header>
          </>
        </Router>
      </div>
    );
  }
}

export default App;

