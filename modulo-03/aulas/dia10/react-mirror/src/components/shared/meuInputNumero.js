import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './meuInputNumero.css'

export default class MeuInputNumero extends Component{
  
  perderFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props
    const valor = evt.target.value
    const erro = obrigatorio && !valor
    atualizarValor( { valor, erro } )
  }

  mostraInput = () => {
    const { placeholder, visivel, nota } = this.props
    if( typeof nota === 'undefined' ){
      return <input type="number" className={ `centralizarTexto ${ visivel  ? 'erro' : '' }` } placeholder={ placeholder } onBlur={ this.perderFoco } />
    }
    return nota
  }
  
  render(){
    const { titulo, visivel, deveExibirErro} = this.props

    return visivel ? (
      <div>
        {
          titulo && <span> { titulo } </span>
        }
        { 
          this.mostraInput()
        }
        {
          deveExibirErro && <span className="mensagem-erro"> * Obrigatório </span>
        }
      </div>
    ) : null
  }
}

MeuInputNumero.propTypes = {
  titulo: PropTypes.string,
  placeholder: PropTypes.string,
  visivel: PropTypes.bool.isRequired,
  obrigatorio: PropTypes.bool,
  atualizarValor: PropTypes.func.isRequired,
}

MeuInputNumero.defaulProps = {
  obrigatorio: false
}
