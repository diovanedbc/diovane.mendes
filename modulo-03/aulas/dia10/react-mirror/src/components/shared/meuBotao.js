import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import './meuBotao.css'

export default class MeuBotao extends Component{

  validaRota = () =>{
    const { texto, rota, dadosNavegacao } = this.props
    if( typeof rota === 'undefined' ){
      return texto
    }
    return <Link to={{ pathname: rota, state: dadosNavegacao }}> { texto } </Link>
  }

  render(){
    const { cor, quandoClicar} = this.props
    return(
      <>
        {
          quandoClicar && <button className={ `botao ${ cor }` } onClick={ quandoClicar }>             
              { this.validaRota() }
          </button>
        }
      </>
    )
  }
}

MeuBotao.propTypes = {
  texto: PropTypes.string.isRequired,
  dadosNavegacao: PropTypes.object,
  quandoClicar: PropTypes.func.isRequired,
  cor: PropTypes.oneOf( [ 'verde', 'azul', 'vermelho', 'laranja' ] ),
  rota: PropTypes.string
}

MeuBotao.defaultProps = {
  cor: 'verde'
}