import { Component } from 'react'
import EpisodiosApi from '../models/episodiosApi'
import ListaEpisodios from '../models/listaEpisodios'

export default class EpisodioNota extends Component{
  constructor(props){
    super(props)
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      lista: this.pqp,
      notas: []
    }
  }

  
  componentDidMount() {
    this.episodiosApi.buscarTodasNotas()
    .then(n => {
      this.setState({
        notas: n.data
      })
    })
    
    this.episodiosApi.buscar()
    .then(episodiosDoServidor => {
      this.listaEpisodios = new ListaEpisodios(episodiosDoServidor.data)
      
      this.pqp = this.listaEpisodios.retornaTodos.map(e => {
        return {
          episodio: e,
          nota: this.state.notas.filter(n => n.episodioId === e.id)
          .map(n => n.nota)[0]
        }
      })
      
      this.setState({
        lista: this.pqp
      })
    })
  }
}