import React, { Component } from 'react'
import EpisodioUI from './episodioUI'
import MeuBotao from './shared/meuBotao'
import MensagemFlash from './shared/mensagemFlash'
import MeuInputNumero from './shared/meuInputNumero'
import ListaEpisodios from '../models/listaEpisodios'
import EpisodiosApi from '../models/episodiosApi'

export default class PaginaInicial extends Component{
  constructor( props ){
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      exibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  componentDidMount() {
    this.episodiosApi.buscar()
      .then( episodiosDoServidor => {
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor.data )
        setTimeout( () => {
          this.setState( { episodio: this.listaEpisodios.episodioAleatorio } )
        }, 1500 )
      } )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio,
      exibirMensagem: false
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  exibeMensagem = ( { mensagem, cor, segundos } ) => {
    this.setState({
      exibirMensagem: true,
      mensagem,
      cor,
      segundos
    })
  }

  registrarNota = ( { valor, erro } ) => {
    this.setState( {
      deveExibirErro: erro
    })

    if ( erro ) {
      return;
    } 
    
    const { episodio } = this.state
    let cor, mensagem, segundos

    if ( episodio.validarNota( valor ) ) {
      episodio.avaliar( valor ).then( () => {
        mensagem = 'Registramos sua nota!'
        cor = 'verde'
        segundos = 2
        this.exibeMensagem( { mensagem, cor, segundos } )   
      })
    } else {
      mensagem = 'Informar uma nota válida (entre 1 e 5)'
      cor = 'vermelho'
      segundos = 5
      this.exibeMensagem( { mensagem, cor, segundos } ) 
    }   
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render(){
    const { episodio, exibirMensagem, mensagem, cor, segundos, deveExibirErro } = this.state

    return(
      !episodio ? (
        <h3>Aguarde...</h3>
      ) : (
        <>
          <MensagemFlash
            atualizarMensagem={ this.atualizarMensagem }
            cor={ cor }
            deveExibirMensagem={ exibirMensagem } 
            mensagem={ mensagem }
            segundos={ segundos }
          />

          <EpisodioUI episodio={ episodio } />

          <MeuInputNumero 
            placeholder={ '1 a 5' }
            titulo={ 'Nota para este episódio:' }
            obrigatorio={ true }
            atualizarValor={ this.registrarNota }
            deveExibirErro={ deveExibirErro }
            visivel={ episodio.assistido || false}
            nota={ episodio.nota }
          />

          <div>
            <MeuBotao
              texto={ 'Próximo' }
              quandoClicar={ this.sortear }
            />

            <MeuBotao
              texto={ 'Já Assisti' }
              quandoClicar={ episodio.qtdVezesAssistido < 10 ? this.marcarComoAssistido : null }
              cor={ 'azul' }
            />
          </div>
          <div>
            <MeuBotao
              texto={ 'Ver notas' }
              quandoClicar={ () =>{} }
              cor={ 'vermelho' }
              rota={ '/avaliacoes' }
            />

            <MeuBotao
              texto={ 'Todos episódios' }
              quandoClicar={ () => {} }
              cor={ 'laranja' }
              rota={ '/episodios' }
            />
          </div>
        </>
      )
    )
  }
}