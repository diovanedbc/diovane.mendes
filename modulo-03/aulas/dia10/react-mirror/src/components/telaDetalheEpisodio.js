import React, { Component } from 'react'
import EpisodiosApi from '../models/episodiosApi'
import Episodio from '../models/episodio'
import MeuBotao from './shared/meuBotao'
import Moment from 'react-moment';
import Card from 'react-bootstrap/Card'

export default class TelaDetalheEpisodio extends Component {
  constructor(props) {
    super(props)
    this.episodiosApi = new EpisodiosApi()
    //this.idUrl = parseInt( window.location.pathname.match( /([0-9])+/ )[0] )
    this.idUrl = parseInt( this.props.match.params.id )
    this.state = {
      detalhe: {}
    }
  }

  componentDidMount() {
    this.episodiosApi.buscarDetalhes()
      .then( detalhes => {
        this.setState({
          detalhe: detalhes.data.find( d => d.episodioId === this.idUrl )
        })
      })
  }

  buscaEpisodio = () => {
    const lista = this.props.location.state.lista    
    const objeto = lista.find( e => e.episodio.id === this.idUrl )
       
    return {
      episodio: new Episodio( objeto.episodio.id, objeto.episodio.nome, objeto.episodio.duracao,
         objeto.episodio.temporada, objeto.episodio.ordemEpisodio, objeto.episodio.thumbUrl ),
      nota: objeto.nota
    }
  }
  
  render() {
    const { detalhe } = this.state
    return (
      <>
        <Card style={{ width: '30rem', margin: '20px'}}>
          <Card.Img variant="top" src={ this.buscaEpisodio().episodio.thumbUrl } />
          <li> Nome: { this.buscaEpisodio().episodio.nome } </li>
          <li> { this.buscaEpisodio().episodio.temporadaEpisodio } </li>
          <li> Duração: { this.buscaEpisodio().episodio.duracao } </li>
          <li> Sinopse: </li>
          <Card.Text> { detalhe.sinopse } </Card.Text>
          <li>Data estreia: 
            <Moment format=" DD/MM/YYYY"> 
              { detalhe.dataEstreia }
            </Moment>
          </li>
          <li>Nota: { this.buscaEpisodio().nota } </li>
          <li>Nota IMB: { detalhe.notaImdb/2 } </li>
        </Card>

        <MeuBotao
          texto={ 'Voltar' }
          quandoClicar={ () => {} }
          cor={ 'azul' }
          rota={ '/avaliacoes' }
        />
      </>
    )
  }
}