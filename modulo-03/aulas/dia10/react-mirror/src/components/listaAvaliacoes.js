import React, { Component } from 'react'
import EpisodiosApi from '../models/episodiosApi'
import MeuBotao from './shared/meuBotao'
import './listaAvaliacoes.css'

export default class ListaAvaliacoes extends Component {
  constructor(props) {
    super(props)
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      lista: [],
      notas: []
    }
  }

  componentDidMount() {
    this.episodiosApi.buscarEpisodiosComNotas().then( lista => {
      this.setState( {
        lista
      } )
    } )
  }

  toSort = (a, b) => {
    return a < b ? -1 : a > b ? 1 : 0
  }

  ordenamento = () => {
    return this.state.lista.filter(e => typeof e.nota !== 'undefined')
      .sort((a, b) => {
        return this.toSort(a.episodio.ordemEpisodio, b.episodio.ordemEpisodio)
      })
      .sort((a, b) => {
        return this.toSort(a.episodio.temporada, b.episodio.temporada)
      })
  }

  imprimirLista = () => {
    return this.ordenamento().map(e =>
      <li key={e.episodio.id}>
        {`${e.episodio.temporadaEpisodio} -> ${e.episodio.nome} - ${e.nota}`}

        <MeuBotao
          texto={'Detalhes'}
          quandoClicar={() => { }}
          dadosNavegacao={ this.state }
          cor={'verde'}
          rota={`/episodio/${e.episodio.id}`}
        />
      </li>
    )
  }

  render() {
    return (
      <>
        <div className={`alinhamento`}>
          {this.imprimirLista()}
        </div>

        <MeuBotao
          texto={'Página inicial'}
          quandoClicar={() => { }}
          cor={'azul'}
          rota={'/'}
        />
      </>
    )
  }
}