import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';

class App extends Component{

  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      exibirMensagemSucesso: false
    }
  }

  sortear(){
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio
    })
  }

  marcarComoAssistido(){
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }
    
  geraCampoDeNota(){
    // se devo exibir campo na tela
    // exibo
    if(this.state.episodio.qtdVezesAssistido > 0){
      return(
        <div>
          <span> Qual a sua nota para este episódio? </span>
          <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) }/>
        </div>
      )
    }
    return undefined
  }

  registrarNota( evt ){
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState({
      episodio,
      exibirMensagemSucesso: true
    })
    setTimeout(() => {
      this.setState({
        exibirMensagemSucesso: false
      })
    }, 5000)
  } 

  mostraMensagemRegistroNota(){
    return this.state.exibirMensagemSucesso && (
      <div>
        <span> { this.state.episodio.nota ? 'Nota registrada com sucesso!' : 'Sem nota!' } </span>
      </div>
    )
  }
 
  render(){
    const { episodio } = this.state
    
    return (
      <div className="App">
        <header className="App-header">
        <h2> { episodio.nome } </h2>
        <span> { episodio._duracao } </span>
        <span> { episodio.temporadaEpisodio } </span>
        <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
        <span> { episodio.assistido ? `Assistido ${episodio.qtdVezesAssistido} vez(es)` : 'Não assistido'}</span>
        { this.geraCampoDeNota() }
        {/* <span> { episodio.nota ? 'Nota registrada com sucesso!' : 'Sem nota!'} </span> */}
        { this.mostraMensagemRegistroNota() }
        <button onClick={ this.sortear.bind( this ) }> Próximo </button>
        { episodio.qtdVezesAssistido >= 10 ? '' : <button onClick={ this.marcarComoAssistido.bind( this ) }> Já assisti! </button> }
        </header>
      </div>
    );
  }
}

export default App;
