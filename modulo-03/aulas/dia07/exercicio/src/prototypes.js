String.prototype.revertePalavra = function(){
  return this.split(' ').length > 0 ? this.split(' ').reverse().join(' ') : this
}

Array.prototype.ordenaPorUltimaPalavra = function(){
  return this.map( s => s.revertePalavra() )
            .sort()
            .map( s => s.revertePalavra() )
}

Array.prototype.invalidas = function(){
  let anoAtual = new Date().getFullYear()

  return `Séries Inválidas: ${this.map( s => {
                              for (let i in s) {
                                if ( s.hasOwnProperty(i) ) {
                                  if( s[i]===undefined || s[i]===null || s.anoEstreia > anoAtual ){
                                    return s.titulo
                                  }                    
                                }
                              } 
                            }).filter( s => typeof s!= 'undefined' ) }`.replace(",", " - ")
}

Array.prototype.filtrarPorAno = function( ano ){
  return this.filter( s => s.anoEstreia >= ano )
}

Array.prototype.procurarPorNome = function( nome ){
  let consta = false
  this.forEach( s => {
      if( s.elenco.includes( nome ) ){
        consta = true 
        return
      }
    })
    return consta
}

Array.prototype.mediaDeEpisodios = function(){
  return this.map( s => s.numeroEpisodios)
              .reduce( (a, b) => {
                return a + b
              }) / this.length
}

Array.prototype.totalSalarios = function( indice ){
  let valorDiretores = this[ indice ].diretor.length * 100000
  let valorElenco = this[ indice ].elenco.length * 40000

  return valorDiretores + valorElenco
}

Array.prototype.queroGenero = function( tipo ){
  return this.filter( s => s.genero.includes( tipo ) )
}

Array.prototype.queroTitulo = function( palavra ){
  return this.filter( s => s.titulo.split(' ').includes( palavra ) )
            .map( s => s.titulo)
}

Array.prototype.creditos = function(){
  return this.map( s => {
    return {
        Titulo: s.titulo,
        Diretores: s.diretor.ordenaPorUltimaPalavra(),
        Elenco: s.elenco.ordenaPorUltimaPalavra()
      }
    }).sort( ( a, b ) => {
      return a.Titulo < b.Titulo ? -1 : a.Titulo > b.Titulo ? 1 : 0;
    })
}

//Westworld
Array.prototype.nomesAbreviados = function(){
   let listasElenco = this.map( s => s.elenco )
  
  // for (let i = 0; i < listasElenco.length; i++) {
  //   let acumulador = 0

  //   for (let j = 0; j < listasElenco[i].length; j++) {
  //     listasElenco[i][j].match( /([A-Z])\.+/g ) ? acumulador++ : ''
  //   }

  //   if( listasElenco[i].length === acumulador ){
  //     return listasElenco[i]
  //   }   
  // }

  let oi =(elenco)=>{
    for (let i = 0; i < elenco.length; i++) {
      let acumulador = 0

      for (let j = 0; j < elenco[i].length; j++) {
        elenco[i][j].match( /([A-Z])\.+/g ) ? acumulador++ : ''
      }

      if( elenco[i].length === acumulador ){
        return elenco[i]
      }   
    }
  }
}