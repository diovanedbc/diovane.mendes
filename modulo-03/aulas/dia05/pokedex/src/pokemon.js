class Pokemon {
  constructor(objVindoDaApi){
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height
    this._peso = objVindoDaApi.weight
    this._tipos = objVindoDaApi.types
  }

  // pokemon.altura
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes#Prototype_methods
  get altura(){
    // transformar altura para cm (originalmente vem em dezenas de cm)
    return this._altura * 10
  }

  get peso(){
    return this._peso / 1000
  }

  get tipos(){
    let arr = []

    this._tipos.forEach(element => {
      
    });

    // for(lista in this._tipos){
    //   for(tipos in lista.type){
    //     arr.append(tipos.name)
    //   }
    // }

    return arr;
  }
}