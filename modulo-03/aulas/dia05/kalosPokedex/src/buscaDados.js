/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

class BuscaRetorna {
  dadosPokemon() {
    return document.getElementById( 'dadosPokemon' )
  }

  static texto( classe, valor ) {
    dadosPokemon.querySelector( classe ).innerText = valor
  }

  static imagem( classe, valor ) {
    dadosPokemon.querySelector( classe ).src = valor
  }

  static html( classe, valor ) {
    dadosPokemon.querySelector( classe ).innerHTML = valor
  }

  static input( classe, valor ) {
    dadosPokemon.querySelector( classe ).value = valor
  }
}
