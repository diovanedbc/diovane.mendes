/* eslint-disable no-unused-vars */
class PokeApi {
  buscar( id ) {
    const fazRequisicao = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` )
    return fazRequisicao.then( resultadoEmString => resultadoEmString.json() )
  }
}
