/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
class Renderiza {
  constructor( id ) {
    this._id = id
    this._pokemon = ''
  }

  // eslint-disable-next-line getter-return
  get newPoke() {
    const pokeApi = new PokeApi()
    // eslint-disable-next-line no-undef
    if ( Validacao.validacaoDoInput( this._id ) ) {
      pokeApi.buscar( this._id )
        .then( pokemonServidor => {
          const poke = new Pokemon( pokemonServidor )
          this._pokemon = poke
          this.renderizaDadosPokemon()
        } )
    }
  }

  renderizaDadosPokemon() {
    BuscaRetorna.texto( '.nome', this._pokemon.nome.toUpperCase() )

    BuscaRetorna.imagem( '.foto', this._pokemon.thumbUrl )

    BuscaRetorna.texto( '.id', this._pokemon.id )

    BuscaRetorna.texto( '.altura', ` ${ this._pokemon.altura } cm` )

    BuscaRetorna.texto( '.peso', ` ${ this._pokemon.peso } Kg` )

    // eslint-disable-next-line no-use-before-define
    BuscaRetorna.html( '.tipos', this._pokemon.tipos.map( t => `<li> ${ t.toUpperCase() } </li>` ).join('') )

    // eslint-disable-next-line no-use-before-define
    BuscaRetorna.html( '.estatisticas', this._pokemon.estatisticas.map( t => `<li> ${ t.nome } : ${ t.percentual } </li>` ).join('') )

    BuscaRetorna.input( '.input-id', `${ this._id }` )
  }
}
