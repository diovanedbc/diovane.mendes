//Registra evento quando carrega os elementos do HTML em memória
let executaIssoQuandoCarregar = function(){
  let h1Titulo = document.getElementById('titulo')
  h1Titulo.innerText = "Oi, eu sou goku!"
}

document.addEventListener('DOMContentLoaded', executaIssoQuandoCarregar)


//Usando classe
let oi = new Jedi('Diovane')

console.log(oi.atacarComSabre());
