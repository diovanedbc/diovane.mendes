package br.com.dbc.Integracao;

import br.com.dbc.Controller.ContratacaoController;
import br.com.dbc.CoworkingApplicationTests;
import br.com.dbc.DTO.ClienteDTO;
import br.com.dbc.DTO.ContratacaoDTO;
import br.com.dbc.DTO.EspacoDTO;
import br.com.dbc.Entity.Contratacao;
import br.com.dbc.Entity.Enuns.TipoContratacao;
import br.com.dbc.Factory.ContratacaoFactory;
import br.com.dbc.JsonString;
import br.com.dbc.Service.ContratacaoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ContratacaoTest extends CoworkingApplicationTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ContratacaoService contratacaoService;

    @Autowired
    private ContratacaoController contratacaoController;

    private ContratacaoDTO contratacaoDTO = new ContratacaoDTO();

    @Before
    public void setUp(){

        EspacoDTO espacoDTO = new EspacoDTO();
        espacoDTO.setId(1);
        espacoDTO.setNome("Arena");
        espacoDTO.setQuantidadePessoas(3);
        espacoDTO.setValor(200);

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setId(1);
        clienteDTO.setNome("Green Valley");
        clienteDTO.setCpf("039085421");
        clienteDTO.setDataNascimento( LocalDate.of(2009, 12, 15) );

        contratacaoDTO.setId(1);
        contratacaoDTO.setEspaco( espacoDTO );
        contratacaoDTO.setCliente( clienteDTO );
        contratacaoDTO.setTipoContratacao( TipoContratacao.DIARIA );
        contratacaoDTO.setQuantidade(3);
        contratacaoDTO.setDesconto(50);
        contratacaoDTO.setPrazoEmDias(18);
        contratacaoDTO.calculoValorCobrado();

        MockitoAnnotations.initMocks(this);

        this.mvc = MockMvcBuilders
                .standaloneSetup( contratacaoController )
                .build();

    }

    @Test
    public void status201AoSalvar() throws Exception{
        this.mvc.perform( MockMvcRequestBuilders
                    .post("/api/contratacao/salvar" )
                    .contentType( MediaType.APPLICATION_JSON )
                    .content( JsonString.asJsonString(contratacaoDTO) ) )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isCreated() );
    }

    @Test
    public void status200AoBuscarUm() throws Exception{
        Contratacao contratacao = ContratacaoFactory.getInstance( contratacaoDTO );

        when( contratacaoService.buscarUm(1) ).thenReturn( contratacao );

        this.mvc.perform( MockMvcRequestBuilders
                    .get("/api/contratacao/{id}", 1) )
                .andExpect(jsonPath("$.valorCobrado").value(550))
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isOk() );
    }

    @Test
    public void status200AoBuscarTodos() throws Exception{
        this.mvc.perform( MockMvcRequestBuilders
                    .get("/api/contratacao/") )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isOk() );
    }

    @Test
    public void status200AoAtualizar() throws Exception{
        // NÃO FUNCIONA!!!
        Contratacao contratacao = ContratacaoFactory.getInstance( contratacaoDTO );
        contratacao.setId(1);

        when( contratacaoService.buscarUm(1) ).thenReturn( contratacao );

        this.mvc.perform( MockMvcRequestBuilders
                    .put("/api/contratacao/atualizar/{id}", 1)
                    .contentType( MediaType.APPLICATION_JSON_UTF8 )
                    .content( JsonString.asJsonString(  contratacaoDTO ) ) )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isOk() );

//        User user = new User(1, "Arya Stark");
//        when(userService.findById(user.getId())).thenReturn(user);
//        doNothing().when(userService).update(user);
//        mockMvc.perform(
//                put("/users/{id}", user.getId())
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(asJsonString(user)))
//                .andExpect(status().isOk());
//        verify(userService, times(1)).findById(user.getId());
//        verify(userService, times(1)).update(user);
//        verifyNoMoreInteractions(userService);
    }
}
