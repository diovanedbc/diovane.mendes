package br.com.dbc.Repository;

import br.com.dbc.Entity.Contratacao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
public class ContratacaoRepositoryTest {

    @MockBean
    private ContratacaoRepository contratacaoRepository;

    private List<Contratacao> contratacoes;

    private Contratacao contratacao;

    @Before
    public void setUp(){
        Contratacao contratacaoUm = new Contratacao();
        contratacaoUm.setQuantidade(3);
        Contratacao contratacaoDois = new Contratacao();
        contratacaoUm.setQuantidade(5);

        contratacoes = Arrays.asList( contratacaoUm, contratacaoDois );
        contratacao = contratacaoUm;

        contratacaoRepository.save(contratacaoUm);
        contratacaoRepository.save(contratacaoDois);

        Mockito.when( contratacaoRepository.findAll() ).thenReturn( contratacoes );
        Mockito.when( contratacaoRepository.findById(1) ). thenReturn( contratacao );
    }

    @Test
    public void deveRetornarListaDeContratacoes(){
        assertThat( contratacaoRepository.findAll() ).isEqualTo( contratacoes );
    }

    @Test
    public void deveRetornarContratacao(){
        assertThat( contratacaoRepository.findById(1) ).isEqualTo( contratacao );
    }
}
