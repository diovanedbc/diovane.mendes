package br.com.dbc.Component;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class AcessoComponentTest {

    private AcessoComponent acessoComponent;

    @Before
    public void setUp(){
        acessoComponent = new AcessoComponent();
    }

    @Test
    public void deveRetornarDataHoraFormatadaBR(){
        LocalDateTime dataHora = LocalDateTime.of(2019,11, 5, 9,36);

        String dataHoraFormatada = acessoComponent.formatarDataHora(dataHora);

        assertThat("05/11/2019 09:36", equalTo(dataHoraFormatada) );
    }

    @Test
    public void deveRetornarLocalDateTimeQuandoPassadoDataEmString(){
        String dataHora = "28/02/2006 11:21";
        LocalDateTime dataHoraEsperada = LocalDateTime.of(2006,2,28,11,21);

        LocalDateTime dataHoraTransformada = acessoComponent.stringToLocalDateTime(dataHora);

        assertThat( dataHoraEsperada, equalTo(dataHoraTransformada) );
    }

}
