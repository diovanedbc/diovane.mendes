package br.com.dbc.Integracao;

import br.com.dbc.Controller.PagamentoController;
import br.com.dbc.CoworkingApplicationTests;
import br.com.dbc.DTO.*;
import br.com.dbc.Entity.Enuns.TipoContratacao;
import br.com.dbc.Entity.Enuns.TipoPagamento;
import br.com.dbc.Entity.Pagamento;
import br.com.dbc.Factory.PagamentoFactory;
import br.com.dbc.JsonString;
import br.com.dbc.Service.PagamentoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PagamentoTest extends CoworkingApplicationTests {

    private MockMvc mvc;

    @MockBean
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoController pagamentoController;

    private final PagamentoDTO pagamentoDTO = new PagamentoDTO();

    @Before
    public void setUp(){
        ClienteDTO cliente = new ClienteDTO();
        cliente.setId(1);
        cliente.setNome("Alfredo");
        cliente.setCpf("03707804515");
        cliente.setDataNascimento( LocalDate.of(1993, 11, 11) );

        TipoContatoDTO tipoContato = new TipoContatoDTO();
        tipoContato.setId(1);
        tipoContato.setNome("email");

        ContatoDTO contato = new ContatoDTO();
        contato.setId(1);
        contato.setTipoContato( tipoContato );
        contato.setValor("tudovaidarcerto@gmail.com");
        contato.setCliente( cliente );

        PacoteDTO pacote = new PacoteDTO();
        pacote.setId(1);
        pacote.setValor(315);

        EspacoDTO espaco = new EspacoDTO();
        espaco.setId(1);
        espaco.setNome("Arena");
        espaco.setQuantidadePessoas(27);
        espaco.setValor(150);

        EspacoPacoteDTO espacoPacote = new EspacoPacoteDTO();
        espacoPacote.setId(1);
        espacoPacote.setEspaco( espaco );
        espacoPacote.setPacote( pacote );
        espacoPacote.setTipoContratacao( TipoContratacao.HORA );
        espacoPacote.setQuantidade(5);
        espacoPacote.setPrazoEmDias(3);

        ClientePacoteDTO clientePacote = new ClientePacoteDTO();
        clientePacote.setId(1);
        clientePacote.setCliente( cliente );
        clientePacote.setPacote( pacote );
        clientePacote.setQuantidadePacote(5);

        pagamentoDTO.setId(1);
        pagamentoDTO.setClientePacote( clientePacote );
        pagamentoDTO.setTipoPagamento( TipoPagamento.DINHEIRO );

        MockitoAnnotations.initMocks(this);

        this.mvc = MockMvcBuilders
                .standaloneSetup( pagamentoController )
                .build();
    }

    @Test
    public void status201AoSalvar() throws Exception {

        this.mvc.perform( MockMvcRequestBuilders
                    .post("/api/pagamento/salvar" )
                        .contentType( MediaType.APPLICATION_JSON )
                        .content( JsonString.asJsonString(pagamentoDTO) ) )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isCreated() );
    }

    @Test
    public void status200AoBuscarUm() throws Exception{
        Pagamento pagamento = PagamentoFactory.getInstance( pagamentoDTO );

        when( pagamentoService.buscarUm(1) ).thenReturn( pagamento );

        this.mvc.perform( MockMvcRequestBuilders
                    .get("/api/pagamento/{id}", 1) )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isOk() );
    }

    @Test
    public void status200AoAtualizar() throws Exception{
        // NÃO FUNCIONA!!!
        Pagamento pagamento = PagamentoFactory.getInstance( pagamentoDTO );
        pagamento.setId(1);

        Mockito.when( pagamentoService.buscarUm(1) ).thenReturn( pagamento );

        this.mvc.perform( MockMvcRequestBuilders
                .put("/api/pagamento/atualizar/{id}", 1)
                .contentType( MediaType.APPLICATION_JSON_UTF8 )
                .content( JsonString.asJsonString(  pagamentoDTO ) ) )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isOk() );

//        User user = new User(1, "Arya Stark");
//        when(userService.findById(user.getId())).thenReturn(user);
//        doNothing().when(userService).update(user);
//        mockMvc.perform(
//                put("/users/{id}", user.getId())
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(asJsonString(user)))
//                .andExpect(status().isOk());
//        verify(userService, times(1)).findById(user.getId());
//        verify(userService, times(1)).update(user);
//        verifyNoMoreInteractions(userService);
    }
}
