package br.com.dbc.DTO;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ContratacaoDTOTest {

    private ContratacaoDTO contratacaoDTO;

    @Before
    public void setUp(){
        contratacaoDTO = new ContratacaoDTO();
        EspacoDTO espacoDTO = new EspacoDTO();
        espacoDTO.setValor(2500);

        contratacaoDTO.setQuantidade(3);
        contratacaoDTO.setDesconto(500);
        contratacaoDTO.setEspaco(espacoDTO);
    }

    @Test
    public void deveGerarValorCobradoDe7000(){
        contratacaoDTO.calculoValorCobrado();

        assertEquals(7000, contratacaoDTO.getValorCobrado(), 1);
    }
}
