package br.com.dbc.Service;

import br.com.dbc.Entity.Usuario;
import br.com.dbc.Repository.UsuarioRepository;
import br.com.dbc.Security.MD5Crypt;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsuarioServiceTest {

    @Mock
    private UsuarioRepository usuarioRepository;

    @Mock
    private MD5Crypt md5Crypt;

    private UsuarioService usuarioService;

    private Usuario usuario;

    @Before
    public void setUp(){
        usuarioService = new UsuarioService(usuarioRepository);

        usuario = new Usuario("Alfredo", "alfredo@gmail.com", "fredo", "59.com");
    }

    @Test
    public void deveSalvarSenhaCriptografadaEmMD5(){
        when( md5Crypt.encode("59.com") ).thenReturn("ed7deb671e31d09ef1c9529f49b50caf");

        usuarioService.salvar(usuario);

        assertThat( usuario.getSenha(), equalTo("ed7deb671e31d09ef1c9529f49b50caf") );

        verify(usuarioRepository).save(usuario);
    }

    @Test
    public void deveRetornarMensagemDeSucesso(){
        String mensagemSucesso = usuarioService.salvar(usuario);

        assertThat( "Usuario salvo com sucesso!", equalTo(mensagemSucesso) );
    }

    @Test
    public void deveRetornarMensagemDeFracasso(){
        String mensagemFracasso = usuarioService.salvar(null);

        assertThat( "Usuario não registrado no banco!", equalTo(mensagemFracasso) );
    }
}
