package br.com.dbc.Entity;

import javax.persistence.*;

@Entity
@Table(name = "TIPO_CONTATOS")
public class TipoContato {

    @Id
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @OneToOne(mappedBy = "tipoContato")
    private Contato contato;

    public TipoContato() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }
}
