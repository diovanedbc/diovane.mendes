package br.com.dbc.DTO;

import br.com.dbc.Entity.Enuns.TipoPagamento;

public class PagamentoDTO {

    private long id;

    private ClientePacoteDTO clientePacote;

    private ContratacaoDTO contratacao;

    private TipoPagamento tipoPagamento;

    public PagamentoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClientePacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
