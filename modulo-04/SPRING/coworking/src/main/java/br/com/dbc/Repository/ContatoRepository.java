package br.com.dbc.Repository;

import br.com.dbc.Entity.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Long> {

    Contato findById(long id);
    List<Contato> findAll();

}
