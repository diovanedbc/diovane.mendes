package br.com.dbc.Controller;

import br.com.dbc.DTO.EspacoDTO;
import br.com.dbc.Entity.Espaco;
import br.com.dbc.Factory.EspacoFactory;
import br.com.dbc.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController implements PadraoController<EspacoDTO, Espaco>{

    @Autowired
    private EspacoService espacoService;

    public EspacoDTO salvar(Espaco espaco) {
        return EspacoFactory.getInstance( espacoService.salvar(espaco) );
    }

    public EspacoDTO buscarUm(long id) {
        return EspacoFactory.getInstance( espacoService.buscarUm(id) );
    }

    public List<EspacoDTO> buscarTodos() {
        return espacoService.buscarTodos()
                .stream()
                .map(EspacoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public EspacoDTO atualizar(long id, Espaco espaco) {
        return EspacoFactory.getInstance( espacoService.atualizar(id, espaco) );
    }

    public void excluir(long id) {
        espacoService.excluir(id);
    }
}
