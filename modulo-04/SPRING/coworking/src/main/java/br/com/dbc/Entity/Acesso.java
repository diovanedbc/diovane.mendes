package br.com.dbc.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.time.format.DateTimeFormatter.ofPattern;

@Entity
@Table(name = "ACESSOS")
public class Acesso {

    @Id
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade=CascadeType.ALL)
    private SaldoCliente saldoCliente;

    @Column(name = "IS_ENTRADA", nullable = false)
    private Boolean isEntrada = false;

    @Column(name = "DATA", nullable = false)
    private LocalDateTime dataHora;

    public Acesso() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getIsEntrada() {
        return isEntrada;
    }

    public void setIsEntrada(Boolean isEntrada) {
        this.isEntrada = isEntrada;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }
}
