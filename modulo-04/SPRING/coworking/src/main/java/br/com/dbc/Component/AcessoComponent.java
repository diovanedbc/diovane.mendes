package br.com.dbc.Component;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

import static java.time.format.DateTimeFormatter.ofPattern;

@Component
public class AcessoComponent {

    public String formatarDataHora(LocalDateTime dataHora){
        return Optional
                .ofNullable(dataHora)
                .map(data -> data.format(ofPattern("dd/MM/yyyy HH:mm")))
                .orElse("");
    }

    public LocalDateTime stringToLocalDateTime(String dataHora){
        return Optional
                .ofNullable(dataHora)
                .map(data -> LocalDateTime.parse( data, ofPattern("dd/MM/yyyy HH:mm") ))
                .orElse(null);
    }
}
