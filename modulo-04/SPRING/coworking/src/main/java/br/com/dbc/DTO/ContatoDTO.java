package br.com.dbc.DTO;

public class ContatoDTO {

    private long id;

    private TipoContatoDTO tipoContato;

    private String valor;

    private ClienteDTO cliente;

    public ContatoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TipoContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }
}
