package br.com.dbc.Service;

import br.com.dbc.Entity.ClientePacote;
import br.com.dbc.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientePacoteService implements PadraoService<ClientePacote>{

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    public ClientePacote salvar(ClientePacote clientePacote) {
        return clientePacoteRepository.save(clientePacote);
    }

    public ClientePacote buscarUm(long id) {
        return clientePacoteRepository.findById(id);
    }

    public List<ClientePacote> buscarTodos() {
        return clientePacoteRepository.findAll();
    }

    public ClientePacote atualizar(long id, ClientePacote clientePacote) {
        clientePacote.setId(id);
        return salvar(clientePacote);
    }

    public void excluir(long id) {
        clientePacoteRepository.deleteById(id);
    }
}
