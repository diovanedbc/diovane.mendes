package br.com.dbc.Controller;

import br.com.dbc.DTO.AcessoDTO;
import br.com.dbc.Entity.Acesso;
import br.com.dbc.Factory.AcessoFactory;
import br.com.dbc.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping("/salvar")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public String salvar(@RequestBody Acesso acesso) {
        return acessoService.salvar(acesso);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public AcessoDTO buscarUm(@PathVariable long id) {
        return AcessoFactory.getInstance( acessoService.buscarUm(id) );
    }

    @GetMapping("/")
    @ResponseBody
    public List<AcessoDTO> buscarTodos() {
        return acessoService.buscarTodos()
                .stream()
                .map(AcessoFactory::getInstance)
                .collect(Collectors.toList());
    }

    @PutMapping("/atualizar/{id}")
    @ResponseBody
    public AcessoDTO atualizar(@PathVariable long id, @RequestBody Acesso acesso) {
        return AcessoFactory.getInstance( acessoService.atualizar(id, acesso) );
    }

    @DeleteMapping("/deletar/{id}")
    public void excluir(@PathVariable long id) {
        acessoService.excluir(id);
    }
}
