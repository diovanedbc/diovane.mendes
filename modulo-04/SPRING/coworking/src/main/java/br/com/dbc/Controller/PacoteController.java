package br.com.dbc.Controller;

import br.com.dbc.DTO.PacoteDTO;
import br.com.dbc.Entity.Pacote;
import br.com.dbc.Factory.PacoteFactory;
import br.com.dbc.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController implements PadraoController<PacoteDTO, Pacote>{

    @Autowired
    private PacoteService pacoteService;

    public PacoteDTO salvar(Pacote pacote) {
        return PacoteFactory.getInstance( pacoteService.salvar(pacote) );
    }

    public PacoteDTO buscarUm(long id) {
        return PacoteFactory.getInstance( pacoteService.buscarUm(id) );
    }

    public List<PacoteDTO> buscarTodos() {
        return pacoteService.buscarTodos()
                .stream()
                .map(PacoteFactory::getInstance)
                .collect(Collectors.toList());
    }

    public PacoteDTO atualizar(long id, Pacote pacote) {
        return PacoteFactory.getInstance( pacoteService.atualizar(id, pacote) );
    }

    public void excluir(long id) {
        pacoteService.excluir(id);
    }
}
