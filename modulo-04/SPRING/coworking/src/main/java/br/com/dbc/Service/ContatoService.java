package br.com.dbc.Service;

import br.com.dbc.Entity.Contato;
import br.com.dbc.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService implements PadraoService<Contato>{

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato salvar(Contato contato) {
        return contatoRepository.save(contato);
    }

    public Contato buscarUm(long id) {
        return contatoRepository.findById(id);
    }

    public List<Contato> buscarTodos() {
        return contatoRepository.findAll();
    }

    public Contato atualizar(long id, Contato contato) {
        contato.setId(id);
        return salvar(contato);
    }

    public void excluir(long id) {
        contatoRepository.deleteById(id);
    }
}
