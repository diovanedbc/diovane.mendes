package br.com.dbc.Service;

import br.com.dbc.Component.SaldoClienteComponent;
import br.com.dbc.Entity.*;
import br.com.dbc.Repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class PagamentoService {

    private PagamentoRepository pagamentoRepository;

    private ClientePacoteRepository clientePacoteRepository;

    private PacoteRepository pacoteRepository;

    private ClienteRepository clienteRepository;

    private SaldoClienteRepository saldoClienteRepository;

    private ContratacaoRepository contratacaoRepository;

    public PagamentoService(PagamentoRepository pagamentoRepository,
                            ClientePacoteRepository clientePacoteRepository,
                            PacoteRepository pacoteRepository,
                            ClienteRepository clienteRepository,
                            SaldoClienteRepository saldoClienteRepository,
                            ContratacaoRepository contratacaoRepository) {
        this.pagamentoRepository = pagamentoRepository;
        this.clientePacoteRepository = clientePacoteRepository;
        this.pacoteRepository = pacoteRepository;
        this.clienteRepository = clienteRepository;
        this.saldoClienteRepository = saldoClienteRepository;
        this.contratacaoRepository = contratacaoRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamento salvarEGeraSaldo(Pagamento pagamento){
        Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);
        verificadorContrato(pagamentoSalvo);
        return pagamentoSalvo;
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamento buscarUm(long id){
        return pagamentoRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Pagamento> buscaTodos(){
        return pagamentoRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamento atualizar(long id, Pagamento pagamento){
        pagamento.setId(id);
        return pagamentoRepository.save(pagamento);
    }

    public void excluir(long id){
        pagamentoRepository.deleteById(id);
    }

    private void verificadorContrato(Pagamento pagamento){
        if ( pagamento.getClientePacote() != null){
            geraSaldoParaPacote(pagamento);
        }
        else if ( pagamento.getContratacao() != null ){
            geraSaldoParaContratacao(pagamento);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void geraSaldoParaPacote(Pagamento pagamento){
        ClientePacote clientePacote = clientePacoteRepository
                .findById( pagamento
                        .getClientePacote()
                        .getId());

        Cliente cliente = clienteRepository
                .findById( clientePacote
                        .getCliente()
                        .getId());

        Pacote pacote = pacoteRepository
                .findById( clientePacote
                        .getPacote()
                        .getId());

        int quantidadeDePacotes = clientePacote.getQuantidadePacote();

        long idCliente = cliente.getId();
        long idEspaco;

        for ( EspacoPacote ep : pacote.getEspacoPacotes() ){
            idEspaco = ep.getEspaco().getId();

            SaldoCliente saldoCliente = verificaExistenciaSaldo(idCliente, idEspaco);

            saldoCliente.setTipoContratacao( ep.getTipoContratacao() );
            saldoCliente.setQuantidadeTipoContratacao( ep.getQuantidade() );

            geraVencimento(saldoCliente, ep.getPrazoEmDias(), quantidadeDePacotes);

            saldoClienteRepository.save(saldoCliente);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void geraSaldoParaContratacao(Pagamento pagamento){
        Contratacao contratacao = contratacaoRepository
                .findById( pagamento
                        .getContratacao()
                        .getId());

        long idCliente = contratacao.getCliente().getId();
        long idEspaco = contratacao.getEspaco().getId();

        SaldoCliente saldoCliente = verificaExistenciaSaldo(idCliente, idEspaco);

            saldoCliente.setTipoContratacao( contratacao.getTipoContratacao() );
            saldoCliente.setQuantidadeTipoContratacao( contratacao.getQuantidade() );

        int prazoEmDias = contratacao.getPrazoEmDias();
        int quantidadeContratacao = contratacao.getQuantidade();

        geraVencimento(saldoCliente, prazoEmDias, quantidadeContratacao);

        saldoClienteRepository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente verificaExistenciaSaldo(long idCliente, long idEspaco){
        return saldoClienteRepository
                .findById( new CompostaClienteEspaco(idCliente, idEspaco) )
                .orElse( new SaldoCliente( new CompostaClienteEspaco(idCliente, idEspaco) ) );
    }

    private void geraVencimento(SaldoCliente saldoCliente, int prazo, int quantidade){
        if ( saldoCliente.getVencimento() == null ){
            saldoCliente.setVencimento( LocalDateTime.now()
                    .plusDays( prazo * quantidade ) );
        }else{
            saldoCliente.setVencimento( saldoCliente.getVencimento()
                    .plusDays( prazo * quantidade ) );
        }
    }
}
