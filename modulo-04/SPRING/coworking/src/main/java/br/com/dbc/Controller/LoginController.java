package br.com.dbc.Controller;

import br.com.dbc.Component.UsuarioComponent;
import br.com.dbc.Entity.Usuario;
import br.com.dbc.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api")
public class LoginController {

    private UsuarioService usuarioService;

    private UsuarioComponent usuarioComponent;

    @Autowired
    public LoginController(UsuarioService usuarioService, UsuarioComponent usuarioComponent) {
        this.usuarioService = usuarioService;
        this.usuarioComponent = usuarioComponent;
    }

    @PostMapping("/login")
    @ResponseBody
    public String login(@RequestBody Usuario usuario){

        return "OK: " + usuario.getNome();
    }

    @PostMapping("/registrar")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public String registro(@RequestBody Usuario usuario, BindingResult bindingResult){
        usuarioComponent.validate(usuario, bindingResult);

        if ( bindingResult.hasErrors() ){
            return "Erro ao salvar o usuario: " + bindingResult.getAllErrors()
                    .stream()
                    .map(o -> "" + o.getDefaultMessage())
                    .collect(Collectors.toList());
        }

        usuarioService.salvar(usuario);

        return "Usuario salvo com sucesso!";
    }
}
