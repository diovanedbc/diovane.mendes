package br.com.dbc.Entity.Enuns;

public enum TipoContratacao {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES;
}
