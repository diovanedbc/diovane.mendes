package br.com.dbc.Service;

import br.com.dbc.Entity.CompostaClienteEspaco;
import br.com.dbc.Entity.SaldoCliente;
import br.com.dbc.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente buscarUm(long idCliente, long idEspaco) {
        CompostaClienteEspaco idComposto = new CompostaClienteEspaco(idCliente, idEspaco);
        return saldoClienteRepository
                .findById(idComposto)
                .orElse(null);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<SaldoCliente> buscarTodos() {
        return saldoClienteRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente atualizar(long idCliente, long idEspaco, SaldoCliente saldoCliente) {
        CompostaClienteEspaco idComposto = new CompostaClienteEspaco(idCliente, idEspaco);
        saldoCliente.setId(idComposto);
        return salvar(saldoCliente);
    }

    public void excluir(long idCliente, long idEspaco) {
        CompostaClienteEspaco idComposto = new CompostaClienteEspaco(idCliente, idEspaco);
        saldoClienteRepository.deleteById(idComposto);
    }
}
