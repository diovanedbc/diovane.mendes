package br.com.dbc.Service;

import br.com.dbc.Entity.Contratacao;
import br.com.dbc.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContratacaoService implements PadraoService<Contratacao>{

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    public Contratacao salvar(Contratacao contratacao){
        return contratacaoRepository.save( contratacao );
    }

    public Contratacao buscarUm(long id){
        return contratacaoRepository.findById(id);
    }

    public List<Contratacao> buscarTodos(){
        return contratacaoRepository.findAll();
    }

    public Contratacao atualizar(long id, Contratacao contratacao){
        contratacao.setId(id);
        return salvar( contratacao );
    }

    public void excluir(long id){
        contratacaoRepository.deleteById(id);
    }
}
