package br.com.dbc.Controller;

import br.com.dbc.DTO.TipoContatoDTO;
import br.com.dbc.Entity.TipoContato;
import br.com.dbc.Factory.TipoContatoFactory;
import br.com.dbc.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/tipo-contato")
public class TipoContatoController implements PadraoController<TipoContatoDTO, TipoContato>{

    @Autowired
    private TipoContatoService tipoContatoService;

    public TipoContatoDTO salvar(TipoContato tipoContato) {
        return TipoContatoFactory.getInstance( tipoContatoService.salvar(tipoContato) );
    }

    public TipoContatoDTO buscarUm(long id) {
        return TipoContatoFactory.getInstance( tipoContatoService.buscarUm(id) );
    }

    public List<TipoContatoDTO> buscarTodos() {
        return tipoContatoService.buscarTodos()
                .stream()
                .map(TipoContatoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public TipoContatoDTO atualizar(long id, TipoContato tipoContato) {
        return TipoContatoFactory.getInstance( tipoContatoService.atualizar(id, tipoContato) );
    }

    public void excluir(long id) {
        tipoContatoService.excluir(id);
    }
}
