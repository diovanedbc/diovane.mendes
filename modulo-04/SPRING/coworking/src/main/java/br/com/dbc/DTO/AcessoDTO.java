package br.com.dbc.DTO;

import br.com.dbc.Entity.SaldoCliente;

public class AcessoDTO {

    private long id;

    private SaldoCliente saldoCliente;

    private Boolean isEntrada = false;

    private String dataHora;

    public AcessoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getIsEntrada() {
        return isEntrada;
    }

    public void setIsEntrada(Boolean isEntrada) {
        this.isEntrada = isEntrada;
    }

    public String getDataHora() {
        return dataHora;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }
}
