package br.com.dbc.Repository;

import br.com.dbc.Entity.Espaco;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacoRepository extends CrudRepository<Espaco, Long> {

    Espaco findById(long id);
    List<Espaco> findAll();

}
