package br.com.dbc.Factory;

import br.com.dbc.DTO.PagamentoDTO;
import br.com.dbc.Entity.Pagamento;

public abstract class PagamentoFactory {

    public static Pagamento getInstance(PagamentoDTO pagamentoDTO){
        Pagamento pagamento = new Pagamento();

        pagamento.setId( pagamentoDTO.getId() );

        if ( pagamentoDTO.getClientePacote() != null )
            pagamento.setClientePacote( ClientePacoteFactory.getInstance( pagamentoDTO.getClientePacote() ) );

        if ( pagamentoDTO.getContratacao() != null )
            pagamento.setContratacao( ContratacaoFactory.getInstance( pagamentoDTO.getContratacao() ));

        pagamento.setTipoPagamento( pagamentoDTO.getTipoPagamento() );

        return pagamento;
    }

    public static PagamentoDTO getInstance(Pagamento pagamento){
        PagamentoDTO pagamentoDTO = new PagamentoDTO();

        pagamentoDTO.setId( pagamento.getId() );

        if ( pagamento.getClientePacote() != null )
            pagamentoDTO.setClientePacote( ClientePacoteFactory.getInstance( pagamento.getClientePacote() ) );

        if ( pagamento.getContratacao() != null )
            pagamentoDTO.setContratacao( ContratacaoFactory.getInstance( pagamento.getContratacao() ));

        pagamentoDTO.setTipoPagamento( pagamento.getTipoPagamento() );

        return pagamentoDTO;
    }
}
