package br.com.dbc.Controller;

import br.com.dbc.DTO.ClienteDTO;
import br.com.dbc.Entity.Cliente;
import br.com.dbc.Factory.ClienteFactory;
import br.com.dbc.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController implements PadraoController<ClienteDTO, Cliente>{

    @Autowired
    private ClienteService clienteService;

    public ClienteDTO salvar(Cliente cliente) {
        return ClienteFactory.getInstance( clienteService.salvar( cliente ) );
    }

    public ClienteDTO buscarUm(long id) {
        return ClienteFactory.getInstance( clienteService.buscarUm(id) );
    }

    public List<ClienteDTO> buscarTodos() {
        return clienteService.buscarTodos()
                .stream()
                .map(ClienteFactory::getInstance)
                .collect(Collectors.toList());
    }

    public ClienteDTO atualizar(long id, Cliente cliente) {
        return ClienteFactory.getInstance( clienteService.atualizar(id, cliente) );
    }

    public void excluir(long id) {
        clienteService.excluir(id);
    }
}
