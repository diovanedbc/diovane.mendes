package br.com.dbc.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ESPACOS")
public class Espaco {

    @Id
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME", nullable = false, unique = true)
    private String nome;

    @Column(name = "QUANTIDADE_PESSOAS", nullable = false)
    private int quantidadePessoas;

    @Column(name = "VALOR", nullable = false)
    private double valor;

    @OneToMany(mappedBy = "espaco")
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacote> espacoPacotes = new ArrayList<>();

    public Espaco() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(int quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<EspacoPacote> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<EspacoPacote> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }
}
