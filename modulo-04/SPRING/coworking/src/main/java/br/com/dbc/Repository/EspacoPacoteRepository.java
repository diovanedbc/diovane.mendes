package br.com.dbc.Repository;

import br.com.dbc.Entity.EspacoPacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Long> {

    EspacoPacote findById(long id);
    List<EspacoPacote> findAll();

}
