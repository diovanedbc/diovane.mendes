package br.com.dbc.Component;

import br.com.dbc.Entity.Usuario;
import br.com.dbc.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UsuarioComponent implements Validator {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return Usuario.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {

        Usuario usuario = (Usuario) object;

        if ( usuarioRepository.findByLogin( usuario.getLogin() ) != null ){
            errors.reject("Login","Login já existente!");
        }

        if ( usuarioRepository.findByEmail( usuario.getEmail() ) != null ){
            errors.reject("Email", "Email já existente!");
        }

        if ( usuario.getNome() == null ){
            errors.reject("Nome", "Campo nome está vazio!");
        }

        if ( usuario.getEmail() == null ){
            errors.reject("Email", "Campo email está vazio!");
        }

        if ( usuario.getSenha() == null ){
            errors.reject("Senha", "Campo senha está vazio!");
        }
        else if ( usuario.getSenha().length() < 6){
            errors.reject("Tamanho", "Senha deve ter no mínimo 6 digitos!");
        }

        if ( usuario.getLogin() == null ){
            errors.reject("Login", "Campo login está vazio!");
        }
    }
}
