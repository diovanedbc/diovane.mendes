package br.com.dbc.Repository;

import br.com.dbc.Entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findById(long id);
    List<Usuario> findAll();
    Usuario findByNome(String nome);
    Usuario findByLogin(String login);
    Usuario findByEmail(String email);

}
