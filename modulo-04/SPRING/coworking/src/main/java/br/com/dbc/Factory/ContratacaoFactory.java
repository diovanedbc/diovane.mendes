package br.com.dbc.Factory;

import br.com.dbc.DTO.ContratacaoDTO;
import br.com.dbc.Entity.Contratacao;

public abstract class ContratacaoFactory {

    public static Contratacao getInstance(ContratacaoDTO contratacaoDTO){
        Contratacao contratacao = new Contratacao();

        contratacao.setId( contratacaoDTO.getId() );
        contratacao.setEspaco( EspacoFactory.getInstance( contratacaoDTO.getEspaco() ) );
        contratacao.setCliente( ClienteFactory.getInstance( contratacaoDTO.getCliente() ) );
        contratacao.setTipoContratacao( contratacaoDTO.getTipoContratacao() );
        contratacao.setQuantidade( contratacaoDTO.getQuantidade() );
        contratacao.setDesconto( contratacaoDTO.getDesconto() );
        contratacao.setPrazoEmDias( contratacaoDTO.getPrazoEmDias() );

        return contratacao;
    }

    public static ContratacaoDTO getInstance(Contratacao contratacao){
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO();

        contratacaoDTO.setId( contratacao.getId() );
        contratacaoDTO.setEspaco( EspacoFactory.getInstance( contratacao.getEspaco() ) );
        contratacaoDTO.setCliente( ClienteFactory.getInstance( contratacao.getCliente() ) );
        contratacaoDTO.setTipoContratacao( contratacao.getTipoContratacao() );
        contratacaoDTO.setQuantidade( contratacao.getQuantidade() );
        contratacaoDTO.setDesconto( contratacao.getDesconto() );
        contratacaoDTO.setPrazoEmDias( contratacao.getPrazoEmDias() );
        contratacaoDTO.calculoValorCobrado();

        return contratacaoDTO;
    }
}
