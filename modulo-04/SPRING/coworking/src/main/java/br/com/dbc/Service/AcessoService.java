package br.com.dbc.Service;

import br.com.dbc.Component.SaldoClienteComponent;
import br.com.dbc.Entity.Acesso;
import br.com.dbc.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class AcessoService{

    @Autowired
    private AcessoRepository acessoRepository;

    private final SaldoClienteComponent saldoClienteComponent = new SaldoClienteComponent();

    @Transactional(rollbackFor = Exception.class)
    public String salvar(Acesso acesso){

        if ( acesso.getSaldoCliente().getQuantidadeTipoContratacao() <= 0 ) {

            if (acesso.getDataHora() == null){
                acesso.setDataHora( LocalDateTime.now() );
            }

            Acesso ultimoAcesso = acessoRepository.findAllBySaldoCliente( acesso.getSaldoCliente() )
                    .stream()
                    .max( (d1, d2) -> d1.getDataHora().compareTo( d2.getDataHora() ) )
                    .get();

            if ( !ultimoAcesso.getIsEntrada() ){
                Duration dataValida = Duration.between( LocalDateTime.now(), acesso.getSaldoCliente().getVencimento() );

                if ( dataValida.isNegative() ){
                    acesso.getSaldoCliente().setQuantidadeTipoContratacao(0);
                }else{
                    LocalDateTime vencimento = ultimoAcesso.getDataHora();
                    Duration diferencaDatas = Duration.between( vencimento , acesso.getDataHora() );

                    int desconto = saldoClienteComponent.saldoPosSaida( ultimoAcesso.getSaldoCliente(), diferencaDatas );

                    acesso.getSaldoCliente().setQuantidadeTipoContratacao(desconto);
                }
            }

            acessoRepository.save(acesso);

            return "Saldo de: " + acesso.getSaldoCliente().getQuantidadeTipoContratacao();
        }
        return "Saldo Insuficiente!";
    }

    @Transactional(rollbackFor = Exception.class)
    public Acesso buscarUm(long id) {
        return acessoRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Acesso> buscarTodos() {
        return acessoRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Acesso atualizar(long id, Acesso acesso) {
        acesso.setId(id);
        return acessoRepository.save(acesso);
    }

    public void excluir(long id) {
        acessoRepository.deleteById(id);
    }
}
