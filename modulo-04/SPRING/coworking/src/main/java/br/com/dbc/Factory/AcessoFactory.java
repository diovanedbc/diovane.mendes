package br.com.dbc.Factory;

import br.com.dbc.Component.AcessoComponent;
import br.com.dbc.DTO.AcessoDTO;
import br.com.dbc.Entity.Acesso;

import java.time.LocalDateTime;

import static java.time.format.DateTimeFormatter.ofPattern;

public abstract class AcessoFactory {
    private static final AcessoComponent acessoComponent = new AcessoComponent();

    public static Acesso getInstance(AcessoDTO acessoDTO){
        Acesso acesso = new Acesso();

        acesso.setId( acessoDTO.getId() );
        acesso.setSaldoCliente( acessoDTO.getSaldoCliente() );
        acesso.setIsEntrada( acessoDTO.getIsEntrada() );
        acesso.setDataHora( acessoComponent.stringToLocalDateTime( acessoDTO.getDataHora() ) );

        return acesso;
    }

    public static AcessoDTO getInstance(Acesso acesso){
        AcessoDTO acessoDTO = new AcessoDTO();

        acessoDTO.setId( acesso.getId() );
        acessoDTO.setSaldoCliente( acesso.getSaldoCliente() );
        acessoDTO.setIsEntrada( acesso.getIsEntrada() );
        acessoDTO.setDataHora( acessoComponent.formatarDataHora( acesso.getDataHora() ) );

        return acessoDTO;
    }
}
