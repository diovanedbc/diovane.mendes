package br.com.dbc.Service;

import br.com.dbc.Entity.Cliente;
import br.com.dbc.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService implements PadraoService<Cliente>{

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvar(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscarUm(long id) {
        return clienteRepository.findById(id);
    }

    public List<Cliente> buscarTodos() {
        return clienteRepository.findAll();
    }

    public Cliente atualizar(long id, Cliente cliente) {
        cliente.setId(id);
        return salvar(cliente);
    }

    public void excluir(long id) {
        clienteRepository.deleteById(id);
    }
}
