package br.com.dbc.Factory;

import br.com.dbc.DTO.ClienteDTO;
import br.com.dbc.Entity.Cliente;

public abstract class ClienteFactory {

    public static Cliente getInstance(ClienteDTO clienteDTO){
        Cliente cliente = new Cliente();

        cliente.setId( clienteDTO.getId() );
        cliente.setNome( clienteDTO.getNome() );
        cliente.setCpf( clienteDTO.getCpf() );
        cliente.setDataNascimento( clienteDTO.getDataNascimento() );

        return cliente;
    }

    public static ClienteDTO getInstance(Cliente cliente){
        ClienteDTO clienteDTO = new ClienteDTO();

        clienteDTO.setId( cliente.getId() );
        clienteDTO.setNome( cliente.getNome() );
        clienteDTO.setCpf( cliente.getCpf() );
        clienteDTO.setDataNascimento( cliente.getDataNascimento() );

        return clienteDTO;
    }
}
