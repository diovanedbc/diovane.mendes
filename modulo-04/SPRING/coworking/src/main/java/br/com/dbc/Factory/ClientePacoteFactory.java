package br.com.dbc.Factory;

import br.com.dbc.DTO.ClientePacoteDTO;
import br.com.dbc.Entity.ClientePacote;

public abstract class ClientePacoteFactory {

    public static ClientePacote getInstance(ClientePacoteDTO clientePacoteDTO) {
        ClientePacote clientePacote = new ClientePacote();

        clientePacote.setId( clientePacoteDTO.getId() );
        clientePacote.setCliente( ClienteFactory.getInstance(clientePacoteDTO.getCliente() ) );
        clientePacote.setPacote( PacoteFactory.getInstance( clientePacoteDTO.getPacote() ) );
        clientePacote.setQuantidadePacote( clientePacoteDTO.getQuantidadePacote() );

        return clientePacote;
    }

    public static ClientePacoteDTO getInstance(ClientePacote clientePacote) {
        ClientePacoteDTO clientePacoteDTO = new ClientePacoteDTO();

        clientePacoteDTO.setId( clientePacote.getId() );
        clientePacoteDTO.setCliente( ClienteFactory.getInstance(clientePacote.getCliente() ) );
        clientePacoteDTO.setPacote( PacoteFactory.getInstance( clientePacote.getPacote() ) );
        clientePacoteDTO.setQuantidadePacote( clientePacote.getQuantidadePacote() );

        return clientePacoteDTO;
    }
}
