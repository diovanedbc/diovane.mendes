package br.com.dbc.Service;

import br.com.dbc.Entity.Usuario;
import br.com.dbc.Repository.UsuarioRepository;
import br.com.dbc.Security.MD5Crypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioService {

    private UsuarioRepository usuarioRepository;

    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public String salvar(Usuario usuario){
        if ( usuario != null ){
            usuario.setSenha( new MD5Crypt().encode( usuario.getSenha() ) );
            usuarioRepository.save(usuario);

            return "Usuario salvo com sucesso!";
        }

        return "Usuario não registrado no banco!";
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario buscarUm(long id){
        return usuarioRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Usuario> buscarTodos(){
        return usuarioRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public String atualizar(long id, Usuario usuario){
        usuario.setId(id);
        usuarioRepository.save(usuario);
        return "Usuario atualizado com sucesso!";
    }

    public void excluir(long id){
        usuarioRepository.deleteById(id);
    }
}
