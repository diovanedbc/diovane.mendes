package br.com.dbc.Repository;

import br.com.dbc.Entity.CompostaClienteEspaco;
import br.com.dbc.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, CompostaClienteEspaco> {

    List<SaldoCliente> findAll();

}
