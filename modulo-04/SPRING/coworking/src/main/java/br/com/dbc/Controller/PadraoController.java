package br.com.dbc.Controller;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface PadraoController<T, E> {

    @PostMapping("/salvar")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    T salvar(@RequestBody E e);

    @GetMapping("/{id}")
    @ResponseBody
    T buscarUm(@PathVariable long id);

    @GetMapping("/")
    @ResponseBody
    List<T> buscarTodos();

    @PutMapping("/atualizar/{id}")
    @ResponseBody
    T atualizar(@PathVariable long id, @RequestBody E e);

    @DeleteMapping("/deletar/{id}")
    void excluir(@PathVariable long id);

}
