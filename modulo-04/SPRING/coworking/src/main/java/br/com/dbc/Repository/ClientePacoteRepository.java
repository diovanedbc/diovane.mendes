package br.com.dbc.Repository;

import br.com.dbc.Entity.ClientePacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Long> {

    ClientePacote findById(long id);
    List<ClientePacote> findAll();

}
