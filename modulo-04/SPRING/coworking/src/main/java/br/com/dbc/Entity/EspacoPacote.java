package br.com.dbc.Entity;

import br.com.dbc.Entity.Enuns.TipoContratacao;

import javax.persistence.*;

@Entity
@Table(name = "ESPACOS_PACOTES")
public class EspacoPacote {

    @Id
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ESPACO", nullable = false)
    private Espaco espaco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PACOTE", nullable = false)
    private Pacote pacote;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_CONTRATACAO")
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE")
    private int quantidade;

    @Column(name = "PRAZO")
    private int prazoEmDias;

    public EspacoPacote() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazoEmDias() {
        return prazoEmDias;
    }

    public void setPrazoEmDias(int prazoEmDias) {
        this.prazoEmDias = prazoEmDias;
    }
}
