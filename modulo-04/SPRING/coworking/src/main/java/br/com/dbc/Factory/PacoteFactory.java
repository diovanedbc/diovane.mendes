package br.com.dbc.Factory;

import br.com.dbc.DTO.PacoteDTO;
import br.com.dbc.Entity.Pacote;

public class PacoteFactory {

    public static Pacote getInstance(PacoteDTO pacoteDTO) {
        Pacote pacote = new Pacote();

        pacote.setId( pacoteDTO.getId() );
        pacote.setValor( pacoteDTO.getValor() );

        return pacote;
    }

    public static PacoteDTO getInstance(Pacote pacote) {
        PacoteDTO pacoteDTO = new PacoteDTO();

        pacoteDTO.setId( pacote.getId() );
        pacoteDTO.setValor( pacote.getValor() );

        return pacoteDTO;
    }
}
