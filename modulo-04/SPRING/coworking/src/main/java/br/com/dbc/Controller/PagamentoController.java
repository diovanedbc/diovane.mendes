package br.com.dbc.Controller;

import br.com.dbc.DTO.PagamentoDTO;
import br.com.dbc.Entity.Pagamento;
import br.com.dbc.Factory.PagamentoFactory;
import br.com.dbc.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController implements PadraoController<PagamentoDTO, Pagamento>{

    @Autowired
    private PagamentoService pagamentoService;

    public PagamentoDTO salvar(Pagamento pagamento) {
        Pagamento pagamentoSalvo = pagamentoService.salvarEGeraSaldo(pagamento);
        return PagamentoFactory.getInstance(pagamentoSalvo);
    }

    public PagamentoDTO buscarUm(long id) {
        return PagamentoFactory.getInstance( pagamentoService.buscarUm(id) );
    }

    public List<PagamentoDTO> buscarTodos() {
        return pagamentoService.buscaTodos()
                .stream()
                .map(PagamentoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public PagamentoDTO atualizar(long id, Pagamento pagamento) {
        return PagamentoFactory.getInstance( pagamentoService.atualizar(id, pagamento) );
    }

    public void excluir(long id) {
        pagamentoService.excluir(id);
    }
}
