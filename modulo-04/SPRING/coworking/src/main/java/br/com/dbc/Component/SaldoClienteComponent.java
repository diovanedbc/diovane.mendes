package br.com.dbc.Component;

import br.com.dbc.Entity.SaldoCliente;

import java.time.Duration;

public class SaldoClienteComponent {

    public int saldoPosSaida(SaldoCliente saldoCliente, Duration duration){

        int quantidade = saldoCliente.getQuantidadeTipoContratacao();
        int desconto = 0;

        switch ( saldoCliente.getTipoContratacao() ){
            case MINUTO:
                desconto =  Math.round( quantidade - duration.toMinutes() );
            break;
            case HORA:
                desconto = Math.round( quantidade - (float) duration.toMinutes() / 60) ;
            break;
            case TURNO:
                desconto = Math.round( quantidade - (float) duration.toMinutes() / 60 * 5 );
            break;
            case DIARIA:
                desconto = Math.round( quantidade - 1);
            break;
        }
        return desconto;
    }
}
