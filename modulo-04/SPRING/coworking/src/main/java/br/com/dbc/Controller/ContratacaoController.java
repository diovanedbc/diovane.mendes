package br.com.dbc.Controller;

import br.com.dbc.DTO.ContratacaoDTO;
import br.com.dbc.Entity.Contratacao;
import br.com.dbc.Factory.ContratacaoFactory;
import br.com.dbc.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController implements PadraoController<ContratacaoDTO, Contratacao>{

    @Autowired
    private ContratacaoService contratacaoService;

    public ContratacaoDTO salvar(Contratacao contratacao) {
        contratacaoService.salvar( contratacao );
        return ContratacaoFactory.getInstance( contratacao );
    }

    public ContratacaoDTO buscarUm(long id) {
        Contratacao contratacao = contratacaoService.buscarUm(id);
        return ContratacaoFactory.getInstance( contratacao );
    }

    public List<ContratacaoDTO> buscarTodos() {
        return contratacaoService.buscarTodos()
                .stream()
                .map(ContratacaoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public ContratacaoDTO atualizar(long id, Contratacao contratacao) {
        Contratacao contratacaoAtualizada = contratacaoService.atualizar(id, contratacao);
        return ContratacaoFactory.getInstance( contratacaoAtualizada );
    }

    public void excluir(long id) {
        contratacaoService.excluir(id);
    }
}
