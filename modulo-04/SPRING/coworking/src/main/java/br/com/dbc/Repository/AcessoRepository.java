package br.com.dbc.Repository;

import br.com.dbc.Entity.Acesso;
import br.com.dbc.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Acesso findById(long id);
    List<Acesso> findAll();
    List<Acesso> findAllBySaldoCliente(SaldoCliente saldoCliente);

}
