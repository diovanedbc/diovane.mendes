package br.com.dbc.Factory;

import br.com.dbc.DTO.ContatoDTO;
import br.com.dbc.Entity.Contato;

public abstract class ContatoFactory {

    public static Contato getInstance(ContatoDTO contatoDTO){
        Contato contato = new Contato();

        contato.setId( contatoDTO.getId() );
        contato.setTipoContato( TipoContatoFactory.getInstance( contatoDTO.getTipoContato() ) );
        contato.setValor( contatoDTO.getValor() );
        contato.setCliente( ClienteFactory.getInstance( contatoDTO.getCliente() ) );

        return contato;
    }

    public static ContatoDTO getInstance(Contato contato){
        ContatoDTO contatoDTO = new ContatoDTO();

        contatoDTO.setId( contato.getId() );
        contatoDTO.setTipoContato( TipoContatoFactory.getInstance( contato.getTipoContato() ) );
        contatoDTO.setValor( contato.getValor() );
        contatoDTO.setCliente( ClienteFactory.getInstance( contato.getCliente() ) );

        return contatoDTO;
    }
}
