package br.com.dbc.Entity;

import br.com.dbc.Entity.Enuns.TipoContratacao;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTES")
public class SaldoCliente{

    @EmbeddedId
    private CompostaClienteEspaco id;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE", nullable = false)
    private int quantidadeTipoContratacao;

    @Column(name = "VENCIMENTO", nullable = false)
    private LocalDateTime vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<Acesso> acessos = new ArrayList<>();

    public SaldoCliente() {
    }

    public SaldoCliente(CompostaClienteEspaco id) {
        this.id = id;
    }

    public CompostaClienteEspaco getId() {
        return id;
    }

    public void setId(CompostaClienteEspaco id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidadeTipoContratacao() {
        return quantidadeTipoContratacao;
    }

    public void setQuantidadeTipoContratacao(int quantidadeTipoContratacao) {
        this.quantidadeTipoContratacao = quantidadeTipoContratacao;
    }

    public LocalDateTime getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDateTime vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acesso> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acesso> acessos) {
        this.acessos = acessos;
    }
}
