package br.com.dbc.DTO;

import br.com.dbc.Entity.Enuns.TipoContratacao;

public class EspacoPacoteDTO {

    private long id;

    private EspacoDTO espaco;

    private PacoteDTO pacote;

    private TipoContratacao tipoContratacao;

    private int quantidade;

    private int prazoEmDias;

    public EspacoPacoteDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazoEmDias() {
        return prazoEmDias;
    }

    public void setPrazoEmDias(int prazoEmDias) {
        this.prazoEmDias = prazoEmDias;
    }
}
