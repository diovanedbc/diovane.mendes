package br.com.dbc.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CompostaClienteEspaco implements Serializable {

    @Column(name = "ID_CLIENTE")
    private long idCliente;

    @Column(name = "ID_ESPACO")
    private long idEspaco;

    public CompostaClienteEspaco() {
    }

    public CompostaClienteEspaco(long idCliente, long idEspaco) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public long getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(long idEspaco) {
        this.idEspaco = idEspaco;
    }
}
