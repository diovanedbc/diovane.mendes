package br.com.dbc.Repository;

import br.com.dbc.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PacoteRepository extends CrudRepository<Pacote, Long> {

    Pacote findById(long id);
    List<Pacote> findAll();

}
