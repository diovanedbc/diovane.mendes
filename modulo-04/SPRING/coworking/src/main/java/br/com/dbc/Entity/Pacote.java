package br.com.dbc.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PACOTES")
public class Pacote {

    @Id
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "VALOR", nullable = false)
    private double valor;

    @OneToMany(mappedBy = "pacote")
    private List<EspacoPacote> espacoPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote")
    private List<ClientePacote> clientePacotes = new ArrayList<>();

    public Pacote() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacoPacote> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<EspacoPacote> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    public List<ClientePacote> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<ClientePacote> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }
}
