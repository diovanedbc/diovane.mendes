package br.com.dbc.DTO;

public class ClientePacoteDTO {

    private long id;

    private ClienteDTO cliente;

    private PacoteDTO pacote;

    private int quantidadePacote;

    public ClientePacoteDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public int getQuantidadePacote() {
        return quantidadePacote;
    }

    public void setQuantidadePacote(int quantidadePacote) {
        this.quantidadePacote = quantidadePacote;
    }
}
