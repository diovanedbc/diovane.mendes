package br.com.dbc.Factory;

import br.com.dbc.DTO.EspacoDTO;
import br.com.dbc.Entity.Espaco;

public abstract class EspacoFactory {

    public static Espaco getInstance(EspacoDTO espacoDTO){
        Espaco espaco = new Espaco();

        espaco.setId( espacoDTO.getId() );
        espaco.setNome( espacoDTO.getNome() );
        espaco.setQuantidadePessoas( espacoDTO.getQuantidadePessoas() );
        espaco.setValor( espacoDTO.getValor() );

        return espaco;
    }

    public static EspacoDTO getInstance(Espaco espaco){
        EspacoDTO espacoDTO = new EspacoDTO();

        espacoDTO.setId( espaco.getId() );
        espacoDTO.setNome( espaco.getNome() );
        espacoDTO.setQuantidadePessoas( espaco.getQuantidadePessoas() );
        espacoDTO.setValor( espaco.getValor() );

        return espacoDTO;
    }
}
