package br.com.dbc.Factory;

import br.com.dbc.DTO.TipoContatoDTO;
import br.com.dbc.Entity.TipoContato;

public abstract class TipoContatoFactory {

    public static TipoContato getInstance(TipoContatoDTO tipoContatoDTO) {
        TipoContato tipoContato = new TipoContato();

        tipoContato.setId( tipoContatoDTO.getId() );
        tipoContato.setNome( tipoContatoDTO.getNome() );

        return tipoContato;
    }

    public static TipoContatoDTO getInstance(TipoContato tipoContato) {
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO();

        tipoContatoDTO.setId( tipoContato.getId() );
        tipoContatoDTO.setNome( tipoContato.getNome() );

        return tipoContatoDTO;
    }
}
