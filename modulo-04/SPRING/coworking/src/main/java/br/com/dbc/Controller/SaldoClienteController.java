package br.com.dbc.Controller;

import br.com.dbc.Entity.SaldoCliente;
import br.com.dbc.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api/saldo-cliente")
public class SaldoClienteController{

    @Autowired
    private SaldoClienteService saldoClienteService;


    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return saldoClienteService.salvar(saldoCliente);
    }

    @GetMapping("/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoCliente buscarUm(@PathVariable long idCliente, @PathVariable long idEspaco) {
        return saldoClienteService.buscarUm(idCliente, idEspaco);
    }

    public List<SaldoCliente> buscarTodos() {
        return saldoClienteService.buscarTodos();
    }

    public SaldoCliente atualizar(long idCliente, long idEspaco, SaldoCliente saldoCliente) {
        return saldoClienteService.atualizar(idCliente, idEspaco, saldoCliente);
    }

    public void excluir(long idCliente, long idEspaco) {
        saldoClienteService.excluir(idCliente,idEspaco);
    }
}
