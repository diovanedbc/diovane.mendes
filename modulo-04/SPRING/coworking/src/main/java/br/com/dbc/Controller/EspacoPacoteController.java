package br.com.dbc.Controller;

import br.com.dbc.DTO.EspacoPacoteDTO;
import br.com.dbc.Entity.EspacoPacote;
import br.com.dbc.Factory.EspacoPacoteFactory;
import br.com.dbc.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/espaco-pacote")
public class EspacoPacoteController implements PadraoController<EspacoPacoteDTO, EspacoPacote>{

    @Autowired
    private EspacoPacoteService espacoPacoteService;

    public EspacoPacoteDTO salvar(EspacoPacote espacoPacote) {
        return EspacoPacoteFactory.getInstance( espacoPacoteService.salvar(espacoPacote) );
    }

    public EspacoPacoteDTO buscarUm(long id) {
        return EspacoPacoteFactory.getInstance( espacoPacoteService.buscarUm(id) );
    }

    public List<EspacoPacoteDTO> buscarTodos() {
        return espacoPacoteService.buscarTodos()
                .stream()
                .map(EspacoPacoteFactory::getInstance)
                .collect(Collectors.toList());
    }

    public EspacoPacoteDTO atualizar(long id, EspacoPacote espacoPacote) {
        return EspacoPacoteFactory.getInstance( espacoPacoteService.atualizar(id, espacoPacote) );
    }

    public void excluir(long id) {
        espacoPacoteService.excluir(id);
    }
}
