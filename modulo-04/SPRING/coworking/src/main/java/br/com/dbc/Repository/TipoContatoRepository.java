package br.com.dbc.Repository;

import br.com.dbc.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TipoContatoRepository extends CrudRepository<TipoContato, Long> {

    TipoContato findById(long id);
    List<TipoContato> findAll();

}
