package br.com.dbc.Repository;

import br.com.dbc.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {

    List<Contratacao> findAll();
    Contratacao findById(long id);

}
