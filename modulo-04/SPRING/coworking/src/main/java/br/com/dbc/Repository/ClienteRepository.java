package br.com.dbc.Repository;

import br.com.dbc.Entity.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

    Cliente findById(long id);
    List<Cliente> findAll();

}
