package br.com.dbc.Repository;

import br.com.dbc.Entity.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    Pagamento findById(long id);
    List<Pagamento> findAll();

}
