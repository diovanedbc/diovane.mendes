package br.com.dbc.Entity.Enuns;

public enum TipoPagamento {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA;
}
