package br.com.dbc.Entity;

import javax.persistence.*;

@Entity
@Table(name = "CLIENTES_PACOTES")
public class ClientePacote {

    @Id
    @GeneratedValue(generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CLIENTE", nullable = false)
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PACOTE", nullable = false)
    private Pacote pacote;

    @Column(name = "QUANTIDADE", nullable = false)
    private int quantidadePacote;

    @OneToOne(mappedBy = "clientePacote")
    private Pagamento pagamento;

    public ClientePacote() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public int getQuantidadePacote() {
        return quantidadePacote;
    }

    public void setQuantidadePacote(int quantidadePacote) {
        this.quantidadePacote = quantidadePacote;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }
}
