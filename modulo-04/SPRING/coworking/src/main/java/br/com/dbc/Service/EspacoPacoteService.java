package br.com.dbc.Service;

import br.com.dbc.Entity.EspacoPacote;
import br.com.dbc.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspacoPacoteService implements PadraoService<EspacoPacote>{

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    public EspacoPacote salvar(EspacoPacote espacoPacote) {
        return espacoPacoteRepository.save(espacoPacote);
    }

    public EspacoPacote buscarUm(long id) {
        return espacoPacoteRepository.findById(id);
    }

    public List<EspacoPacote> buscarTodos() {
        return espacoPacoteRepository.findAll();
    }

    public EspacoPacote atualizar(long id, EspacoPacote espacoPacote) {
        espacoPacote.setId(id);
        return salvar(espacoPacote);
    }

    public void excluir(long id) {
        espacoPacoteRepository.deleteById(id);
    }
}
