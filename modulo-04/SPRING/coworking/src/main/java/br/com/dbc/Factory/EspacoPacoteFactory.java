package br.com.dbc.Factory;

import br.com.dbc.DTO.EspacoPacoteDTO;
import br.com.dbc.Entity.EspacoPacote;

public abstract class EspacoPacoteFactory {

    public static EspacoPacote getInstance(EspacoPacoteDTO espacoPacoteDTO){
        EspacoPacote espacoPacote = new EspacoPacote();

        espacoPacote.setId( espacoPacoteDTO.getId() );
        espacoPacote.setEspaco( EspacoFactory.getInstance( espacoPacoteDTO.getEspaco() ) );
        espacoPacote.setPacote( PacoteFactory.getInstance( espacoPacoteDTO.getPacote() ) );
        espacoPacote.setTipoContratacao( espacoPacoteDTO.getTipoContratacao() );
        espacoPacote.setQuantidade( espacoPacoteDTO.getQuantidade() );
        espacoPacote.setPrazoEmDias( espacoPacoteDTO.getPrazoEmDias() );

        return espacoPacote;
    }

    public static EspacoPacoteDTO getInstance(EspacoPacote espacoPacote){
        EspacoPacoteDTO espacoPacoteDTO = new EspacoPacoteDTO();

        espacoPacoteDTO.setId( espacoPacote.getId() );
        espacoPacoteDTO.setEspaco( EspacoFactory.getInstance( espacoPacote.getEspaco() ) );
        espacoPacoteDTO.setPacote( PacoteFactory.getInstance( espacoPacote.getPacote() ) );
        espacoPacoteDTO.setTipoContratacao( espacoPacote.getTipoContratacao() );
        espacoPacoteDTO.setQuantidade( espacoPacote.getQuantidade() );
        espacoPacoteDTO.setPrazoEmDias( espacoPacote.getPrazoEmDias() );

        return espacoPacoteDTO;
    }
}
