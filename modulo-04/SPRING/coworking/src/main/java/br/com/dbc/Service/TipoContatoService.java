package br.com.dbc.Service;

import br.com.dbc.Entity.TipoContato;
import br.com.dbc.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoContatoService implements PadraoService<TipoContato>{

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    public TipoContato salvar(TipoContato tipoContato) {
        return tipoContatoRepository.save(tipoContato);
    }

    public TipoContato buscarUm(long id) {
        return tipoContatoRepository.findById(id);
    }

    public List<TipoContato> buscarTodos() {
        return tipoContatoRepository.findAll();
    }

    public TipoContato atualizar(long id, TipoContato tipoContato) {
        tipoContato.setId(id);
        return salvar(tipoContato);
    }

    public void excluir(long id) {
        tipoContatoRepository.deleteById(id);
    }
}
