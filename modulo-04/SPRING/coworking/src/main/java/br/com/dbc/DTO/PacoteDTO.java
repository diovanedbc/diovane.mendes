package br.com.dbc.DTO;

public class PacoteDTO {

    private long id;

    private double valor;

    public PacoteDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
