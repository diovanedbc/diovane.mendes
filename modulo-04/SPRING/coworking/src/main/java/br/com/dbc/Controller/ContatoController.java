package br.com.dbc.Controller;

import br.com.dbc.DTO.ContatoDTO;
import br.com.dbc.Entity.Contato;
import br.com.dbc.Factory.ContatoFactory;
import br.com.dbc.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/contato")
public class ContatoController implements PadraoController<ContatoDTO, Contato>{

    @Autowired
    private ContatoService contatoService;

    public ContatoDTO salvar(Contato contato) {
        return ContatoFactory.getInstance( contatoService.salvar(contato) );
    }

    public ContatoDTO buscarUm(long id) {
        return ContatoFactory.getInstance( contatoService.buscarUm(id) );
    }

    public List<ContatoDTO> buscarTodos() {
        return contatoService.buscarTodos()
                .stream()
                .map(ContatoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public ContatoDTO atualizar(long id, Contato contato) {
        return ContatoFactory.getInstance( contatoService.atualizar(id, contato) );
    }

    public void excluir(long id) {
        contatoService.excluir(id);
    }
}
