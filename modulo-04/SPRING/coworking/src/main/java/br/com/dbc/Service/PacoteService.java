package br.com.dbc.Service;

import br.com.dbc.Entity.Pacote;
import br.com.dbc.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacoteService implements PadraoService<Pacote>{

    @Autowired
    private PacoteRepository pacoteRepository;

    public Pacote salvar(Pacote pacote) {
        return pacoteRepository.save(pacote);
    }

    public Pacote buscarUm(long id) {
        return pacoteRepository.findById(id);
    }

    public List<Pacote> buscarTodos() {
        return pacoteRepository.findAll();
    }

    public Pacote atualizar(long id, Pacote pacote) {
        pacote.setId(id);
        return salvar(pacote);
    }

    public void excluir(long id) {
        pacoteRepository.deleteById(id);
    }
}
