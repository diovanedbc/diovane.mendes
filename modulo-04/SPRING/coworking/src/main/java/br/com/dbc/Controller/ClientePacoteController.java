package br.com.dbc.Controller;

import br.com.dbc.DTO.ClientePacoteDTO;
import br.com.dbc.Entity.ClientePacote;
import br.com.dbc.Factory.ClientePacoteFactory;
import br.com.dbc.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class ClientePacoteController implements PadraoController<ClientePacoteDTO, ClientePacote> {

    @Autowired
    private ClientePacoteService clientePacoteService;

    public ClientePacoteDTO salvar(ClientePacote clientePacote) {
        return ClientePacoteFactory.getInstance( clientePacoteService.salvar(clientePacote) );
    }

    public ClientePacoteDTO buscarUm(long id) {
        return ClientePacoteFactory.getInstance( clientePacoteService.buscarUm(id) );
    }

    public List<ClientePacoteDTO> buscarTodos() {
        return clientePacoteService.buscarTodos()
                .stream()
                .map(ClientePacoteFactory::getInstance)
                .collect(Collectors.toList());
    }

    public ClientePacoteDTO atualizar(long id, ClientePacote clientePacote) {
        return ClientePacoteFactory.getInstance( clientePacoteService.atualizar(id, clientePacote) );
    }

    public void excluir(long id) {
        clientePacoteService.excluir(id);
    }
}
