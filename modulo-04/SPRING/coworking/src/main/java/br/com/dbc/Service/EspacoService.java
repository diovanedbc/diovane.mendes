package br.com.dbc.Service;

import br.com.dbc.Entity.Espaco;
import br.com.dbc.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspacoService implements PadraoService<Espaco>{

    @Autowired
    private EspacoRepository espacoRepository;

    public Espaco salvar(Espaco espaco) {
        return espacoRepository.save(espaco);
    }

    public Espaco buscarUm(long id) {
        return espacoRepository.findById(id);
    }

    public List<Espaco> buscarTodos() {
        return espacoRepository.findAll();
    }

    public Espaco atualizar(long id, Espaco espaco) {
        espaco.setId(id);
        return salvar(espaco);
    }

    public void excluir(long id) {
        espacoRepository.deleteById(id);
    }
}
