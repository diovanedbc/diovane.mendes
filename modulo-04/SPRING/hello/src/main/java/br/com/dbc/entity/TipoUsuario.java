package br.com.dbc.entity;

import javax.persistence.*;

@Entity
@Table(name = "TIPO_USUARIOS")
public class TipoUsuario {

    @Id
    @GeneratedValue(generator = "TIPO_USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME")
    private String nome;

    @OneToOne(mappedBy = "tipoUsuario")
    private Usuario usuario;

    @OneToOne(mappedBy = "tipoUsuario")
    private Regra regra;

    public TipoUsuario() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Regra getRegra() {
        return regra;
    }

    public void setRegra(Regra regra) {
        this.regra = regra;
    }
}
