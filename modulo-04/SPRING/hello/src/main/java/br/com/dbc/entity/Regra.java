package br.com.dbc.entity;

import javax.persistence.*;

@Entity
@Table(name = "REGRAS")
public class Regra {

    @Id
    @GeneratedValue(generator = "REGRA_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "DESCRICAO")
    private String descricao;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_TIPO_USUARIO")
    private TipoUsuario tipoUsuario;

    @OneToOne(mappedBy = "regra")
    private Solicitacao solicitacao;

    public Regra() {
    }

    @PrePersist
    public void usuarioParaSolicitacao(){
        if ( this.solicitacao.getValor() <= 10.000 ){
            this.tipoUsuario.setNome("Gerente");
        } else {
            this.tipoUsuario.setNome("Gerente Geral");
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Solicitacao getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(Solicitacao solicitacao) {
        this.solicitacao = solicitacao;
    }
}
