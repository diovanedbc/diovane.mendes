package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "CONTATOS")
public class Contato {

    @Id
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "VALOR")
    private double valor;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONTATO_CLIENTE",
            joinColumns = {
                    @JoinColumn(name = "ID_CONTATO")},
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_CLIENTE")})
    private List<Cliente> clientes = new ArrayList<>();

    public Contato() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void pushClientes(Cliente... clientes) {
        this.clientes.addAll( Arrays.asList(clientes) );
    }
}
