package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BANCOS")
public class Banco {
    @Id
    @GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "CODIGO")
    private long codigo;

    @Column(name = "NOME")
    private String nome;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "banco")
    private List<Agencia> agencias = new ArrayList<>();

    public Banco() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
