package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "DOCUMENTOS")
public class Documento {

    @Id
    @GeneratedValue(generator = "DOCUMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "VALOR")
    private String valor;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "DOCUMENTO_CLIENTE",
            joinColumns = {
                    @JoinColumn(name = "ID_DOCUMENTO")},
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_CLIENTE")})
    private List<Cliente> clientes = new ArrayList<>();

    public Documento() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void pushClientes(Cliente... clientes) {
        this.clientes.addAll( Arrays.asList(clientes) );
    }
}
