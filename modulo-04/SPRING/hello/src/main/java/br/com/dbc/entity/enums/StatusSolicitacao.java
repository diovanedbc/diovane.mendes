package br.com.dbc.entity.enums;

public enum StatusSolicitacao {
    APROVADO,
    NEGADO,
    PENDENTE;
}
