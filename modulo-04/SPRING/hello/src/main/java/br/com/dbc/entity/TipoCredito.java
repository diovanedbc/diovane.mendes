package br.com.dbc.entity;

import javax.persistence.*;

@Entity
@Table(name = "TIPO_CREDITOS")
public class TipoCredito {
    @Id
    @GeneratedValue(generator = "TIPO_CREDITO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "VALOR")
    private double valor;

    @ManyToOne
    @JoinColumn(name = "ID_CREDITO_PRE_APROVADO")
    private CreditoPreAprovado creditoPreAprovado;

    public TipoCredito() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public CreditoPreAprovado getCreditoPreAprovado() {
        return creditoPreAprovado;
    }

    public void setCreditoPreAprovado(CreditoPreAprovado creditoPreAprovado) {
        this.creditoPreAprovado = creditoPreAprovado;
    }
}
