package br.com.dbc.entity;

import javax.persistence.*;

@Entity
@Table(name = "EMPRESTIMOS")
public class Emprestimo {

    @Id
    @GeneratedValue(generator = "EMPRESTIMO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "VALOR")
    private double valor;

    @ManyToOne
    @JoinColumn(name = "ID_CREDITO_PRE_APROVADO")
    private CreditoPreAprovado creditoPreAprovado;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_SOLICITACAO")
    private Solicitacao solicitacao;

    public Emprestimo() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public CreditoPreAprovado getCreditoPreAprovado() {
        return creditoPreAprovado;
    }

    public void setCreditoPreAprovado(CreditoPreAprovado creditoPreAprovado) {
        this.creditoPreAprovado = creditoPreAprovado;
    }

    public Solicitacao getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(Solicitacao solicitacao) {
        this.solicitacao = solicitacao;
    }
}
