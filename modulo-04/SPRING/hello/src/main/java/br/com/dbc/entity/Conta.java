package br.com.dbc.entity;

import br.com.dbc.entity.enums.TipoConta;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "CONTAS")
public class Conta {

    @Id
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO")
    private TipoConta tipoConta;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Cliente cliente;

    @Column(name = "SALDO")
    private double saldo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conta")
    private List<CreditoPreAprovado> creditoPreAprovados = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conta")
    private List<Movimentacao> movimentacoes = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conta")
    private List<Solicitacao> solicitacoes = new ArrayList<>();

    public Conta() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<CreditoPreAprovado> getContas() {
        return creditoPreAprovados;
    }

    public void pushContas(CreditoPreAprovado... creditoPreAprovados) {
        this.creditoPreAprovados.addAll( Arrays.asList(creditoPreAprovados) );
    }

    public List<Movimentacao> getMovimentacoes() {
        return movimentacoes;
    }

    public void pushMovimentacoes(Movimentacao... movimentacoes) {
        this.movimentacoes.addAll( Arrays.asList(movimentacoes) );
    }

    public List<Solicitacao> getSolicitacoes() {
        return solicitacoes;
    }

    public void pushSolicitacoes(Solicitacao... solicitacoes) {
        this.solicitacoes.addAll( Arrays.asList(solicitacoes) );
    }
}
