package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "AGENCIAS")
public class Agencia {

    @Id
    @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CODIGO")
    private long codigo;

    @ManyToOne
    @JoinColumn(name = "ID_BANCO")
    private Banco banco;

    @OneToOne
    @JoinColumn(name = "ID_CONTA")
    private Conta conta;

    @OneToOne(mappedBy = "agencia")
    private Endereco endereco;

    @OneToMany(mappedBy = "agencia", cascade = CascadeType.ALL)
    private List<Usuario> usuarios = new ArrayList<>();

    public Agencia() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void pushUsuarios(Usuario... usuarios) {
        this.usuarios.addAll( Arrays.asList(usuarios) );
    }
}
