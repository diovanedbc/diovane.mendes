package br.com.dbc.entity;

import br.com.dbc.entity.enums.StatusSolicitacao;

import javax.persistence.*;

@Entity
@Table(name = "SOLICITACOES")
public class Solicitacao {

    @Id
    @GeneratedValue(generator = "SOLICITACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @OneToOne(mappedBy = "solicitacao")
    private Emprestimo emprestimo;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "STATUS")
    private StatusSolicitacao statusSolicitacao = StatusSolicitacao.PENDENTE;

    @Column(name = "VALOR")
    private double valor;

    @ManyToOne
    @JoinColumn(name = "ID_CONTA")
    private Conta conta;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_SOLICITACAO")
    private Regra regra;

    @ManyToOne
    @JoinColumn(name = "ID_USUARIO")
    private Usuario usuario;

    public Solicitacao() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Emprestimo getEmprestimo() {
        return emprestimo;
    }

    public void setEmprestimo(Emprestimo emprestimo) {
        this.emprestimo = emprestimo;
    }

    public StatusSolicitacao getStatusSolicitacao() {
        return statusSolicitacao;
    }

    public void setStatusSolicitacao(StatusSolicitacao statusSolicitacao) {
        this.statusSolicitacao = statusSolicitacao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public Regra getRegra() {
        return regra;
    }

    public void setRegra(Regra regra) {
        this.regra = regra;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
