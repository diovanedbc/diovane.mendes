package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "ENDERECOS")
public class Endereco {

    @Id
    @GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "CEP")
    private long cep;

    @Column(name = "CIDADE")
    private String cidade;

    @Column(name = "BAIRRO")
    private String bairro;

    @Column(name = "RUA")
    private String rua;

    @Column(name = "NUMERO")
    private int numero;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_AGENCIA")
    private Agencia agencia;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ENDERECO_CLIENTE",
            joinColumns = {
                    @JoinColumn(name = "ID_ENDERECO")},
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_CLIENTE")})
    private List<Cliente> clientes = new ArrayList<>();

    public Endereco() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCep() {
        return cep;
    }

    public void setCep(long cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void pushClientes(Cliente... clientes) {
        this.clientes.addAll( Arrays.asList(clientes) );
    }
}
