package br.com.dbc.dto;

import br.com.dbc.entity.TipoUsuario;

public class RegraDTO {

    private String descricao;

    private TipoUsuario tipoUsuario;

    public RegraDTO() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
}