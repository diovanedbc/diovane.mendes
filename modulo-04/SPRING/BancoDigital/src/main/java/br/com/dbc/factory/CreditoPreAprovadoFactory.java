package br.com.dbc.factory;

import br.com.dbc.dto.CreditoPreAprovadoDTO;
import br.com.dbc.entity.CreditoPreAprovado;

public abstract class CreditoPreAprovadoFactory {

    public static CreditoPreAprovadoDTO getInstance(CreditoPreAprovado creditoPreAprovado){
        CreditoPreAprovadoDTO creditoPreAprovadoDTO = new CreditoPreAprovadoDTO();

        creditoPreAprovadoDTO.setContaDTO( ContaFactory.getInstance(creditoPreAprovado.getConta()) );

        return creditoPreAprovadoDTO;
    }

    public static CreditoPreAprovado getInstance(CreditoPreAprovadoDTO creditoPreAprovadoDTO){
        CreditoPreAprovado creditoPreAprovado = new CreditoPreAprovado();

        creditoPreAprovado.setConta( ContaFactory.getInstance(creditoPreAprovadoDTO.getContaDTO()) );

        return creditoPreAprovado;
    }
}