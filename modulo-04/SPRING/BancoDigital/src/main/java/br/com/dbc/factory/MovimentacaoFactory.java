package br.com.dbc.factory;

import br.com.dbc.dto.MovimentacaoDTO;
import br.com.dbc.entity.Movimentacao;

public abstract class MovimentacaoFactory {

    public static MovimentacaoDTO getInstance(Movimentacao movimentacao){
        MovimentacaoDTO movimentacaoDTO = new MovimentacaoDTO();

        movimentacaoDTO.setDeposito( movimentacao.getDeposito() );
        movimentacaoDTO.setPagamento( movimentacao.getPagamento() );
        movimentacaoDTO.setSaque( movimentacao.getSaque() );
        movimentacaoDTO.setTransferencia( movimentacao.getTransferencia() );
        movimentacaoDTO.setContaDTO( ContaFactory.getInstance(movimentacao.getConta()) );
        movimentacaoDTO.setEmprestimoDTO( EmprestimoFactory.getInstance(movimentacao.getEmprestimo()) );

        return movimentacaoDTO;
    }

    public static Movimentacao getInstance(MovimentacaoDTO movimentacaoDTO){
        Movimentacao movimentacao = new Movimentacao();

        movimentacao.setDeposito( movimentacaoDTO.getDeposito() );
        movimentacao.setPagamento( movimentacaoDTO.getPagamento() );
        movimentacao.setSaque( movimentacaoDTO.getSaque() );
        movimentacao.setTransferencia( movimentacaoDTO.getTransferencia() );
        movimentacao.setConta( ContaFactory.getInstance(movimentacaoDTO.getContaDTO()) );
        movimentacao.setEmprestimo( EmprestimoFactory.getInstance(movimentacaoDTO.getEmprestimoDTO()) );

        return movimentacao;
    }
}