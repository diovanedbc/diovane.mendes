package br.com.dbc.repository;

import br.com.dbc.entity.Conta;
import br.com.dbc.entity.CreditoPreAprovado;
import br.com.dbc.entity.Emprestimo;
import br.com.dbc.entity.TipoCredito;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CreditoPreAprovadoRepository extends CrudRepository<CreditoPreAprovado, Long> {

//    CreditoPreAprovado findByTipoCredito(TipoCredito tipoCredito);
    List<CreditoPreAprovado> findByTipoCreditos(List<TipoCredito> tipoCreditos);
    CreditoPreAprovado findByConta(Conta conta);
//    CreditoPreAprovado findByEmprestimo(Emprestimo emprestimo);
    List<CreditoPreAprovado> findByEmprestimos(List<Emprestimo> emprestimos);

}
