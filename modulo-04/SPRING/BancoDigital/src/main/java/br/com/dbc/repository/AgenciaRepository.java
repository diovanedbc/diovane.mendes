package br.com.dbc.repository;

import br.com.dbc.entity.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AgenciaRepository extends CrudRepository<Agencia, Long> {

    Agencia findByNome(String nome);
    Agencia findByCodigo(long codigo);
    Agencia findByBanco(Banco banco);
//    Agencia findbyConta(Conta conta);
    Agencia findByEndereco(Endereco endereco);
//    Agencia findByUsuario(Usuario usuario);
    List<Agencia> findByUsuarios(List<Usuario> usuarios);
}
