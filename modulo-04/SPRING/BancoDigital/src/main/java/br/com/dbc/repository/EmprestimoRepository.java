package br.com.dbc.repository;

import br.com.dbc.entity.CreditoPreAprovado;
import br.com.dbc.entity.Emprestimo;
import br.com.dbc.entity.Solicitacao;
import org.springframework.data.repository.CrudRepository;

public interface EmprestimoRepository extends CrudRepository<Emprestimo, Long> {

    Emprestimo findByValor(double valor);
    Emprestimo findByCreditoPreAprovado(CreditoPreAprovado creditoPreAprovado);
    Emprestimo findBySolicitacao(Solicitacao solicitacao);

}
