package br.com.dbc.factory;

import br.com.dbc.dto.EmprestimoDTO;
import br.com.dbc.entity.Emprestimo;

public abstract class EmprestimoFactory {

    public static EmprestimoDTO getInstance(Emprestimo emprestimo){
        EmprestimoDTO emprestimoDTO = new EmprestimoDTO();

        emprestimoDTO.setValor( emprestimo.getValor() );
        emprestimoDTO.setCreditoPreAprovadoDTO( CreditoPreAprovadoFactory.getInstance(emprestimo.getCreditoPreAprovado()) );
        emprestimoDTO.setSolicitacaoDTO( SolicitacaoFactory.getInstance(emprestimo.getSolicitacao()) );

        return emprestimoDTO;
    }

    public static Emprestimo getInstance(EmprestimoDTO emprestimoDTO){
        Emprestimo emprestimo = new Emprestimo();

        emprestimo.setValor( emprestimo.getValor() );
        emprestimo.setCreditoPreAprovado( CreditoPreAprovadoFactory.getInstance(emprestimoDTO.getCreditoPreAprovadoDTO()) );
        emprestimo.setSolicitacao( SolicitacaoFactory.getInstance(emprestimoDTO.getSolicitacaoDTO()) );

        return emprestimo;
    }
}