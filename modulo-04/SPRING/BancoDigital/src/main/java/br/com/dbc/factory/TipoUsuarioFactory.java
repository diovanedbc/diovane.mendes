package br.com.dbc.factory;

import br.com.dbc.dto.TipoUsuarioDTO;
import br.com.dbc.entity.TipoUsuario;

public abstract class TipoUsuarioFactory {

    public static TipoUsuarioDTO getInstance(TipoUsuario tipoUsuario){
        TipoUsuarioDTO tipoUsuarioDTO = new TipoUsuarioDTO();

        tipoUsuarioDTO.setNome( tipoUsuario.getNome() );

        return tipoUsuarioDTO;
    }

    public static TipoUsuario getInstance(TipoUsuarioDTO tipoUsuarioDTO){
        TipoUsuario tipoUsuario = new TipoUsuario();

        tipoUsuario.setNome( tipoUsuarioDTO.getNome() );

        return tipoUsuario;
    }
}