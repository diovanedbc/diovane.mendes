package br.com.dbc.controller;

import br.com.dbc.entity.Agencia;
import br.com.dbc.service.interfaces.AgenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/agencia")
public class AgenciaController {

	@Autowired
	private AgenciaService agenciaService;

	@PostMapping("/salvar")
	@ResponseBody
	public Agencia hello(Model model, @RequestBody Agencia agencia) {
		model.addAttribute("mensagem", "Agência salva!");

//		Agencia agencia = new Agencia();
//		agencia.setNome("Eldorado do Sul");
//		agencia.setCodigo(123);

//		return "index";

		return agenciaService.salvar(agencia);
	}

	@DeleteMapping("/deletar/{id}")
	public String deletado(Model model, @PathVariable long id){
		model.addAttribute("mensagem", "Agência deletada!");
		agenciaService.remover(id);
		return "index";
	}

	@GetMapping("/buscar/{id}")
	@ResponseBody
	public Agencia buscaPorId(@PathVariable long id){
		return agenciaService.buscar(id);
	}

	@PutMapping("/editar/{id}")
	@ResponseBody
	public Agencia editar(@PathVariable long id, @RequestBody Agencia agencia){
		return agenciaService.editar(id, agencia);
	}
}