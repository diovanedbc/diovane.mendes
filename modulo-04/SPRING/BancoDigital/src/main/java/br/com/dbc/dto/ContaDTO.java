package br.com.dbc.dto;

import br.com.dbc.entity.enums.TipoConta;

public class ContaDTO {

    private TipoConta tipoConta;

    private ClienteDTO clienteDTO;

    private double saldo;

    public ContaDTO() {
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    public ClienteDTO getClienteDTO() {
        return clienteDTO;
    }

    public void setClienteDTO(ClienteDTO clienteDTO) {
        this.clienteDTO = clienteDTO;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
