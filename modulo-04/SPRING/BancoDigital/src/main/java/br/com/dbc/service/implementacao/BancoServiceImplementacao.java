package br.com.dbc.service.implementacao;

import br.com.dbc.entity.Banco;
import br.com.dbc.repository.BancoRepository;
import br.com.dbc.service.interfaces.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BancoServiceImplementacao implements BancoService {

    @Autowired
    BancoRepository bancoRepository;

    @Transactional(rollbackFor = Exception.class)
    public Banco salvar(Banco banco) {
        return bancoRepository.save(banco);
    }

    public Banco buscar(long id) {
        return bancoRepository
                .findById(id)
                .orElse(null);
    }

    @Transactional(rollbackFor = Exception.class)
    public Banco editar(long id, Banco banco) {
        banco.setId(id);
        return bancoRepository.save(banco);
    }

    public void remover(Banco banco) {
        bancoRepository.delete(banco);
    }

    public void remover(long id) {
        bancoRepository.deleteById(id);
    }
}
