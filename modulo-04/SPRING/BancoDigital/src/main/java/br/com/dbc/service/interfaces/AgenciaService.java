package br.com.dbc.service.interfaces;

import br.com.dbc.entity.Agencia;

public interface AgenciaService {

    Agencia salvar(Agencia agencia);
    Agencia buscar(long id);
    Agencia editar(long id, Agencia agencia);
    void remover(Agencia agencia);
    void remover(long id);
}
