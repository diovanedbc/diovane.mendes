package br.com.dbc.dto;

import br.com.dbc.entity.enums.StatusSolicitacao;

public class SolicitacaoDTO {

    private EmprestimoDTO emprestimoDTO;

    private StatusSolicitacao statusSolicitacao;

    private double valor;

    private ContaDTO contaDTO;

    private RegraDTO regraDTO;

    private UsuarioDTO usuarioDTO;

    public SolicitacaoDTO() {
    }

    public EmprestimoDTO getEmprestimoDTO() {
        return emprestimoDTO;
    }

    public void setEmprestimoDTO(EmprestimoDTO emprestimoDTO) {
        this.emprestimoDTO = emprestimoDTO;
    }

    public StatusSolicitacao getStatusSolicitacao() {
        return statusSolicitacao;
    }

    public void setStatusSolicitacao(StatusSolicitacao statusSolicitacao) {
        this.statusSolicitacao = statusSolicitacao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public ContaDTO getContaDTO() {
        return contaDTO;
    }

    public void setContaDTO(ContaDTO contaDTO) {
        this.contaDTO = contaDTO;
    }

    public RegraDTO getRegraDTO() {
        return regraDTO;
    }

    public void setRegraDTO(RegraDTO regraDTO) {
        this.regraDTO = regraDTO;
    }

    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }
}
