package br.com.dbc.repository;

import br.com.dbc.entity.Regra;
import br.com.dbc.entity.TipoUsuario;
import br.com.dbc.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface TipoUsuarioRepository extends CrudRepository<TipoUsuario, Long> {

    TipoUsuario findByNome(String nome);
    TipoUsuario findByUsuario(Usuario usuario);
    TipoUsuario findByRegra(Regra regra);

}
