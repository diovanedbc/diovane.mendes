package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "CREDITO_PRE_APROVADOS")
public class CreditoPreAprovado {
    @Id
    @GeneratedValue(generator = "CREDITO_PRE_APROVADO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @OneToMany(mappedBy = "creditoPreAprovado", cascade = CascadeType.ALL)
    private List<TipoCredito> tipoCreditos = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "ID_CONTA")
    private Conta conta;

    @OneToMany(mappedBy = "creditoPreAprovado", cascade = CascadeType.ALL)
    private List<Emprestimo> emprestimos = new ArrayList<>();

    public CreditoPreAprovado() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<TipoCredito> getTipoCredito() {
        return tipoCreditos;
    }

    public void pushTipoCredito(TipoCredito... tipoCreditos) {
        this.tipoCreditos.addAll( Arrays.asList(tipoCreditos) );
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public List<Emprestimo> getEmprestimos() {
        return emprestimos;
    }

    public void pushEmprestimos(Emprestimo... emprestimos) {
        this.emprestimos.addAll( Arrays.asList(emprestimos) );
    }
}
