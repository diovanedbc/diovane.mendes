package br.com.dbc.dto;

public class CreditoPreAprovadoDTO {

    private ContaDTO contaDTO;

    public CreditoPreAprovadoDTO() {
    }

    public ContaDTO getContaDTO() {
        return contaDTO;
    }

    public void setContaDTO(ContaDTO contaDTO) {
        this.contaDTO = contaDTO;
    }
}
