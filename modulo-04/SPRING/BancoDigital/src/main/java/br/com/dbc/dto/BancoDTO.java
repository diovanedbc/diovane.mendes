package br.com.dbc.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BancoDTO {

    private long codigo;

    private String nome;

    private List<AgenciaDTO> agenciaDTOS = new ArrayList<>();

    public BancoDTO() {
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<AgenciaDTO> getAgenciaDTOS() {
        return agenciaDTOS;
    }

    public void pushAgenciaDTOS(AgenciaDTO... agenciaDTOS) {
        this.agenciaDTOS.addAll( Arrays.asList(agenciaDTOS) );
    }
}
