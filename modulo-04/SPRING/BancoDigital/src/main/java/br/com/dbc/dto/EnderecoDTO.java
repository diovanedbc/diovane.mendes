package br.com.dbc.dto;

public class EnderecoDTO {

    private long cep;

    private String cidade;

    private String bairro;

    private String rua;

    private int numero;

    private AgenciaDTO agenciaDTO;

    public EnderecoDTO() {

    }

    public long getCep() {
        return cep;
    }

    public void setCep(long cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public AgenciaDTO getAgenciaDTO() {
        return agenciaDTO;
    }

    public void setAgenciaDTO(AgenciaDTO agenciaDTO) {
        this.agenciaDTO = agenciaDTO;
    }
}
