package br.com.dbc.repository;

import br.com.dbc.entity.Cliente;
import br.com.dbc.entity.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Long> {

    Contato findByTipo(String tipo);
    Contato findByValor(double valor);
    List<Contato> findByClientes(List<Cliente> clientes);
    List<Contato> findByClientes(Cliente clientes);
}
