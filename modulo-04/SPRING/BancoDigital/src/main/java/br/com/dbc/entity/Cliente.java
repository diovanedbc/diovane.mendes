package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "CLIENTES")
public class Cliente {

    @Id
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME")
    private String nome;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private List<Conta> contas = new ArrayList<>();

    @ManyToMany(mappedBy = "clientes")
    private List<Endereco> enderecos = new ArrayList<>();

    @ManyToMany(mappedBy = "clientes")
    private List<Contato> contatos = new ArrayList<>();

    @ManyToMany(mappedBy = "clientes")
    private List<Documento> documentos = new ArrayList<>();

    public Cliente() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Conta> getConta() {
        return contas;
    }

    public void pushConta(Conta... contas) {
        this.contas.addAll( Arrays.asList(contas) );
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        this.enderecos.addAll( Arrays.asList(enderecos) );
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll( Arrays.asList(contatos) );
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void pushDocumentos(Documento... documentos) {
        this.documentos.addAll( Arrays.asList(documentos) );
    }
}
