package br.com.dbc.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "USUARIOS")
public class Usuario {

    @Id
    @GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME")
    private String nome;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_TIPO_USUARIO")
    private TipoUsuario tipoUsuario;

    @ManyToOne
    @JoinColumn(name = "ID_AGENCIA")
    private Agencia agencia;

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
    private List<Solicitacao> solicitacoes = new ArrayList<>();

    public Usuario() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public List<Solicitacao> getSolicitacoes() {
        return solicitacoes;
    }

    public void pushSolicitacoes(Solicitacao... solicitacoes) {
        this.solicitacoes.addAll( Arrays.asList(solicitacoes) );
    }
}
