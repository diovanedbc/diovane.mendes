package br.com.dbc.factory;

import br.com.dbc.dto.UsuarioDTO;
import br.com.dbc.entity.Usuario;

public abstract class UsuarioFactory {

    public static UsuarioDTO getInstance(Usuario usuario){
        UsuarioDTO usuarioDTO = new UsuarioDTO();

        usuarioDTO.setNome( usuario.getNome() );
        usuarioDTO.setAgenciaDTO( AgenciaFactory.getInstance(usuario.getAgencia()) );
        usuarioDTO.setTipoUsuarioDTO( TipoUsuarioFactory.getInstance(usuario.getTipoUsuario()) );

        return usuarioDTO;
    }

    public static Usuario getInstance(UsuarioDTO usuarioDTO){
        Usuario usuario = new Usuario();

        usuario.setNome( usuarioDTO.getNome() );
        usuario.setAgencia( AgenciaFactory.getInstance(usuarioDTO.getAgenciaDTO()) );
        usuario.setTipoUsuario( TipoUsuarioFactory.getInstance(usuarioDTO.getTipoUsuarioDTO()) );

        return usuario;
    }
}