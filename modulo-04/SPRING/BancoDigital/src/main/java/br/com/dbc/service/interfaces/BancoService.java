package br.com.dbc.service.interfaces;

import br.com.dbc.entity.Banco;

public interface BancoService {

    Banco salvar(Banco agencia);
    Banco buscar(long id);
    Banco editar(long id, Banco agencia);
    void remover(Banco agencia);
    void remover(long id);

}