package br.com.dbc.repository;

import br.com.dbc.entity.Regra;
import br.com.dbc.entity.Solicitacao;
import br.com.dbc.entity.TipoUsuario;
import org.springframework.data.repository.CrudRepository;

public interface RegraRepository extends CrudRepository<Regra, Long> {

    Regra findByDescricao(String descricao);
    Regra findByTipoUsuario(TipoUsuario tipoUsuario);
    Regra findBySolicitacao(Solicitacao solicitacao);

}
