package br.com.dbc.repository;

import br.com.dbc.entity.Agencia;
import br.com.dbc.entity.Cliente;
import br.com.dbc.entity.Endereco;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EnderecoRepository extends CrudRepository<Endereco, Long> {

    Endereco findByCep(long cep);
    Endereco findByCidade(String cidade);
    Endereco findByBairro(String bairro);
    Endereco findByRua(String rua);
    Endereco findByNumero(int numero);
    Endereco findByAgencia(Agencia agencia);
    List<Endereco> findByClientes(Cliente cliente);
    List<Endereco> findByClientes(List<Cliente> clientes);

}
