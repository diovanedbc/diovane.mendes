package br.com.dbc.repository;

import br.com.dbc.entity.Cliente;
import br.com.dbc.entity.Documento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DocumentoRepository extends CrudRepository<Documento, Long> {

    Documento findByTipo(String tipo);
    Documento findByValor(String valor);
    List<Documento> findByClientes(Cliente cliente);
    List<Documento> findByClientes(List<Cliente> clientes);

}
