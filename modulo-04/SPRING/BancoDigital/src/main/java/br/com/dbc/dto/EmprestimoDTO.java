package br.com.dbc.dto;

public class EmprestimoDTO {

    private double valor;

    private CreditoPreAprovadoDTO creditoPreAprovadoDTO;

    private SolicitacaoDTO solicitacaoDTO;

    public EmprestimoDTO() {
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public CreditoPreAprovadoDTO getCreditoPreAprovadoDTO() {
        return creditoPreAprovadoDTO;
    }

    public void setCreditoPreAprovadoDTO(CreditoPreAprovadoDTO creditoPreAprovadoDTO) {
        this.creditoPreAprovadoDTO = creditoPreAprovadoDTO;
    }

    public SolicitacaoDTO getSolicitacaoDTO() {
        return solicitacaoDTO;
    }

    public void setSolicitacaoDTO(SolicitacaoDTO solicitacaoDTO) {
        this.solicitacaoDTO = solicitacaoDTO;
    }
}
