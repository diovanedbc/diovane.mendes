package br.com.dbc.factory;

import br.com.dbc.dto.ContaDTO;
import br.com.dbc.entity.Conta;

public abstract class ContaFactory {

    public static ContaDTO getInstance(Conta conta){
        ContaDTO contaDTO = new ContaDTO();

        contaDTO.setTipoConta( conta.getTipoConta() );
        contaDTO.setClienteDTO( ClienteFactory.getInstance(conta.getCliente()) );
        contaDTO.setSaldo( conta.getSaldo() );

        return contaDTO;
    }

    public static Conta getInstance(ContaDTO contaDTO){
        Conta conta = new Conta();

        conta.setTipoConta( contaDTO.getTipoConta() );
        conta.setCliente( ClienteFactory.getInstance(contaDTO.getClienteDTO()) );
        conta.setSaldo( contaDTO.getSaldo() );

        return conta;
    }
}
