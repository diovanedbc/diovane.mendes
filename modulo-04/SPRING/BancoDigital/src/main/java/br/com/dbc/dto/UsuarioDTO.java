package br.com.dbc.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsuarioDTO {

    private String nome;

    private TipoUsuarioDTO tipoUsuarioDTO;

    private AgenciaDTO agenciaDTO;

    private List<SolicitacaoDTO> solicitacaoDTOS = new ArrayList<>();

    public UsuarioDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoUsuarioDTO getTipoUsuarioDTO() {
        return tipoUsuarioDTO;
    }

    public void setTipoUsuarioDTO(TipoUsuarioDTO tipoUsuarioDTO) {
        this.tipoUsuarioDTO = tipoUsuarioDTO;
    }

    public AgenciaDTO getAgenciaDTO() {
        return agenciaDTO;
    }

    public void setAgenciaDTO(AgenciaDTO agenciaDTO) {
        this.agenciaDTO = agenciaDTO;
    }

    public List<SolicitacaoDTO> getSolicitacaoDTOS() {
        return solicitacaoDTOS;
    }

    public void pushSolicitacaoDTOS(SolicitacaoDTO... solicitacaoDTOS) {
        this.solicitacaoDTOS.addAll( Arrays.asList(solicitacaoDTOS) );
    }
}
