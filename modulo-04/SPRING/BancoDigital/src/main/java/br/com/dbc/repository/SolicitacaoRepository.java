package br.com.dbc.repository;

import br.com.dbc.entity.*;
import br.com.dbc.entity.enums.StatusSolicitacao;
import org.springframework.data.repository.CrudRepository;

public interface SolicitacaoRepository extends CrudRepository<Solicitacao, Long> {

    Solicitacao findByEmprestimo(Emprestimo emprestimo);
    Solicitacao findByStatusSolicitacao(StatusSolicitacao statusSolicitacao);
    Solicitacao findByValor(double valor);
    Solicitacao findByConta(Conta conta);
    Solicitacao findByRegra(Regra regra);
    Solicitacao findByUsuario(Usuario usuario);

}
