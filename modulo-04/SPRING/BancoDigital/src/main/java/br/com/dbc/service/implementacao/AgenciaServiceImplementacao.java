package br.com.dbc.service.implementacao;

import br.com.dbc.entity.Agencia;
import br.com.dbc.repository.AgenciaRepository;
import br.com.dbc.service.interfaces.AgenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AgenciaServiceImplementacao implements AgenciaService {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Transactional(rollbackFor = Exception.class)
    public Agencia salvar(Agencia agencia) {
        return agenciaRepository.save(agencia);
    }

    public Agencia buscar(long id) {
        return agenciaRepository
                .findById(id)
                .orElse(null);
    }

    @Transactional(rollbackFor = Exception.class)
    public Agencia editar(long id, Agencia agencia) {
        agencia.setId(id);
        return agenciaRepository.save(agencia);
    }

    public void remover(Agencia agencia) {
        agenciaRepository.delete(agencia);
    }

    public void remover(long id) {
        agenciaRepository.deleteById(id);
    }
}
