package br.com.dbc.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DocumentoDTO {

    private String tipo;

    private String valor;

    private List<ClienteDTO> clienteDTOS = new ArrayList<>();

    public DocumentoDTO() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<ClienteDTO> getClienteDTOS() {
        return clienteDTOS;
    }

    public void pushClienteDTOS(ClienteDTO... clienteDTOS) {
        this.clienteDTOS.addAll( Arrays.asList(clienteDTOS) );
    }
}
