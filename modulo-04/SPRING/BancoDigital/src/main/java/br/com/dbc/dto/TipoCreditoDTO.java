package br.com.dbc.dto;

public class TipoCreditoDTO {

    private String nome;

    private double valor;

    private CreditoPreAprovadoDTO creditoPreAprovadoDTO;

    public TipoCreditoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public CreditoPreAprovadoDTO getCreditoPreAprovadoDTO() {
        return creditoPreAprovadoDTO;
    }

    public void setCreditoPreAprovadoDTO(CreditoPreAprovadoDTO creditoPreAprovadoDTO) {
        this.creditoPreAprovadoDTO = creditoPreAprovadoDTO;
    }
}
