package br.com.dbc.repository;

import br.com.dbc.entity.Conta;
import br.com.dbc.entity.Emprestimo;
import br.com.dbc.entity.Movimentacao;
import org.springframework.data.repository.CrudRepository;

public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Long> {

    Movimentacao findByConta(Conta conta);
    Movimentacao findBySaque(double saque);
    Movimentacao findByDeposito(double deposito);
    Movimentacao findByTransferencia(double transferencia);
    Movimentacao findByPagamento(double pagamento);
    Movimentacao findByEmprestimo(Emprestimo emprestimo);

}
