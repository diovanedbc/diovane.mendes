package br.com.dbc.entity;

import javax.persistence.*;

@Entity
@Table(name = "MOVIMENTACOES")
public class Movimentacao {

    @Id
    @GeneratedValue(generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CONTA")
    private Conta conta;

    @Column(name = "SAQUE")
    private double saque;

    @Column(name = "DEPOSITO")
    private double deposito;

    @Column(name = "TRANSFERENCIA")
    private double transferencia;

    @Column(name = "PAGAMENTO")
    private double pagamento;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_EMPRESTIMO")
    private Emprestimo emprestimo;

    public Movimentacao() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public double getSaque() {
        return saque;
    }

    public void setSaque(double saque) {
        this.saque = saque;
    }

    public double getDeposito() {
        return deposito;
    }

    public void setDeposito(double deposito) {
        this.deposito = deposito;
    }

    public double getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(double transferencia) {
        this.transferencia = transferencia;
    }

    public double getPagamento() {
        return pagamento;
    }

    public void setPagamento(double pagamento) {
        this.pagamento = pagamento;
    }

    public Emprestimo getEmprestimo() {
        return emprestimo;
    }

    public void setEmprestimo(Emprestimo emprestimo) {
        this.emprestimo = emprestimo;
    }
}
