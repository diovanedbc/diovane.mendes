package br.com.dbc.entity.enums;

public enum TipoConta {
    FISICA,
    CONJUNTO,
    JURIDICA;
}
