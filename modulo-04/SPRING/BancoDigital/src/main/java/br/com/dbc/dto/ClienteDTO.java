package br.com.dbc.dto;

public class ClienteDTO {

    private String nome;

    public ClienteDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
