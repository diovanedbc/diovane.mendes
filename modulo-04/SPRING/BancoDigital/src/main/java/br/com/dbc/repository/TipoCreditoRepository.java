package br.com.dbc.repository;

import br.com.dbc.entity.CreditoPreAprovado;
import br.com.dbc.entity.TipoCredito;
import org.springframework.data.repository.CrudRepository;

public interface TipoCreditoRepository extends CrudRepository<TipoCredito, Long> {

    TipoCredito findByNome(String nome);
    TipoCredito findByValor(double valor);
    TipoCredito findByCreditoPreAprovado(CreditoPreAprovado creditoPreAprovado);

}
