package br.com.dbc.factory;

import br.com.dbc.dto.TipoCreditoDTO;
import br.com.dbc.entity.TipoCredito;

public abstract class TipoCreditoFactory {

    public static TipoCreditoDTO getInstance(TipoCredito tipoCredito){
        TipoCreditoDTO tipoCreditoDTO = new TipoCreditoDTO();

        tipoCreditoDTO.setNome( tipoCredito.getNome() );
        tipoCreditoDTO.setValor( tipoCredito.getValor() );
        tipoCreditoDTO.setCreditoPreAprovadoDTO( CreditoPreAprovadoFactory.getInstance(tipoCredito.getCreditoPreAprovado()) );

        return tipoCreditoDTO;
    }

    public static TipoCredito getInstance(TipoCreditoDTO tipoCreditoDTO){
        TipoCredito tipoCredito = new TipoCredito();

        tipoCredito.setNome( tipoCreditoDTO.getNome() );
        tipoCredito.setValor( tipoCreditoDTO.getValor() );
        tipoCredito.setCreditoPreAprovado( CreditoPreAprovadoFactory.getInstance(tipoCreditoDTO.getCreditoPreAprovadoDTO()) );

        return tipoCredito;
    }
}
