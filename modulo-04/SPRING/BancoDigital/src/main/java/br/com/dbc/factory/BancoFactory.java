package br.com.dbc.factory;

import br.com.dbc.dto.BancoDTO;
import br.com.dbc.entity.Banco;

public abstract class BancoFactory {

    public static BancoDTO getInstance(Banco banco){
        BancoDTO bancoDTO = new BancoDTO();

        bancoDTO.setNome( banco.getNome() );
        bancoDTO.setCodigo( banco.getCodigo() );

        return bancoDTO;
    }

    public static Banco getInstance(BancoDTO bancoDTO){
        Banco banco = new Banco();

        banco.setNome( bancoDTO.getNome() );
        banco.setCodigo( bancoDTO.getCodigo() );

        return banco;
    }
}
