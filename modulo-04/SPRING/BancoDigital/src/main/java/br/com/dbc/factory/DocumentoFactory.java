package br.com.dbc.factory;

import br.com.dbc.dto.DocumentoDTO;
import br.com.dbc.entity.Documento;

public abstract class DocumentoFactory {

    public static DocumentoDTO getInstance(Documento documento){
        DocumentoDTO documentoDTO = new DocumentoDTO();

        documentoDTO.setTipo( documento.getTipo() );
        documentoDTO.setValor( documento.getValor() );

        return documentoDTO;
    }

    public static Documento getInstance(DocumentoDTO documentoDTO){
        Documento documento = new Documento();

        documento.setTipo( documentoDTO.getTipo() );
        documento.setValor( documentoDTO.getValor() );

        return documento;
    }
}
