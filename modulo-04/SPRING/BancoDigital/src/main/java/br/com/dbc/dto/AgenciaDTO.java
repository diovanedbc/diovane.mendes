package br.com.dbc.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AgenciaDTO {

    private String nome;

    private long codigo;

    private BancoDTO bancoDTO;

    private ContaDTO contaDTO;

    private EnderecoDTO enderecoDTO;

    private List<UsuarioDTO> usuarioDTOs = new ArrayList<>();

    public AgenciaDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public BancoDTO getBancoDTO() {
        return bancoDTO;
    }

    public void setBancoDTO(BancoDTO bancoDTO) {
        this.bancoDTO = bancoDTO;
    }

    public ContaDTO getContaDTO() {
        return contaDTO;
    }

    public void setContaDTO(ContaDTO contaDTO) {
        this.contaDTO = contaDTO;
    }

    public EnderecoDTO getEnderecoDTO() {
        return enderecoDTO;
    }

    public void setEnderecoDTO(EnderecoDTO enderecoDTO) {
        this.enderecoDTO = enderecoDTO;
    }

    public List<UsuarioDTO> getUsuarioDTO() {
        return usuarioDTOs;
    }

    public void pushUsuarios(UsuarioDTO... usuarioDTOs) {
        this.usuarioDTOs.addAll( Arrays.asList(usuarioDTOs) );
    }
}
