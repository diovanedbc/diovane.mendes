package br.com.dbc.dto;

public class TipoUsuarioDTO {

    private String nome;

    public TipoUsuarioDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
