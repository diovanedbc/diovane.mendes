package br.com.dbc.factory;

import br.com.dbc.dto.RegraDTO;
import br.com.dbc.entity.Regra;

public abstract class RegraFactory {

    public static RegraDTO getInstance(Regra regra){
        RegraDTO regraDTO = new RegraDTO();

        regraDTO.setDescricao( regra.getDescricao() );
        regraDTO.setTipoUsuario( regra.getTipoUsuario() );

        return regraDTO;
    }

    public static Regra getInstance(RegraDTO regraDTO){
        Regra regra = new Regra();

        regra.setDescricao( regra.getDescricao() );
        regra.setTipoUsuario( regra.getTipoUsuario() );

        return regra;
    }
}