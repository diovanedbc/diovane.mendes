package br.com.dbc.factory;

import br.com.dbc.dto.EnderecoDTO;
import br.com.dbc.entity.Endereco;

public abstract class EnderecoFactory {

    public static EnderecoDTO getInstance(Endereco endereco){
        EnderecoDTO enderecoDTO = new EnderecoDTO();

        enderecoDTO.setCidade( endereco.getCidade() );
        enderecoDTO.setBairro( endereco.getBairro() );
        enderecoDTO.setRua( endereco.getRua() );
        enderecoDTO.setNumero( endereco.getNumero() );
        enderecoDTO.setAgenciaDTO( AgenciaFactory.getInstance(endereco.getAgencia()) );

        return enderecoDTO;
    }

    public static Endereco getInstance(EnderecoDTO enderecoDTO){
        Endereco endereco = new Endereco();

        endereco.setCidade( enderecoDTO.getCidade() );
        endereco.setBairro( enderecoDTO.getBairro() );
        endereco.setRua( enderecoDTO.getRua() );
        endereco.setNumero( enderecoDTO.getNumero() );
        endereco.setAgencia( AgenciaFactory.getInstance(enderecoDTO.getAgenciaDTO()) );

        return endereco;
    }
}
