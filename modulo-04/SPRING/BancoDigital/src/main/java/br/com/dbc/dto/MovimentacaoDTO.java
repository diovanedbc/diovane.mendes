package br.com.dbc.dto;

import br.com.dbc.entity.Conta;
import br.com.dbc.entity.Emprestimo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class MovimentacaoDTO {

    private ContaDTO contaDTO;

    private double saque;

    private double deposito;

    private double transferencia;

    private double pagamento;

    private EmprestimoDTO emprestimoDTO;

    public MovimentacaoDTO() {
    }

    public ContaDTO getContaDTO() {
        return contaDTO;
    }

    public void setContaDTO(ContaDTO contaDTO) {
        this.contaDTO = contaDTO;
    }

    public double getSaque() {
        return saque;
    }

    public void setSaque(double saque) {
        this.saque = saque;
    }

    public double getDeposito() {
        return deposito;
    }

    public void setDeposito(double deposito) {
        this.deposito = deposito;
    }

    public double getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(double transferencia) {
        this.transferencia = transferencia;
    }

    public double getPagamento() {
        return pagamento;
    }

    public void setPagamento(double pagamento) {
        this.pagamento = pagamento;
    }

    public EmprestimoDTO getEmprestimoDTO() {
        return emprestimoDTO;
    }

    public void setEmprestimoDTO(EmprestimoDTO emprestimoDTO) {
        this.emprestimoDTO = emprestimoDTO;
    }
}
