package br.com.dbc.repository;

import br.com.dbc.entity.Agencia;
import br.com.dbc.entity.Banco;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BancoRepository extends CrudRepository<Banco, Long> {

    Banco findByCodigo(long codigo);
    Banco findByNome(String nome);
//    Banco findByAgencia(Agencia agencia);
    List<Banco> findByAgencias(List<Agencia> agencias);

}
