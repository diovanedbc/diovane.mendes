package br.com.dbc.repository;

import br.com.dbc.entity.Agencia;
import br.com.dbc.entity.Solicitacao;
import br.com.dbc.entity.TipoUsuario;
import br.com.dbc.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findByNome(String Nome);
    Usuario findByTipoUsuario(TipoUsuario tipoUsuario);
    List<Usuario> findByAgencia(Agencia agencia);
//    Usuario findBySolicitacao(Solicitacao solicitacao);
    List<Usuario> findBySolicitacoes(List<Solicitacao> solicitacoes);

}
