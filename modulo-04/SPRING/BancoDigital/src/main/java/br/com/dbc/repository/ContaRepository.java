package br.com.dbc.repository;

import br.com.dbc.entity.*;
import br.com.dbc.entity.enums.TipoConta;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContaRepository extends CrudRepository<Conta, Long> {

    Conta findByTipoConta(TipoConta tipoConta);
    Conta findByCliente(Cliente cliente);
    Conta findBySaldo(double saldo);
//    Conta findByCreditoPreAprovado(CreditoPreAprovado creditoPreAprovado);
    List<Conta> findByCreditoPreAprovados(List<CreditoPreAprovado> creditoPreAprovados);
//    Conta findByMovimentacao(Movimentacao movimentacao);
    List<Conta> findByMovimentacoes(List<Movimentacao> movimentacoes);
//    Conta findBySolicitacao(Solicitacao solicitacao);
    List<Conta> findBySolicitacoes(List<Solicitacao> solicitacoes);

}
