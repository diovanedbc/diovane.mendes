package br.com.dbc.factory;

import br.com.dbc.dto.SolicitacaoDTO;
import br.com.dbc.entity.Solicitacao;

public abstract class SolicitacaoFactory {

    public static SolicitacaoDTO getInstance(Solicitacao solicitacao){
        SolicitacaoDTO solicitacaoDTO = new SolicitacaoDTO();

        solicitacaoDTO.setStatusSolicitacao( solicitacao.getStatusSolicitacao() );
        solicitacaoDTO.setValor( solicitacao.getValor() );
        solicitacaoDTO.setContaDTO( ContaFactory.getInstance(solicitacao.getConta()) );
        solicitacaoDTO.setRegraDTO( RegraFactory.getInstance(solicitacao.getRegra()) );
        solicitacaoDTO.setUsuarioDTO( UsuarioFactory.getInstance(solicitacao.getUsuario()) );

        return solicitacaoDTO;
    }

    public static Solicitacao getInstance(SolicitacaoDTO solicitacaoDTO){
        Solicitacao solicitacao = new Solicitacao();

        solicitacao.setStatusSolicitacao( solicitacaoDTO.getStatusSolicitacao() );
        solicitacao.setValor( solicitacaoDTO.getValor() );
        solicitacao.setConta( ContaFactory.getInstance(solicitacaoDTO.getContaDTO()) );
        solicitacao.setRegra( RegraFactory.getInstance(solicitacaoDTO.getRegraDTO()) );
        solicitacao.setUsuario( UsuarioFactory.getInstance(solicitacaoDTO.getUsuarioDTO()) );

        return solicitacao;
    }
}