package br.com.dbc.factory;

import br.com.dbc.dto.AgenciaDTO;
import br.com.dbc.entity.Agencia;

public abstract class AgenciaFactory {

    public static AgenciaDTO getInstance(Agencia agencia){
        AgenciaDTO agenciaDTO = new AgenciaDTO();

        agenciaDTO.setNome( agencia.getNome() );
        agenciaDTO.setCodigo( agencia.getCodigo() );
        agenciaDTO.setBancoDTO( BancoFactory.getInstance(agencia.getBanco()) );
        agenciaDTO.setContaDTO( ContaFactory.getInstance(agencia.getConta()) );
        agenciaDTO.setEnderecoDTO( EnderecoFactory.getInstance(agencia.getEndereco()) );

        return agenciaDTO;
    }

    public static Agencia getInstance(AgenciaDTO agenciaDTO){
        Agencia agencia = new Agencia();

        agencia.setNome( agenciaDTO.getNome() );
        agencia.setCodigo( agenciaDTO.getCodigo() );
        agencia.setBanco( BancoFactory.getInstance(agenciaDTO.getBancoDTO()) );
        agencia.setConta( ContaFactory.getInstance(agenciaDTO.getContaDTO()) );
        agencia.setEndereco( EnderecoFactory.getInstance(agenciaDTO.getEnderecoDTO()) );

        return agencia;
    }
}