package br.com.dbc.factory;

import br.com.dbc.dto.ContatoDTO;
import br.com.dbc.entity.Contato;

public abstract class ContatoFactory {

    public static ContatoDTO getInstance(Contato contato){
        ContatoDTO contatoDTO = new ContatoDTO();

        contatoDTO.setTipo( contato.getTipo() );
        contatoDTO.setValor( contato.getValor() );

        return contatoDTO;
    }

    public static Contato getInstance(ContatoDTO contatoDTO){
        Contato contato = new Contato();

        contato.setTipo( contatoDTO.getTipo() );
        contato.setValor( contatoDTO.getValor() );

        return contato;
    }
}
