package br.com.dbc.factory;

import br.com.dbc.dto.ClienteDTO;
import br.com.dbc.entity.Cliente;

public abstract class ClienteFactory {

    public static ClienteDTO getInstance(Cliente cliente){
        ClienteDTO clienteDTO = new ClienteDTO();

        clienteDTO.setNome( cliente.getNome() );

        return clienteDTO;
    }

    public static Cliente getInstance(ClienteDTO clienteDTO){
        Cliente cliente = new Cliente();

        cliente.setNome( cliente.getNome() );

        return cliente;
    }
}
