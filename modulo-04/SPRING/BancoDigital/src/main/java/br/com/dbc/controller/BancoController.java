package br.com.dbc.controller;

import br.com.dbc.entity.Banco;
import br.com.dbc.service.interfaces.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/banco")
public class BancoController {

    @Autowired
    private BancoService bancoService;

    @PostMapping("/salvar")
    @ResponseBody
    public Banco salvar(@RequestBody Banco banco) {
        return bancoService.salvar(banco);
    }

    @DeleteMapping("/deletar/{id}")
    public String deletado(@PathVariable long id){
        bancoService.remover(id);
        return "index";
    }

    @GetMapping("/buscar/{id}")
    @ResponseBody
    public Banco buscaPorId(@PathVariable long id){
        return bancoService.buscar(id);
    }

    @PutMapping("/editar/{id}")
    @ResponseBody
    public Banco editar(@PathVariable long id, @RequestBody Banco banco){
        return bancoService.editar(id, banco);
    }
}
