package br.com.dbc.repository;

import br.com.dbc.entity.*;
import org.springframework.data.repository.CrudRepository;

import javax.print.Doc;
import java.util.List;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

    Cliente findByNome(String nome);
//    Cliente findByConta(Conta conta);
    List<Cliente> findByEnderecos(Endereco endereco);
    List<Cliente> findByEnderecos(List<Endereco> enderecos);
    List<Cliente> findByContatos(List<Contato> contatos);
    List<Cliente> findByContatos(Contato contato);
    List<Cliente> findByDocumentos(Documento documento);
    List<Cliente> findByDocumentos(List<Documento> documentos);

}
