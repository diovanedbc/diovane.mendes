package br.com.dbc.Entity;

import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class ServicoContratadoTest {

    @Test
    public void incrementaUmNaQuantidadeServicoDoSeguradoNaContratacao(){
        Segurado segurado = new Segurado();
        ServicoContratado servicoContratado = new ServicoContratado();

        servicoContratado.setSegurado(segurado);

        servicoContratado.incrementaQuantidadeServicoSegurado();

        assertEquals(1, servicoContratado.getSegurado().getQuantidadeServico());
    }

    @Test
    public void geraComissaoDe115(){
        Corretor corretor = new Corretor();
        ServicoContratado servicoContratado = new ServicoContratado();

        servicoContratado.setValor(1150.00);
        servicoContratado.setCorretor(corretor);

        servicoContratado.incrementaPorcentagemComissaoCorretor();

        assertEquals(115, servicoContratado.getCorretor().getComissao(),1);
    }
}
