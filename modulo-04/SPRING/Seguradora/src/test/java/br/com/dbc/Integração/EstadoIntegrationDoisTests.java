package br.com.dbc.Integração;

import br.com.dbc.Entity.Estado;
import br.com.dbc.Repository.EstadoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
public class EstadoIntegrationDoisTests {

    @MockBean
    private EstadoRepository estadoRepository;

    @Before
    public void setUp(){
        Estado estado = new Estado();
        estado.setNome("Rio Grande do Sul");

        Mockito.when( estadoRepository.findByNome( estado.getNome() ) ).thenReturn( estado );
    }

    @Test
    public void achaEstadoPorNome(){
        String nome = "Rio Grande do Sul";
        Estado found = estadoRepository.findByNome(nome);

        assertThat( found.getNome() ).isEqualTo( nome );
    }
}
