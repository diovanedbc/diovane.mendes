package br.com.dbc.Entity;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SeguradoTest {

    @Test
    public void deveIncrementarUmAQuantidadeServicos(){
        Segurado segurado = new Segurado();

        segurado.inclementaQuantidadeServico();

        assertEquals(1, segurado.getQuantidadeServico());
    }
}
