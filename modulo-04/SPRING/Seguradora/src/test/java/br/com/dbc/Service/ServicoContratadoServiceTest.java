package br.com.dbc.Service;

import br.com.dbc.DTO.RelatorioDTO;
import br.com.dbc.Entity.*;
import br.com.dbc.Factory.ServicoContratadoFactory;
import br.com.dbc.Service.contrato.ServicoContratadoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServicoContratadoServiceTest {

    @Autowired
    private ServicoContratadoService servicoContratadoService;

    private RelatorioDTO relatorioDTO = new RelatorioDTO();

    @Before
    public void setUp(){
        Random gerador = new Random();

        Seguradora seguradora = new Seguradora();
        seguradora.setNome("HDI");
        seguradora.setCnpj(gerador.nextInt(4589));

        Servico servico = new Servico();
        servico.setNome("Perca total");
        servico.setDescricao("Bateu forte");
        servico.setValorPadrao(2500);
        servico.pushSeguradoras( seguradora );

        Segurado segurado = new Segurado();
        segurado.setNome("Andreia");
        segurado.setCpf( gerador.nextInt(789) );
        segurado.setPai("Joao " + gerador.nextInt(99) );
        segurado.setMae("Maria " + gerador.nextInt(99) );
        segurado.setTelefone("(51) 994154" + gerador.nextInt(999));
        segurado.setEmail("andreia"+ gerador.nextInt(99) +"silva@gmail.com");

        Corretor corretor = new Corretor();
        corretor.setNome("Alfredo");
        corretor.setCpf( gerador.nextInt(789) );
        corretor.setPai("Manuel " + gerador.nextInt(99) );
        corretor.setMae("Viviane " + gerador.nextInt(99) );
        corretor.setTelefone("(51) 992364" + gerador.nextInt(999));
        corretor.setEmail("alfredo"+ gerador.nextInt(99) +"lewandosky@gmail.com");
        corretor.setCargo("Corretor");

        ServicoContratado servicoContratado = new ServicoContratado();
        servicoContratado.setServico( servico );
        servicoContratado.setDescricao( "Perca Total" );
        servicoContratado.setValor( 2100 );
        servicoContratado.setSegurado( segurado );
        servicoContratado.setCorretor( corretor );

        servicoContratadoService.salvar( servicoContratado );

        relatorioDTO = servicoContratadoService.buscaRelatorio(1);
    }

    @Test
    public void deveIncrementarQuantidadeDeServicosDoSegurado(){
        int quantidade = servicoContratadoService
                .buscarPorId(1)
                .getSegurado()
                .getQuantidadeServico();

        assertEquals(1, quantidade);
    }

    @Test
    public void deveGerarComissaoDoCorretorAoSalvar(){
        double comissao = servicoContratadoService
                .buscarPorId(1)
                .getCorretor()
                .getComissao();

        assertEquals(210, comissao, 1);
    }

    @Test
    public void deveMostrarRelatorioPreenchido(){
//        Equals não está funcionando, da como notEquals os RelatorioDTOs
        assertThat( relatorioDTO ).isEqualTo( servicoContratadoService.buscaRelatorio(1) );
    }
}
