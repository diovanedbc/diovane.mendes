package br.com.dbc.Integração;

import br.com.dbc.Controller.EstadoController;
import br.com.dbc.DTO.EstadoDTO;
import br.com.dbc.SeguradoraApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
public class EstadoIntegrationTresTests extends SeguradoraApplicationTests {

    private MockMvc mvc;

    @Autowired
    private EstadoController estadoController;

    private EstadoDTO estadoDTO = new EstadoDTO();

    @Before
    public void setUp(){
        estadoDTO.setId(1);
        estadoDTO.setNome("Rio Grande do Sul");

        this.mvc = MockMvcBuilders
                .standaloneSetup( estadoController )
                .build();
    }

    @Test
    public void statusOk() throws Exception{
        this.mvc.perform( MockMvcRequestBuilders
                        .get("/api/estado/listar") )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isOk() );
    }

    @Test
    public void statusOkParaPost() throws Exception{
        this.mvc.perform( MockMvcRequestBuilders
                    .post("/api/estado/salvar" )
                    .contentType( MediaType.APPLICATION_JSON )
                    .content( JsonString.asJsonString(estadoDTO) ) )
                .andExpect( MockMvcResultMatchers
                        .status()
                        .isOk() );
    }


}
