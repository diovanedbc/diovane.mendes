package br.com.dbc.Entity;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CorretorTest {

    @Test
    public void deveGanhar89DeComissaoPelaVendaDe890(){
        Corretor corretor = new Corretor();

        corretor.incrementaPorcentagem(890);

        assertEquals(89, corretor.getComissao(), 1);
    }
}