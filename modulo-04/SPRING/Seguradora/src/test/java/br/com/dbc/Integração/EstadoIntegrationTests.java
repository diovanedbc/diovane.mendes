package br.com.dbc.Integração;

import br.com.dbc.Entity.Estado;
import br.com.dbc.Repository.EstadoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegrationTests {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private EstadoRepository estadoRepository;

    @Test
    public void achaEstadoPorNome(){
        Estado estado = new Estado();
        estado.setNome("Rio Grande do Sul");

        testEntityManager.persist(estado);
        testEntityManager.flush();

        Estado found = estadoRepository.findByNome( estado.getNome() );

        assertThat( found.getNome() ).isEqualTo( estado.getNome() );
    }

}
