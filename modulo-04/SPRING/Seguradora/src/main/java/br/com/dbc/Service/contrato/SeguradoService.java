package br.com.dbc.Service.contrato;

import br.com.dbc.DTO.SeguradoDTO;
import br.com.dbc.Entity.Segurado;
import br.com.dbc.Service.PadraoService;

public interface SeguradoService extends PadraoService<Segurado> {
}
