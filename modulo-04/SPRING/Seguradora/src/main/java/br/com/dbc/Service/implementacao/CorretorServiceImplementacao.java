package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Corretor;
import br.com.dbc.Repository.CorretorRepository;
import br.com.dbc.Service.contrato.CorretorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CorretorServiceImplementacao implements CorretorService {

    @Autowired
    private CorretorRepository corretorRepository;

    public Corretor salvar(Corretor corretor) {
        return corretorRepository.save( corretor );
    }

    public Corretor buscarPorId(long id) {
        return corretorRepository
                .findById(id)
                .orElse(null);
    }

    public List<Corretor> buscarTodos() {
        return (List<Corretor>) corretorRepository.findAll();
    }

    public Corretor atualizar(long id, Corretor corretor) {
        corretor.setId(id);
        return corretorRepository.save( corretor );
    }

    public void deletar(long id) {
        corretorRepository.deleteById(id);
    }
}