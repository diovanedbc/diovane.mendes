package br.com.dbc.Service.contrato;

import br.com.dbc.Entity.Cidade;
import br.com.dbc.Service.PadraoService;

public interface CidadeService extends PadraoService<Cidade> {
}
