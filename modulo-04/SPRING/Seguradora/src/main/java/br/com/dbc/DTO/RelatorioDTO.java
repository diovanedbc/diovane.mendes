package br.com.dbc.DTO;

import br.com.dbc.Entity.ServicoContratado;

import java.util.Objects;

public class RelatorioDTO {

    private ServicoContratado servicoContratado;

    private double valorPadrao;

    public RelatorioDTO() {
    }

    public ServicoContratado getServicoContratado() {
        return servicoContratado;
    }

    public void setServicoContratado(ServicoContratado servicoContratado) {
        this.servicoContratado = servicoContratado;
    }

    public double getValorPadrao() {
        return valorPadrao;
    }

    public void setValorPadrao(double valorPadrao) {
        this.valorPadrao = valorPadrao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RelatorioDTO that = (RelatorioDTO) o;
        return Objects.equals(getValorPadrao(), that.getValorPadrao()) &&
                Objects.equals(getServicoContratado(), that.getServicoContratado());
    }

    @Override
    public int hashCode() {
        return Objects.hash(servicoContratado, valorPadrao);
    }

    @Override
    public String toString() {
        return "RelatorioDTO{" +
                "servicoContratado=" + servicoContratado +
                ", valorPadrao=" + valorPadrao +
                '}';
    }
}
