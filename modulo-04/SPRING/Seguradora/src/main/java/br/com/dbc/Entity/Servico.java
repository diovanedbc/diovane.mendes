package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SERVICOS")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Servico {

    @Id
    @GeneratedValue(generator = "SERVICO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @Column(name = "DESCRICAO", nullable = false)
    private String descricao;

    @Column(name = "VALOR_PADRAO", nullable = false)
    private double valorPadrao;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "SEGURADORAS_SERVICOS",
            joinColumns = {
                    @JoinColumn(name = "ID_SERVICO"/*, nullable = false*/)},
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_SEGURADORA"/* , nullable = false */)})
    private List<Seguradora> seguradoras = new ArrayList<>();

    @OneToOne(mappedBy = "servico")
    private ServicoContratado servicoContratado;

    public Servico() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValorPadrao() {
        return valorPadrao;
    }

    public void setValorPadrao(double valorPadrao) {
        this.valorPadrao = valorPadrao;
    }

    public List<Seguradora> getSeguradoras() {
        return seguradoras;
    }

    public void pushSeguradoras(Seguradora... seguradoras) {
        this.seguradoras.addAll( Arrays.asList(seguradoras) );
    }

    public void setSeguradoras(List<Seguradora> seguradoras) {
        this.seguradoras = seguradoras;
    }

    public ServicoContratado getServicoContratado() {
        return servicoContratado;
    }

    public void setServicoContratado(ServicoContratado servicoContratado) {
        this.servicoContratado = servicoContratado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Servico)) return false;
        Servico servico = (Servico) o;
        return Objects.equals(getId(), servico.getId()) &&
                Objects.equals(getValorPadrao(), servico.getValorPadrao()) &&
                Objects.equals(getNome(), servico.getNome()) &&
                Objects.equals(getDescricao(), servico.getDescricao()) &&
                Objects.equals(getSeguradoras(), servico.getSeguradoras()) &&
                Objects.equals(getServicoContratado(), servico.getServicoContratado());
    }

    @Override
    public String toString() {
        return "Servico{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", valorPadrao=" + valorPadrao +
                '}';
    }
}
