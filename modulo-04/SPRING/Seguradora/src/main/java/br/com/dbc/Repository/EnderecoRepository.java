package br.com.dbc.Repository;

import br.com.dbc.Entity.Bairro;
import br.com.dbc.Entity.Endereco;
import br.com.dbc.Entity.Pessoa;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EnderecoRepository extends CrudRepository<Endereco, Long> {

    Endereco findByLougradouro(String lougradouro);
    Endereco findByNumero(int numero);
    Endereco findByComplemento(String complemento);
    Endereco findByBairro(Bairro bairro);
    List<Endereco> findByPessoas(Pessoa... pessoas);

}
