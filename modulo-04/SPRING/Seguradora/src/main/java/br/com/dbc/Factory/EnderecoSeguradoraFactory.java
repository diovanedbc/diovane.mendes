package br.com.dbc.Factory;

import br.com.dbc.DTO.EnderecoSeguradoraDTO;
import br.com.dbc.Entity.EnderecoSeguradora;

public abstract class EnderecoSeguradoraFactory {

    public static EnderecoSeguradoraDTO getInstance(EnderecoSeguradora enderecoSeguradora){
        EnderecoSeguradoraDTO enderecoSeguradoraDTO = new EnderecoSeguradoraDTO();

        enderecoSeguradoraDTO.setId( enderecoSeguradora.getId() );
        enderecoSeguradoraDTO.setLougradouro( enderecoSeguradora.getLougradouro() );
        enderecoSeguradoraDTO.setNumero( enderecoSeguradora.getNumero() );
        enderecoSeguradoraDTO.setComplemento( enderecoSeguradora.getComplemento() );
        enderecoSeguradoraDTO.setBairroDTO( BairroFactory.getInstance(enderecoSeguradora.getBairro()) );

        return enderecoSeguradoraDTO;
    }

    public static EnderecoSeguradora getInstance(EnderecoSeguradoraDTO enderecoSeguradoraDTO){
        EnderecoSeguradora enderecoSeguradora = new EnderecoSeguradora();

        enderecoSeguradora.setId( enderecoSeguradoraDTO.getId() );
        enderecoSeguradora.setLougradouro( enderecoSeguradoraDTO.getLougradouro() );
        enderecoSeguradora.setNumero( enderecoSeguradoraDTO.getNumero() );
        enderecoSeguradora.setComplemento( enderecoSeguradoraDTO.getComplemento() );
        enderecoSeguradora.setBairro( BairroFactory.getInstance(enderecoSeguradoraDTO.getBairroDTO()) );

        return enderecoSeguradora;
    }
}
