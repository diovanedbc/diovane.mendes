package br.com.dbc.Factory;

import br.com.dbc.DTO.EstadoDTO;
import br.com.dbc.Entity.Estado;

public abstract class EstadoFactory {

    public static EstadoDTO getInstance(Estado estado){
        EstadoDTO estadoDTO = new EstadoDTO();

        estadoDTO.setId( estado.getId() );
        estadoDTO.setNome( estado.getNome() );

        return estadoDTO;
    }

    public static Estado getInstance(EstadoDTO estadoDTO){
        Estado estado = new Estado();

        estado.setId( estadoDTO.getId() );
        estado.setNome( estadoDTO.getNome() );

        return estado;
    }
}