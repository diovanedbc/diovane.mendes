package br.com.dbc.Service.contrato;

import br.com.dbc.Entity.Bairro;
import br.com.dbc.Service.PadraoService;

public interface BairroService extends PadraoService<Bairro> {
}
