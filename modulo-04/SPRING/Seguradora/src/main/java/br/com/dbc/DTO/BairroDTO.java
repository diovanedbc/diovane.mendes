package br.com.dbc.DTO;

public class BairroDTO {

    private long id;

    private CidadeDTO cidadeDTO;

    private String nome;

    public BairroDTO() {
    }

    public CidadeDTO getCidadeDTO() {
        return cidadeDTO;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCidadeDTO(CidadeDTO cidadeDTO) {
        this.cidadeDTO = cidadeDTO;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
