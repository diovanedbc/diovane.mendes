package br.com.dbc.DTO;

public class SeguradoDTO extends PessoaDTO {

    private int quantidadeServico;

    public SeguradoDTO() {
    }

    public int getQuantidadeServico() {
        return quantidadeServico;
    }

    public void setQuantidadeServico(int quantidadeServico) {
        this.quantidadeServico = quantidadeServico;
    }
}
