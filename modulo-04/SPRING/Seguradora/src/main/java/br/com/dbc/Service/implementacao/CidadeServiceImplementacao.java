package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Cidade;
import br.com.dbc.Repository.CidadeRepository;
import br.com.dbc.Service.contrato.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadeServiceImplementacao implements CidadeService {

    @Autowired
    private CidadeRepository cidadeRepository;

    public Cidade salvar(Cidade cidade) {
        return cidadeRepository.save(cidade);
    }

    public Cidade buscarPorId(long id) {
        return cidadeRepository
                .findById(id)
                .orElse(null);
    }

    public List<Cidade> buscarTodos() {
        return (List<Cidade>) cidadeRepository.findAll();
    }

    public Cidade atualizar(long id, Cidade cidade) {
        cidade.setId(id);
        return cidadeRepository.save(cidade);
    }

    public void deletar(long id) {
        cidadeRepository.deleteById(id);
    }
}
