package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "CIDADES")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Cidade {

    @Id
    @GeneratedValue(generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ESTADO", nullable = false)
    private Estado estado;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @OneToMany(mappedBy = "cidade")
    private List<Bairro> bairros = new ArrayList<>();

    public Cidade() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Bairro> getBairros() {
        return bairros;
    }

    public void pushBairros(Bairro... bairros) {
        this.bairros.addAll( Arrays.asList(bairros) );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cidade)) return false;
        Cidade cidade = (Cidade) o;
        return Objects.equals(getId(), cidade.getId()) &&
                Objects.equals(getEstado(), cidade.getEstado()) &&
                Objects.equals(getNome(), cidade.getNome()) &&
                Objects.equals(getBairros(), cidade.getBairros());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, estado, nome, bairros);
    }
}
