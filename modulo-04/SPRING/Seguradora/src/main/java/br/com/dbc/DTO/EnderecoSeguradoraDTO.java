package br.com.dbc.DTO;

public class EnderecoSeguradoraDTO extends EnderecoDTO{

    private SeguradoraDTO seguradoraDTO;

    public EnderecoSeguradoraDTO() {
    }

    public SeguradoraDTO getSeguradoraDTO() {
        return seguradoraDTO;
    }

    public void setSeguradoraDTO(SeguradoraDTO seguradoraDTO) {
        this.seguradoraDTO = seguradoraDTO;
    }
}
