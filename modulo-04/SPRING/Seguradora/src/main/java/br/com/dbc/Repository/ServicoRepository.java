package br.com.dbc.Repository;

import br.com.dbc.Entity.Seguradora;
import br.com.dbc.Entity.Servico;
import br.com.dbc.Entity.ServicoContratado;
import org.springframework.data.repository.CrudRepository;

public interface ServicoRepository extends CrudRepository<Servico, Long> {

    Servico findByNome(String nome);
    Servico findByDescricao(String descricao);
    Servico findByValorPadrao(double valorPadrao);
    Servico findBySeguradoras(Seguradora... seguradoras);
    Servico findByServicoContratado(ServicoContratado servicoContratado);

}
