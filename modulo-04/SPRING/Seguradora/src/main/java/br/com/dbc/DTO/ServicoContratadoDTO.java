package br.com.dbc.DTO;

public class ServicoContratadoDTO {

    private long id;

    private ServicoDTO servicoDTO;

    private String descricao;

    private double valor;

    private SeguradoDTO seguradoDTO;

    private CorretorDTO corretorDTO;

    public ServicoContratadoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ServicoDTO getServicoDTO() {
        return servicoDTO;
    }

    public void setServicoDTO(ServicoDTO servicoDTO) {
        this.servicoDTO = servicoDTO;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public SeguradoDTO getSeguradoDTO() {
        return seguradoDTO;
    }

    public void setSeguradoDTO(SeguradoDTO seguradoDTO) {
        this.seguradoDTO = seguradoDTO;
    }

    public CorretorDTO getCorretorDTO() {
        return corretorDTO;
    }

    public void setCorretorDTO(CorretorDTO corretorDTO) {
        this.corretorDTO = corretorDTO;
    }
}
