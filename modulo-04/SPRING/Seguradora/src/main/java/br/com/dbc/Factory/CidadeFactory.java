package br.com.dbc.Factory;

import br.com.dbc.DTO.CidadeDTO;
import br.com.dbc.Entity.Cidade;

public abstract class CidadeFactory {

    public static CidadeDTO getInstance(Cidade cidade){
        CidadeDTO cidadeDTO = new CidadeDTO();

        cidadeDTO.setId( cidade.getId() );
        cidadeDTO.setNome( cidade.getNome() );
        cidadeDTO.setEstadoDTO( EstadoFactory.getInstance(cidade.getEstado()) );

        return cidadeDTO;
    }

    public static Cidade getInstance(CidadeDTO cidadeDTO){
        Cidade cidade = new Cidade();

        cidade.setId( cidadeDTO.getId() );
        cidade.setNome( cidadeDTO.getNome() );
        cidade.setEstado( EstadoFactory.getInstance(cidadeDTO.getEstadoDTO()) );

        return cidade;
    }
}
