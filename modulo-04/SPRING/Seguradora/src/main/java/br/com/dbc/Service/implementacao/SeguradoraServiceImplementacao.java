package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Seguradora;
import br.com.dbc.Repository.SeguradoraRepository;
import br.com.dbc.Service.contrato.SeguradoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeguradoraServiceImplementacao implements SeguradoraService {

    @Autowired
    private SeguradoraRepository seguradoraRepository;

    public Seguradora salvar(Seguradora seguradora) {
        return seguradoraRepository.save(seguradora);
    }

    public Seguradora buscarPorId(long id) {
        return seguradoraRepository
                .findById(id)
                .orElse(null);
    }

    public List<Seguradora> buscarTodos() {
        return (List<Seguradora>) seguradoraRepository.findAll();
    }

    public Seguradora atualizar(long id, Seguradora seguradora) {
        seguradora.setId(id);
        return seguradoraRepository.save(seguradora);
    }

    public void deletar(long id) {
        seguradoraRepository.deleteById(id);
    }
}
