package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "ENDERECO_SEGURADORAS")
@PrimaryKeyJoinColumn(name = "ID_ENDERECO")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class EnderecoSeguradora extends Endereco{

    @OneToOne(mappedBy = "enderecoSeguradora", cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_SEGURADORA", nullable = false)
    private Seguradora seguradora;

    public EnderecoSeguradora() {
    }

    public Seguradora getSeguradora() {
        return seguradora;
    }

    public void setSeguradora(Seguradora seguradora) {
        this.seguradora = seguradora;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EnderecoSeguradora)) return false;
        if (!super.equals(o)) return false;
        EnderecoSeguradora that = (EnderecoSeguradora) o;
        return Objects.equals(getSeguradora(), that.getSeguradora());
    }

    @Override
    public int hashCode() {
        return Objects.hash(seguradora);
    }
}
