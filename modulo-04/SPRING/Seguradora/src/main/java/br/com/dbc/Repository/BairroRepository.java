package br.com.dbc.Repository;

import br.com.dbc.Entity.Bairro;
import br.com.dbc.Entity.Cidade;
import br.com.dbc.Entity.Endereco;
import org.springframework.data.repository.CrudRepository;

public interface BairroRepository extends CrudRepository<Bairro, Long> {

    Bairro findByCidade(Cidade cidade);
    Bairro findByNome(String nome);
    Bairro findByEndereco(Endereco endereco);

}
