package br.com.dbc.Repository;

import br.com.dbc.Entity.EnderecoSeguradora;
import br.com.dbc.Entity.Seguradora;
import br.com.dbc.Entity.Servico;
import br.com.dbc.Entity.ServicoContratado;
import org.springframework.data.repository.CrudRepository;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Long> {

    Seguradora findByNome(String nome);
    Seguradora findByCnpj(long cnpj);
    Seguradora findByEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora);
    Seguradora findByServicos(Servico... servico);

}
