package br.com.dbc.Factory;

import br.com.dbc.DTO.PessoaDTO;
import br.com.dbc.Entity.Pessoa;

import java.util.stream.Collectors;

public abstract class PessoaFactory {

    public static PessoaDTO getInstance(Pessoa pessoa){
        PessoaDTO pessoaDTO = new PessoaDTO();

        pessoaDTO.setId( pessoa.getId() );
        pessoaDTO.setNome( pessoa.getNome() );
        pessoaDTO.setCpf( pessoa.getCpf() );
        pessoaDTO.setPai( pessoa.getPai() );
        pessoaDTO.setMae( pessoa.getMae() );
        pessoaDTO.setTelefone( pessoa.getTelefone() );
        pessoaDTO.setEnderecosDTO( pessoa.getEnderecos().stream()
                .map(EnderecoFactory::getInstance)
                .collect(Collectors.toList()) );

        return pessoaDTO;
    }

    public static Pessoa getInstance(PessoaDTO pessoaDTO){
        Pessoa pessoa = new Pessoa();

        pessoa.setId( pessoaDTO.getId() );
        pessoa.setNome( pessoaDTO.getNome() );
        pessoa.setCpf( pessoaDTO.getCpf() );
        pessoa.setPai( pessoaDTO.getPai() );
        pessoa.setMae( pessoaDTO.getMae() );
        pessoa.setTelefone( pessoaDTO.getTelefone() );
        pessoa.setEnderecos( pessoaDTO.getEnderecosDTO().stream()
                .map(EnderecoFactory::getInstance)
                .collect(Collectors.toList()) );

        return pessoa;
    }
}
