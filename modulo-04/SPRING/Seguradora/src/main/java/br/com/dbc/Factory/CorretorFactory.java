package br.com.dbc.Factory;

import br.com.dbc.DTO.CorretorDTO;
import br.com.dbc.Entity.Corretor;

import java.util.stream.Collectors;

public abstract class CorretorFactory {

    public static CorretorDTO getInstance(Corretor corretor){
        CorretorDTO corretorDTO = new CorretorDTO();

        corretorDTO.setId( corretor.getId() );
        corretorDTO.setNome( corretor.getNome() );
        corretorDTO.setCpf( corretor.getCpf() );
        corretorDTO.setPai( corretor.getPai() );
        corretorDTO.setMae( corretor.getMae() );
        corretorDTO.setTelefone( corretor.getTelefone() );
        corretorDTO.setEmail( corretor.getEmail() );
        corretorDTO.setEnderecosDTO( corretor.getEnderecos().stream()
                .map(EnderecoFactory::getInstance)
                .collect(Collectors.toList()) );
        corretorDTO.setCargo( corretor.getCargo() );
        corretorDTO.setComissao( corretor.getComissao() );

        return corretorDTO;
    }

    public static Corretor getInstance(CorretorDTO corretorDTO){
        Corretor corretor  = new Corretor();

        corretor.setId(corretorDTO.getId());
        corretor.setNome(corretorDTO.getNome());
        corretor.setCpf(corretorDTO.getCpf());
        corretor.setPai(corretorDTO.getPai());
        corretor.setMae(corretorDTO.getMae());
        corretor.setTelefone(corretorDTO.getTelefone());
        corretor.setEmail( corretorDTO.getEmail() );
        corretor.setEnderecos(corretorDTO.getEnderecosDTO().stream()
                .map(EnderecoFactory::getInstance)
                .collect(Collectors.toList()));
        corretor.setCargo(corretorDTO.getCargo());
        corretor.setComissao(corretorDTO.getComissao());

        return corretor;
    }
}
