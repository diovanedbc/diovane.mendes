package br.com.dbc.Factory;

import br.com.dbc.DTO.SeguradoDTO;
import br.com.dbc.Entity.Segurado;

import java.util.stream.Collectors;

public abstract class SeguradoFactory {

    public static SeguradoDTO getInstance(Segurado segurado){
        SeguradoDTO seguradoDTO  = new SeguradoDTO();

        seguradoDTO.setId( segurado.getId() );
        seguradoDTO.setNome( segurado.getNome() );
        seguradoDTO.setCpf( segurado.getCpf() );
        seguradoDTO.setPai( segurado.getPai() );
        seguradoDTO.setMae( segurado.getMae() );
        seguradoDTO.setTelefone( segurado.getTelefone() );
        seguradoDTO.setEmail( segurado.getEmail() );
        seguradoDTO.setEnderecosDTO( segurado.getEnderecos().stream()
                .map(EnderecoFactory::getInstance)
                .collect(Collectors.toList()) );
        seguradoDTO.setQuantidadeServico( segurado.getQuantidadeServico() );

        return seguradoDTO;
    }

    public static Segurado getInstance(SeguradoDTO seguradoDTO){
        Segurado segurado  = new Segurado();

        segurado.setId( seguradoDTO.getId() );
        segurado.setNome( seguradoDTO.getNome() );
        segurado.setCpf( seguradoDTO.getCpf() );
        segurado.setPai( seguradoDTO.getPai() );
        segurado.setMae( seguradoDTO.getMae() );
        segurado.setTelefone( seguradoDTO.getTelefone() );
        segurado.setEmail( seguradoDTO.getEmail() );
        segurado.setEnderecos( seguradoDTO.getEnderecosDTO().stream()
                .map(EnderecoFactory::getInstance)
                .collect(Collectors.toList()));
        segurado.setQuantidadeServico( seguradoDTO.getQuantidadeServico() );

        return segurado;
    }
}
