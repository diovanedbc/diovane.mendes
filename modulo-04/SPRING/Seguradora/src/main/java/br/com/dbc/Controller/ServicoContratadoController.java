package br.com.dbc.Controller;

import br.com.dbc.DTO.RelatorioDTO;
import br.com.dbc.DTO.ServicoContratadoDTO;
import br.com.dbc.Entity.ServicoContratado;
import br.com.dbc.Factory.ServicoContratadoFactory;
import br.com.dbc.Service.contrato.ServicoContratadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/servico-contratado")
public class ServicoContratadoController implements PadraoController<ServicoContratadoDTO, ServicoContratado>{

    @Autowired
    private ServicoContratadoService servicoContratadoService;

    public ServicoContratadoDTO salvar(ServicoContratado servicoContratado) {
        return ServicoContratadoFactory.getInstance(servicoContratadoService.salvar(servicoContratado));
    }

    public ServicoContratadoDTO buscarPorId(long id) {
        return ServicoContratadoFactory.getInstance(
                servicoContratadoService.buscarPorId(id) );
    }

    public List<ServicoContratadoDTO> buscarTodos() {
        return servicoContratadoService.buscarTodos()
                .stream()
                .map(ServicoContratadoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public ServicoContratadoDTO atualizar(long id, ServicoContratado servicoContratado) {
        return ServicoContratadoFactory.getInstance(
                servicoContratadoService.atualizar(id, servicoContratado) );
    }

    public void deletar(long id) {
        servicoContratadoService.deletar(id);
    }

    @GetMapping("/relatorio/{id}")
    @ResponseBody
    public RelatorioDTO relatorio(@PathVariable long id){
        return servicoContratadoService.buscaRelatorio(id);
    }
}