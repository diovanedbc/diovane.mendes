package br.com.dbc.Controller;

import br.com.dbc.DTO.SeguradoDTO;
import br.com.dbc.Entity.Segurado;
import br.com.dbc.Factory.SeguradoFactory;
import br.com.dbc.Service.contrato.SeguradoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/segurado")
public class SeguradoController implements PadraoController<SeguradoDTO, Segurado>{

    @Autowired
    private SeguradoService seguradoService;

    public SeguradoDTO salvar(Segurado segurado) {
        return SeguradoFactory.getInstance( seguradoService.salvar(segurado) );
    }

    public SeguradoDTO buscarPorId(long id) {
        return SeguradoFactory.getInstance( seguradoService.buscarPorId(id) );
    }

    public List<SeguradoDTO> buscarTodos() {
        return seguradoService.buscarTodos()
                .stream()
                .map(SeguradoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public SeguradoDTO atualizar(long id, Segurado segurado) {
        return SeguradoFactory.getInstance( seguradoService.atualizar(id, segurado) );
    }

    public void deletar(long id) {
        seguradoService.deletar(id);
    }
}
