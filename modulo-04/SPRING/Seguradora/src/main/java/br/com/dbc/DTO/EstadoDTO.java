package br.com.dbc.DTO;

public class EstadoDTO {

    private long id;

    private String nome;

    public EstadoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
