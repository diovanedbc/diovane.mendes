package br.com.dbc.Factory;

import br.com.dbc.DTO.ServicoContratadoDTO;
import br.com.dbc.Entity.ServicoContratado;

public abstract class ServicoContratadoFactory {

    public static ServicoContratadoDTO getInstance(ServicoContratado servicoContratado){
        ServicoContratadoDTO servicoContratadoDTO = new ServicoContratadoDTO();

        servicoContratadoDTO.setId( servicoContratado.getId() );
        servicoContratadoDTO.setServicoDTO( ServicoFactory.getInstance(servicoContratado.getServico()) );
        servicoContratadoDTO.setDescricao( servicoContratado.getDescricao() );
        servicoContratadoDTO.setValor( servicoContratado.getValor() );
        servicoContratadoDTO.setSeguradoDTO( SeguradoFactory.getInstance(servicoContratado.getSegurado()) );
        servicoContratadoDTO.setCorretorDTO( CorretorFactory.getInstance(servicoContratado.getCorretor()) );

        return servicoContratadoDTO;
    }

    public static ServicoContratado getInstance(ServicoContratadoDTO servicoContratadoDTO){
        ServicoContratado servicoContratado = new ServicoContratado();

        servicoContratado.setId( servicoContratadoDTO.getId() );
        servicoContratado.setServico( ServicoFactory.getInstance(servicoContratadoDTO.getServicoDTO()) );
        servicoContratado.setDescricao( servicoContratado.getDescricao() );
        servicoContratado.setValor( servicoContratado.getValor() );
        servicoContratado.setSegurado( SeguradoFactory.getInstance(servicoContratadoDTO.getSeguradoDTO()) );
        servicoContratado.setCorretor( CorretorFactory.getInstance(servicoContratadoDTO.getCorretorDTO()) );

        return servicoContratado;
    }
}
