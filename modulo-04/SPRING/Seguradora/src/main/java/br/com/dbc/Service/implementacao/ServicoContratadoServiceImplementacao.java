package br.com.dbc.Service.implementacao;

import br.com.dbc.DTO.RelatorioDTO;
import br.com.dbc.Entity.ServicoContratado;
import br.com.dbc.Repository.ServicoContratadoRepository;
import br.com.dbc.Service.contrato.ServicoContratadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicoContratadoServiceImplementacao implements ServicoContratadoService{

    @Autowired
    private ServicoContratadoRepository servicoContratadoRepository;

    public ServicoContratado salvar(ServicoContratado servicoContratado) {
        return servicoContratadoRepository.save(servicoContratado);
    }

    public ServicoContratado buscarPorId(long id) {
        return servicoContratadoRepository
                .findById(id)
                .orElse(null);
    }

    public List<ServicoContratado> buscarTodos() {
        return (List<ServicoContratado>) servicoContratadoRepository.findAll();
    }

    public ServicoContratado atualizar(long id, ServicoContratado servicoContratado) {
        servicoContratado.setId(id);
        return salvar(servicoContratado);
    }

    public void deletar(long id) {
        servicoContratadoRepository.deleteById(id);
    }

    public RelatorioDTO buscaRelatorio(long id){
        RelatorioDTO relatorioDTO = new RelatorioDTO();

        ServicoContratado servicoContratado = servicoContratadoRepository
                .findById(id)
                .orElse(new ServicoContratado());

        relatorioDTO.setServicoContratado(servicoContratado);

        if (servicoContratado.getServico() != null){
            relatorioDTO.setValorPadrao(servicoContratado.getServico().getValorPadrao());
        }

        return relatorioDTO;
    }
}
