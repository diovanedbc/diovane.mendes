package br.com.dbc.Controller;

import br.com.dbc.DTO.PessoaDTO;
import br.com.dbc.Entity.Pessoa;
import br.com.dbc.Factory.PessoaFactory;
import br.com.dbc.Service.contrato.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/pessoa")
public class PessoaController implements PadraoController<PessoaDTO, Pessoa>{

    @Autowired
    private PessoaService pessoaService;

    public PessoaDTO salvar(Pessoa pessoa) {
        return PessoaFactory.getInstance( pessoaService.salvar(pessoa) );
    }

    public PessoaDTO buscarPorId(long id) {
        return PessoaFactory.getInstance( pessoaService.buscarPorId(id) );
    }

    public List<PessoaDTO> buscarTodos() {
        return pessoaService.buscarTodos()
                .stream()
                .map(PessoaFactory::getInstance)
                .collect(Collectors.toList());
    }

    public PessoaDTO atualizar(long id, Pessoa pessoa) {
        return PessoaFactory.getInstance( pessoaService.atualizar(id, pessoa) );
    }

    public void deletar(long id) {
        pessoaService.deletar(id);
    }
}
