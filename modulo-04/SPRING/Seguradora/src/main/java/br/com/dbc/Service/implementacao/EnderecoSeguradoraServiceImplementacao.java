package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.EnderecoSeguradora;
import br.com.dbc.Repository.EnderecoSeguradoraRepository;
import br.com.dbc.Service.contrato.EnderecoSeguradoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoSeguradoraServiceImplementacao implements EnderecoSeguradoraService {

    @Autowired
    private EnderecoSeguradoraRepository enderecoSeguradoraRepository;

    public EnderecoSeguradora salvar(EnderecoSeguradora enderecoSeguradora) {
        return enderecoSeguradoraRepository.save(enderecoSeguradora);
    }

    public EnderecoSeguradora buscarPorId(long id) {
        return enderecoSeguradoraRepository
                .findById(id)
                .orElse(null);
    }

    public List<EnderecoSeguradora> buscarTodos() {
        return (List<EnderecoSeguradora>) enderecoSeguradoraRepository.findAll();
    }

    public EnderecoSeguradora atualizar(long id, EnderecoSeguradora enderecoSeguradora) {
        enderecoSeguradora.setId(id);
        return enderecoSeguradoraRepository.save(enderecoSeguradora);
    }

    public void deletar(long id) {
        enderecoSeguradoraRepository.deleteById(id);
    }
}
