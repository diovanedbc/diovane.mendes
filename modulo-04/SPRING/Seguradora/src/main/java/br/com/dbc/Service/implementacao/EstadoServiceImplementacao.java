package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Estado;
import br.com.dbc.Repository.EstadoRepository;
import br.com.dbc.Service.contrato.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoServiceImplementacao implements EstadoService {

    @Autowired
    private EstadoRepository estadoRepository;

    public Estado salvar(Estado estado) {
        return estadoRepository.save(estado);
    }

    public Estado buscarPorId(long id) {
        return estadoRepository
                .findById(id)
                .orElse(null);
    }

    public List<Estado> buscarTodos() {
        return (List<Estado>) estadoRepository.findAll();
    }

    public Estado atualizar(long id, Estado estado) {
        estado.setId(id);
        return estadoRepository.save(estado);
    }

    public void deletar(long id) {
        estadoRepository.deleteById(id);
    }
}
