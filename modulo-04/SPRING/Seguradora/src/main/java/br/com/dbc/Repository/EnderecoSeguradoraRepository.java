package br.com.dbc.Repository;

import br.com.dbc.Entity.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EnderecoSeguradoraRepository extends CrudRepository<EnderecoSeguradora, Long> {

    EnderecoSeguradora findByLougradouro(String lougradouro);
    EnderecoSeguradora findByNumero(int numero);
    EnderecoSeguradora findByComplemento(String complemento);
    EnderecoSeguradora findByBairro(Bairro bairro);
    List<EnderecoSeguradora> findByPessoas(Pessoa... pessoas);
    EnderecoSeguradora findBySeguradora(Seguradora seguradora);

}
