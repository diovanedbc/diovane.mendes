package br.com.dbc.Repository;

import br.com.dbc.Entity.Cidade;
import br.com.dbc.Entity.Estado;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EstadoRepository extends CrudRepository<Estado, Long> {

    Estado findByNome(String nome);
    List<Estado> findByCidades(Cidade... cidades);

}
