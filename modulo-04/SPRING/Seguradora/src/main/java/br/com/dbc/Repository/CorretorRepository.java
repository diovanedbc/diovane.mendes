package br.com.dbc.Repository;

import br.com.dbc.Entity.Corretor;
import br.com.dbc.Entity.Endereco;
import br.com.dbc.Entity.Pessoa;
import br.com.dbc.Entity.ServicoContratado;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CorretorRepository extends CrudRepository<Corretor, Long> {

    Pessoa findByNome(String nome);
    Pessoa findByCpf(long cpf);
    Pessoa findByPai(String pai);
    Pessoa findByMae(String mae);
    Pessoa findByTelefone(String numero);
    Pessoa findByEmail(String email);
    Pessoa findByEnderecos(Endereco... enderecos);
    Corretor findByComissao(double comissao);
    List<Corretor> findByServicoContratados(ServicoContratado... servicoContratado);

}
