package br.com.dbc.Factory;

import br.com.dbc.DTO.ServicoDTO;
import br.com.dbc.Entity.Servico;

import java.util.stream.Collectors;

public abstract class ServicoFactory {

    public static ServicoDTO getInstance(Servico servico){
        ServicoDTO servicoDTO = new ServicoDTO();

        servicoDTO.setId( servico.getId() );
        servicoDTO.setNome( servico.getNome() );
        servicoDTO.setDescricao( servico.getDescricao() );
        servicoDTO.setValorPadrao( servico.getValorPadrao() );
        servicoDTO.setSeguradoraDTOS( servico.getSeguradoras()
                .stream()
                .map(SeguradoraFactory::getInstance)
                .collect(Collectors.toList()) );

        return servicoDTO;
    }

    public static Servico getInstance(ServicoDTO servicoDTO){
        Servico servico = new Servico();

        servico.setId( servicoDTO.getId() );
        servico.setNome( servicoDTO.getNome() );
        servico.setDescricao( servicoDTO.getDescricao() );
        servico.setValorPadrao( servicoDTO.getValorPadrao() );
        servico.setSeguradoras( servicoDTO.getSeguradoraDTOS()
                .stream()
                .map(SeguradoraFactory::getInstance)
                .collect(Collectors.toList()) );

        return servico;
    }
}
