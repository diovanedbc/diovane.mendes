package br.com.dbc.DTO;

public class CorretorDTO extends PessoaDTO{

    private String cargo;

    private double comissao;

    public CorretorDTO() {
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getComissao() {
        return comissao;
    }

    public void setComissao(double comissao) {
        this.comissao = comissao;
    }
}
