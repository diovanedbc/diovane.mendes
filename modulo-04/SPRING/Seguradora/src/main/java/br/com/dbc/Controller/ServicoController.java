package br.com.dbc.Controller;

import br.com.dbc.DTO.ServicoDTO;
import br.com.dbc.Entity.Servico;
import br.com.dbc.Factory.ServicoFactory;
import br.com.dbc.Service.contrato.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/servico")
public class ServicoController implements PadraoController<ServicoDTO, Servico>{

    @Autowired
    private ServicoService servicoService;

    public ServicoDTO salvar(Servico servico) {
        return ServicoFactory.getInstance( servicoService.salvar(servico) );
    }

    public ServicoDTO buscarPorId(long id) {
        return ServicoFactory.getInstance( servicoService.buscarPorId(id) );
    }

    public List<ServicoDTO> buscarTodos() {
        return servicoService.buscarTodos()
                .stream()
                .map(ServicoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public ServicoDTO atualizar(long id, Servico servico) {
        return ServicoFactory.getInstance( servicoService.atualizar(id, servico) );
    }

    public void deletar(long id) {
        servicoService.deletar(id);
    }
}