package br.com.dbc.Service.contrato;

import br.com.dbc.Entity.Pessoa;
import br.com.dbc.Service.PadraoService;

public interface PessoaService extends PadraoService<Pessoa> {
}
