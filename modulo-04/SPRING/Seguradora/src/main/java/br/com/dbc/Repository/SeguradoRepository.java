package br.com.dbc.Repository;

import br.com.dbc.Entity.Endereco;
import br.com.dbc.Entity.Pessoa;
import br.com.dbc.Entity.Segurado;
import org.springframework.data.repository.CrudRepository;

public interface SeguradoRepository extends CrudRepository<Segurado, Long> {

    Segurado findByNome(String nome);
    Segurado findByCpf(long cpf);
    Segurado findByPai(String pai);
    Segurado findByMae(String mae);
    Segurado findByTelefone(String numero);
    Segurado findByEmail(String email);
    Segurado findByEnderecos(Endereco... enderecos);
    Segurado findByQuantidadeServico(int qunatidadeServico);

}
