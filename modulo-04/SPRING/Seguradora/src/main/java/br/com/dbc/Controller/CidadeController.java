package br.com.dbc.Controller;

import br.com.dbc.DTO.CidadeDTO;
import br.com.dbc.Entity.Cidade;
import br.com.dbc.Factory.CidadeFactory;
import br.com.dbc.Service.contrato.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/cidade")
public class CidadeController implements PadraoController<CidadeDTO, Cidade> {

    @Autowired
    private CidadeService cidadeService;

    public CidadeDTO salvar(Cidade cidade) {
        return CidadeFactory.getInstance( cidadeService.salvar(cidade) );
    }


    public CidadeDTO buscarPorId(long id) {
        return CidadeFactory.getInstance( cidadeService.buscarPorId(id) );
    }


    public List<CidadeDTO> buscarTodos() {
        return cidadeService.buscarTodos()
                .stream()
                .map(CidadeFactory::getInstance)
                .collect(Collectors.toList());
    }


    public CidadeDTO atualizar(long id, Cidade cidade) {
        return CidadeFactory.getInstance( cidadeService.atualizar(id, cidade) );
    }


    public void deletar(long id) {
        cidadeService.deletar(id);
    }
}
