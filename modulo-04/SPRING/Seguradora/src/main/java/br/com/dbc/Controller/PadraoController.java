package br.com.dbc.Controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface PadraoController<T, E> {

    @PostMapping("/salvar")
    @ResponseBody
    T salvar(@RequestBody E e);

    @GetMapping("/{id}")
    @ResponseBody
    T buscarPorId(@PathVariable long id);

    @GetMapping("/listar")
    @ResponseBody
    List<T> buscarTodos();

    @PutMapping("/atualizar/{id}")
    T atualizar(@PathVariable long id, @RequestBody E e);

    @DeleteMapping("/deletar/{id}")
    void deletar(long id);

}