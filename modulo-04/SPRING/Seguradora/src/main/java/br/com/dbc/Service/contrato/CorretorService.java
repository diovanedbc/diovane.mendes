package br.com.dbc.Service.contrato;

import br.com.dbc.Entity.Corretor;
import br.com.dbc.Service.PadraoService;

public interface CorretorService extends PadraoService<Corretor> {
}
