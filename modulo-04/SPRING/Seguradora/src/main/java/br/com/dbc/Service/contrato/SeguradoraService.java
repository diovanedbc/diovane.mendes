package br.com.dbc.Service.contrato;

import br.com.dbc.Entity.Seguradora;
import br.com.dbc.Service.PadraoService;

public interface SeguradoraService extends PadraoService<Seguradora> {
}
