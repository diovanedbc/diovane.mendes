package br.com.dbc.Controller;

import br.com.dbc.DTO.BairroDTO;
import br.com.dbc.Entity.Bairro;
import br.com.dbc.Factory.BairroFactory;
import br.com.dbc.Service.contrato.BairroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/bairro")
public class BairroController implements PadraoController<BairroDTO,Bairro>{

    @Autowired
    private BairroService bairroService;

    public BairroDTO salvar(Bairro bairro) {
        return BairroFactory.getInstance( bairroService.salvar(bairro) );
    }

    public BairroDTO buscarPorId(long id) {
        return BairroFactory.getInstance( bairroService.buscarPorId(id) );
    }

    public List<BairroDTO> buscarTodos() {
        return bairroService.buscarTodos()
                .stream()
                .map(BairroFactory::getInstance)
                .collect(Collectors.toList());
    }

    public BairroDTO atualizar(long id, Bairro bairro) {
        return BairroFactory.getInstance( bairroService.atualizar(id, bairro) );
    }

    public void deletar(long id) {
        bairroService.deletar(id);
    }
}