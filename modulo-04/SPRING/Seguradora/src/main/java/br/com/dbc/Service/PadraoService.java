package br.com.dbc.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PadraoService<T> {

    @Transactional(rollbackFor = Exception.class)
    T salvar(T t);

    @Transactional(rollbackFor = Exception.class)
    T buscarPorId(long id);

    @Transactional(rollbackFor = Exception.class)
    List<T> buscarTodos();

    @Transactional(rollbackFor = Exception.class)
    T atualizar(long id, T t);

    void deletar(long id);

}
