package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Bairro;
import br.com.dbc.Repository.BairroRepository;
import br.com.dbc.Service.contrato.BairroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BairroServiceImplementacao implements BairroService {

    @Autowired
    private BairroRepository bairroRepository;

    public Bairro salvar(Bairro bairro) {
        return bairroRepository.save(bairro);
    }

    public Bairro buscarPorId(long id) {
        return bairroRepository
                .findById(id)
                .orElse(null);
    }

    public List<Bairro> buscarTodos() {
        return (List<Bairro>) bairroRepository.findAll();
    }

    public Bairro atualizar(long id, Bairro bairro) {
        bairro.setId(id);
        return bairroRepository.save(bairro);
    }

    public void deletar(long id) {
        bairroRepository.deleteById(id);
    }
}
