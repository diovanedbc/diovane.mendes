package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Servico;
import br.com.dbc.Repository.ServicoRepository;
import br.com.dbc.Service.contrato.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicoServiceImplementacao implements ServicoService {

    @Autowired
    private ServicoRepository servicoRepository;

    public Servico salvar(Servico servico) {
        return servicoRepository.save(servico);
    }

    public Servico buscarPorId(long id) {
        return servicoRepository
                .findById(id)
                .orElse(null);
    }

    public List<Servico> buscarTodos() {
        return (List<Servico>) servicoRepository.findAll();
    }

    public Servico atualizar(long id, Servico servico) {
        servico.setId(id);
        return servicoRepository.save(servico);
    }

    public void deletar(long id) {
        servicoRepository.deleteById(id);
    }
}
