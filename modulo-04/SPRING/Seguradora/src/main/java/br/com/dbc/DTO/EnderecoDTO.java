package br.com.dbc.DTO;

public class EnderecoDTO {

    private long id;

    private String lougradouro;

    private int numero;

    private String complemento;

    private BairroDTO bairroDTO;

    public EnderecoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLougradouro() {
        return lougradouro;
    }

    public void setLougradouro(String lougradouro) {
        this.lougradouro = lougradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public BairroDTO getBairroDTO() {
        return bairroDTO;
    }

    public void setBairroDTO(BairroDTO bairroDTO) {
        this.bairroDTO = bairroDTO;
    }
}
