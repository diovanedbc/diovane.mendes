package br.com.dbc.DTO;

import java.util.ArrayList;
import java.util.List;

public class ServicoDTO {

    private long id;

    private String nome;

    private String descricao;

    private double valorPadrao;

    private List<SeguradoraDTO> seguradoraDTOS = new ArrayList<>();

    public ServicoDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValorPadrao() {
        return valorPadrao;
    }

    public void setValorPadrao(double valorPadrao) {
        this.valorPadrao = valorPadrao;
    }

    public List<SeguradoraDTO> getSeguradoraDTOS() {
        return seguradoraDTOS;
    }

    public void setSeguradoraDTOS(List<SeguradoraDTO> seguradoraDTOS) {
        this.seguradoraDTOS = seguradoraDTOS;
    }
}
