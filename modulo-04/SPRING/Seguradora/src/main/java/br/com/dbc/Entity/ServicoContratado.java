package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SERVICO_CONTRATADOS")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class ServicoContratado {

    @Id
    @GeneratedValue(generator = "SERVICO_CONTRATADO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_SERVICO", nullable = false)
    private Servico servico;

    @Column(name = "DESCRICAO", nullable = false)
    private String descricao;

    @Column(name = "VALOR", nullable = false)
    private double valor;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_SEGURADO", nullable = false)
    private Segurado segurado;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CORRETOR", nullable = false)
    private Corretor corretor;

    public ServicoContratado() {
    }

    @PrePersist
    public void antesDePersistir(){
        this.incrementaQuantidadeServicoSegurado();
        this.incrementaPorcentagemComissaoCorretor();
    }

    public void incrementaQuantidadeServicoSegurado(){
        this.segurado.inclementaQuantidadeServico();
    }

    public void incrementaPorcentagemComissaoCorretor(){
        this.corretor.incrementaPorcentagem(this.getValor());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Segurado getSegurado() {
        return segurado;
    }

    public void setSegurado(Segurado segurado) {
        this.segurado = segurado;
    }

    public Corretor getCorretor() {
        return corretor;
    }

    public void setCorretor(Corretor corretor) {
        this.corretor = corretor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServicoContratado)) return false;
        ServicoContratado that = (ServicoContratado) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getValor(), that.getValor()) &&
                Objects.equals(getServico(), that.getServico()) &&
                Objects.equals(getDescricao(), that.getDescricao()) &&
                Objects.equals(getSegurado(), that.getSegurado()) &&
                Objects.equals(getCorretor(), that.getCorretor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, servico, descricao, valor, segurado, corretor);
    }

    @Override
    public String toString() {
        return "ServicoContratado{" +
                "id=" + id +
                ", servico=" + servico +
                ", descricao='" + descricao + '\'' +
                ", valor=" + valor +
                ", segurado=" + segurado +
                ", corretor=" + corretor +
                '}';
    }
}