package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Segurado;
import br.com.dbc.Repository.SeguradoRepository;
import br.com.dbc.Service.contrato.SeguradoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeguradoServiceImplementacao implements SeguradoService {

    @Autowired
    private SeguradoRepository seguradoRepository;

    public Segurado salvar(Segurado segurado) {
        return seguradoRepository.save(segurado);
    }

    public Segurado buscarPorId(long id) {
        return seguradoRepository
                .findById(id)
                .orElse(null);
    }

    public List<Segurado> buscarTodos() {
        return (List<Segurado>) seguradoRepository.findAll();
    }

    public Segurado atualizar(long id, Segurado segurado) {
        segurado.setId(id);
        return seguradoRepository.save(segurado);
    }

    public void deletar(long id) {
        seguradoRepository.deleteById(id);
    }
}