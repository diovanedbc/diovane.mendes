package br.com.dbc.Factory;

import br.com.dbc.DTO.SeguradoraDTO;
import br.com.dbc.Entity.Seguradora;

public abstract class SeguradoraFactory {

    public static SeguradoraDTO getInstance(Seguradora seguradora) {
        SeguradoraDTO seguradoraDTO = new SeguradoraDTO();

        seguradoraDTO.setId( seguradora.getId() );
        seguradoraDTO.setNome( seguradora.getNome() );
        seguradoraDTO.setCnpj( seguradora.getCnpj() );
        seguradoraDTO.setEnderecoSeguradoraDTO( EnderecoSeguradoraFactory.getInstance(seguradora.getEnderecoSeguradora()) );

        return seguradoraDTO;
    }

    public static Seguradora getInstance(SeguradoraDTO seguradoraDTO) {
        Seguradora seguradora = new Seguradora();

        seguradora.setId( seguradoraDTO.getId() );
        seguradora.setNome( seguradoraDTO.getNome() );
        seguradora.setCnpj( seguradoraDTO.getCnpj() );
        seguradora.setEnderecoSeguradora( EnderecoSeguradoraFactory.getInstance(seguradoraDTO.getEnderecoSeguradoraDTO()) );

        return seguradora;
    }
}
