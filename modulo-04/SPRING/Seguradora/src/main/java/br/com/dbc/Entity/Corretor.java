package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "CORRETORES")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Corretor extends Pessoa{

    @Column(name = "CARGO", nullable = false)
    private String cargo;

    @Column(name = "COMISSAO")
    private double comissao;

    @OneToMany(mappedBy = "corretor")
    private List<ServicoContratado> servicoContratados = new ArrayList<>();

    public Corretor() {
    }
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getComissao() {
        return comissao;
    }

    public void setComissao(double comissao) {
        this.comissao = comissao;
    }

    public List<ServicoContratado> getServicoContratados() {
        return servicoContratados;
    }

    public void pushServicoContratados(ServicoContratado... servicoContratados) {
        this.servicoContratados.addAll( Arrays.asList(servicoContratados) );
    }

    public void incrementaPorcentagem(double valor){
        this.comissao += valor/100*10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Corretor)) return false;
        Corretor corretor = (Corretor) o;
        return Objects.equals(getComissao(), corretor.getComissao()) &&
                Objects.equals(getCargo(), corretor.getCargo()) &&
                Objects.equals(getServicoContratados(), corretor.getServicoContratados());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cargo, comissao, servicoContratados);
    }

    @Override
    public String toString() {
        return "Corretor{" +
                "cargo='" + cargo + '\'' +
                ", comissao=" + comissao +
                '}';
    }
}
