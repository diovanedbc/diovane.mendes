package br.com.dbc.Controller;

import br.com.dbc.DTO.EstadoDTO;
import br.com.dbc.Entity.Estado;
import br.com.dbc.Factory.EstadoFactory;
import br.com.dbc.Service.contrato.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/estado")
public class EstadoController implements PadraoController<EstadoDTO, Estado>{

    @Autowired
    private EstadoService estadoService;

    public EstadoDTO salvar(Estado estado) {
        return EstadoFactory.getInstance( estadoService.salvar(estado) );
    }

    public EstadoDTO buscarPorId(long id) {
        return EstadoFactory.getInstance( estadoService.buscarPorId(id) );
    }

    public List<EstadoDTO> buscarTodos() {
        return estadoService.buscarTodos()
                .stream()
                .map(EstadoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public EstadoDTO atualizar(long id, Estado estado) {
        return EstadoFactory.getInstance( estadoService.atualizar(id, estado) );
    }

    public void deletar(long id) {
        estadoService.deletar(id);
    }
}
