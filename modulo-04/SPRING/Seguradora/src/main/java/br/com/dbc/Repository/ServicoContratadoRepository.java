package br.com.dbc.Repository;

import br.com.dbc.Entity.*;
import org.springframework.data.repository.CrudRepository;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Long> {

    ServicoContratado findByServico(Servico servico);
    ServicoContratado findByDescricao(String descricao);
    ServicoContratado findByValor(double valor);
    ServicoContratado findBySegurado(Segurado segurado);
    ServicoContratado findByCorretor(Corretor corretor);

}
