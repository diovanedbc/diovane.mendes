package br.com.dbc.Service.contrato;

import br.com.dbc.DTO.RelatorioDTO;
import br.com.dbc.Entity.ServicoContratado;
import br.com.dbc.Service.PadraoService;

public interface ServicoContratadoService extends PadraoService<ServicoContratado> {

    RelatorioDTO buscaRelatorio(long id);

}
