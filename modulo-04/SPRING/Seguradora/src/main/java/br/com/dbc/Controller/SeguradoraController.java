package br.com.dbc.Controller;

import br.com.dbc.DTO.SeguradoraDTO;
import br.com.dbc.Entity.Seguradora;
import br.com.dbc.Factory.SeguradoraFactory;
import br.com.dbc.Service.contrato.SeguradoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/seguradora")
public class SeguradoraController implements PadraoController<SeguradoraDTO, Seguradora>{

    @Autowired
    private SeguradoraService seguradoraService;

    public SeguradoraDTO salvar(Seguradora seguradora) {
        return SeguradoraFactory.getInstance( seguradoraService.salvar(seguradora) );
    }

    public SeguradoraDTO buscarPorId(long id) {
        return SeguradoraFactory.getInstance( seguradoraService.buscarPorId(id) );
    }

    public List<SeguradoraDTO> buscarTodos() {
        return seguradoraService.buscarTodos()
                .stream()
                .map(SeguradoraFactory::getInstance)
                .collect(Collectors.toList());
    }

    public SeguradoraDTO atualizar(long id, Seguradora seguradora) {
        return SeguradoraFactory.getInstance( seguradoraService.atualizar(id, seguradora) );
    }

    public void deletar(long id) {
        seguradoraService.deletar(id);
    }
}
