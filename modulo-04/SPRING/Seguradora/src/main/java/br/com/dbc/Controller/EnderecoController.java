package br.com.dbc.Controller;

import br.com.dbc.DTO.EnderecoDTO;
import br.com.dbc.Entity.Endereco;
import br.com.dbc.Factory.EnderecoFactory;
import br.com.dbc.Service.contrato.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/endereco")
public class EnderecoController implements PadraoController<EnderecoDTO, Endereco>{

    @Autowired
    private EnderecoService enderecoService;

    public EnderecoDTO salvar(Endereco endereco) {
        return EnderecoFactory.getInstance( enderecoService.salvar(endereco) );
    }

    public EnderecoDTO buscarPorId(long id) {
        return EnderecoFactory.getInstance( enderecoService.buscarPorId(id) );
    }

    public List<EnderecoDTO> buscarTodos() {
        return enderecoService.buscarTodos()
                .stream()
                .map(EnderecoFactory::getInstance)
                .collect(Collectors.toList());
    }

    public EnderecoDTO atualizar(long id, Endereco endereco) {
        return EnderecoFactory.getInstance( enderecoService.atualizar(id, endereco) );
    }

    public void deletar(long id) {
        enderecoService.deletar(id);
    }
}
