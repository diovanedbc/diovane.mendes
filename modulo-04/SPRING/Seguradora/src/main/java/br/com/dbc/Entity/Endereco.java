package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "ENDERECOS")
@Inheritance(strategy = InheritanceType.JOINED)
public class Endereco implements Serializable {

    @Id
    @GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "LOGRADOURO", nullable = false)
    private String lougradouro;

    @Column(name = "NUMERO", nullable = false)
    private int numero;

    @Column(name = "COMPLEMENTO", nullable = false)
    private String complemento;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_BAIRRO", nullable = false)
    private Bairro bairro;

    @ManyToMany(mappedBy = "enderecos")
    private List<Pessoa> pessoas = new ArrayList<>();

    public Endereco() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLougradouro() {
        return lougradouro;
    }

    public void setLougradouro(String lougradouro) {
        this.lougradouro = lougradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public Bairro getBairro() {
        return bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Endereco)) return false;
        Endereco endereco = (Endereco) o;
        return Objects.equals(getId(), endereco.getId()) &&
                Objects.equals(getNumero(), endereco.getNumero()) &&
                Objects.equals(getLougradouro(), endereco.getLougradouro()) &&
                Objects.equals(getComplemento(), endereco.getComplemento()) &&
                Objects.equals(getBairro(), endereco.getBairro()) &&
                Objects.equals(getPessoas(), endereco.getPessoas());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lougradouro, numero, complemento, bairro, pessoas);
    }
}
