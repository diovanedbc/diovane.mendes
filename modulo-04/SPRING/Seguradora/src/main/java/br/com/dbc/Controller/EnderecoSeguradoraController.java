package br.com.dbc.Controller;

import br.com.dbc.DTO.EnderecoSeguradoraDTO;
import br.com.dbc.Entity.EnderecoSeguradora;
import br.com.dbc.Factory.EnderecoSeguradoraFactory;
import br.com.dbc.Service.contrato.EnderecoSeguradoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/endereco-seguradora")
public class EnderecoSeguradoraController implements PadraoController<EnderecoSeguradoraDTO, EnderecoSeguradora>{

    @Autowired
    private EnderecoSeguradoraService enderecoSeguradoraService;

    public EnderecoSeguradoraDTO salvar(EnderecoSeguradora enderecoSeguradora) {
        return EnderecoSeguradoraFactory.getInstance(
                enderecoSeguradoraService.salvar(enderecoSeguradora) );
    }

    public EnderecoSeguradoraDTO buscarPorId(long id) {
        return EnderecoSeguradoraFactory.getInstance(
                enderecoSeguradoraService.buscarPorId(id) );
    }

    public List<EnderecoSeguradoraDTO> buscarTodos() {
        return enderecoSeguradoraService.buscarTodos()
                .stream()
                .map(EnderecoSeguradoraFactory::getInstance)
                .collect(Collectors.toList());
    }

    public EnderecoSeguradoraDTO atualizar(long id, EnderecoSeguradora enderecoSeguradora) {
        return EnderecoSeguradoraFactory.getInstance(
                enderecoSeguradoraService.atualizar(id, enderecoSeguradora) );
    }

    public void deletar(long id) {
        enderecoSeguradoraService.deletar(id);
    }
}
