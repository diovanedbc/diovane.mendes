package br.com.dbc.Repository;

import br.com.dbc.Entity.Bairro;
import br.com.dbc.Entity.Cidade;
import br.com.dbc.Entity.Estado;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CidadeRepository extends CrudRepository<Cidade, Long> {

    Cidade findByEstado(Estado estado);
    Cidade findByNome(String nome);
    List<Cidade> findByBairros(Bairro... bairro);

}
