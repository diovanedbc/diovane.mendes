package br.com.dbc.Service.contrato;

import br.com.dbc.Entity.Endereco;
import br.com.dbc.Service.PadraoService;

public interface EnderecoService extends PadraoService<Endereco> {
}
