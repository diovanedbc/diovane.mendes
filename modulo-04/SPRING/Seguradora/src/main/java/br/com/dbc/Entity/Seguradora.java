package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SEGURADORAS")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Seguradora {

    @Id
    @GeneratedValue(generator = "SEGURADORA_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @Column(name = "CNPJ", unique = true, nullable = false)
    private long cnpj;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ENDERECO_SEGURADORA")
    private EnderecoSeguradora enderecoSeguradora;

    @ManyToMany(mappedBy = "seguradoras")
    private List<Servico> servicos = new ArrayList<>();

    public Seguradora() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCnpj() {
        return cnpj;
    }

    public void setCnpj(long cnpj) {
        this.cnpj = cnpj;
    }

    public EnderecoSeguradora getEnderecoSeguradora() {
        return enderecoSeguradora;
    }

    public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
        this.enderecoSeguradora = enderecoSeguradora;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void pushServicos(Servico... servicos) {
        this.servicos.addAll( Arrays.asList(servicos) );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Seguradora)) return false;
        Seguradora that = (Seguradora) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCnpj(), that.getCnpj()) &&
                Objects.equals(getNome(), that.getNome()) &&
                Objects.equals(getEnderecoSeguradora(), that.getEnderecoSeguradora()) &&
                Objects.equals(getServicos(), that.getServicos());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, cnpj, enderecoSeguradora, servicos);
    }

    @Override
    public String toString() {
        return "Seguradora{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cnpj=" + cnpj +
                ", enderecoSeguradora=" + enderecoSeguradora +
                '}';
    }
}
