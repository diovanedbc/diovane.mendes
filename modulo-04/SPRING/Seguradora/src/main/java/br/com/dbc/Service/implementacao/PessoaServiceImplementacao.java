package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Pessoa;
import br.com.dbc.Repository.PessoaRepository;
import br.com.dbc.Service.contrato.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PessoaServiceImplementacao implements PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    public Pessoa salvar(Pessoa pessoa) {
        return pessoaRepository.save( pessoa );
    }

    public Pessoa buscarPorId(long id) {
        return pessoaRepository.findById(id)
                .orElse(null);
    }

    public List<Pessoa> buscarTodos() {
        return (List<Pessoa>) pessoaRepository.findAll();
    }

    public Pessoa atualizar(long id, Pessoa pessoa) {
        pessoa.setId(id);
        return pessoaRepository.save( pessoa );
    }

    public void deletar(long id) {
        pessoaRepository.deleteById(id);
    }
}