package br.com.dbc.Factory;

import br.com.dbc.DTO.EnderecoDTO;
import br.com.dbc.Entity.Endereco;

public abstract class EnderecoFactory {

    public static EnderecoDTO getInstance(Endereco endereco){
        EnderecoDTO enderecoDTO = new EnderecoDTO();

        enderecoDTO.setId( endereco.getId() );
        enderecoDTO.setLougradouro( endereco.getLougradouro() );
        enderecoDTO.setNumero( endereco.getNumero() );
        enderecoDTO.setComplemento( endereco.getComplemento() );
        enderecoDTO.setBairroDTO( BairroFactory.getInstance(endereco.getBairro()) );

        return enderecoDTO;
    }

    public static Endereco getInstance(EnderecoDTO enderecoDTO){
        Endereco endereco = new Endereco();

        endereco.setId( enderecoDTO.getId() );
        endereco.setLougradouro( enderecoDTO.getLougradouro() );
        endereco.setNumero( enderecoDTO.getNumero() );
        endereco.setComplemento( enderecoDTO.getComplemento() );
        endereco.setBairro( BairroFactory.getInstance(enderecoDTO.getBairroDTO()) );

        return endereco;
    }
}