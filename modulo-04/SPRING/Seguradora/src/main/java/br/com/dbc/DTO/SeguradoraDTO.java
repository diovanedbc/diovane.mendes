package br.com.dbc.DTO;

public class SeguradoraDTO {

    private long id;

    private String nome;

    private long cnpj;

    private EnderecoSeguradoraDTO enderecoSeguradoraDTO;

    public SeguradoraDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCnpj() {
        return cnpj;
    }

    public void setCnpj(long cnpj) {
        this.cnpj = cnpj;
    }

    public EnderecoSeguradoraDTO getEnderecoSeguradoraDTO() {
        return enderecoSeguradoraDTO;
    }

    public void setEnderecoSeguradoraDTO(EnderecoSeguradoraDTO enderecoSeguradoraDTO) {
        this.enderecoSeguradoraDTO = enderecoSeguradoraDTO;
    }
}
