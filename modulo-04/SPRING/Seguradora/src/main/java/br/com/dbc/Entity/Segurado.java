package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "SEGURADOS")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Segurado extends Pessoa{

    @Column(name = "QUANTIDADE_SERVICO")
    private int quantidadeServico;

    public Segurado() {
    }

    public int getQuantidadeServico() {
        return quantidadeServico;
    }

    public void setQuantidadeServico(int quantidadeServico) {
        this.quantidadeServico = quantidadeServico;
    }

    public void inclementaQuantidadeServico(){
        this.quantidadeServico++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Segurado)) return false;
        if (!super.equals(o)) return false;
        Segurado segurado = (Segurado) o;
        return Objects.equals(getQuantidadeServico(), segurado.getQuantidadeServico());
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantidadeServico);
    }

    @Override
    public String toString() {
        return "Segurado{" +
                "quantidadeServico=" + quantidadeServico +
                '}';
    }
}
