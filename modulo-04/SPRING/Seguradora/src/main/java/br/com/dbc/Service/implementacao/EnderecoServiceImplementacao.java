package br.com.dbc.Service.implementacao;

import br.com.dbc.Entity.Endereco;
import br.com.dbc.Repository.EnderecoRepository;
import br.com.dbc.Service.contrato.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoServiceImplementacao implements EnderecoService {

    @Autowired
    private EnderecoRepository enderecoRepository;

    public Endereco salvar(Endereco endereco) {
        return enderecoRepository.save(endereco);
    }

    public Endereco buscarPorId(long id) {
        return enderecoRepository.findById(id)
                .orElse(null);
    }

    public List<Endereco> buscarTodos() {
        return (List<Endereco>) enderecoRepository.findAll();    }

    public Endereco atualizar(long id, Endereco endereco) {
        endereco.setId(id);
        return enderecoRepository.save(endereco);
    }

    public void deletar(long id) {
        enderecoRepository.deleteById(id);
    }
}
