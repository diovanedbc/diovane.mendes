package br.com.dbc.DTO;

import java.util.ArrayList;
import java.util.List;

public class PessoaDTO {

    private long id;

    private String nome;

    private long cpf;

    private String pai;

    private String mae;

    private String telefone;

    private String email;

    private List<EnderecoDTO> enderecosDTO = new ArrayList<>();

    public PessoaDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getPai() {
        return pai;
    }

    public void setPai(String pai) {
        this.pai = pai;
    }

    public String getMae() {
        return mae;
    }

    public void setMae(String mae) {
        this.mae = mae;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<EnderecoDTO> getEnderecosDTO() {
        return enderecosDTO;
    }

    public void setEnderecosDTO(List<EnderecoDTO> enderecosDTO) {
        this.enderecosDTO = enderecosDTO;
    }
}
