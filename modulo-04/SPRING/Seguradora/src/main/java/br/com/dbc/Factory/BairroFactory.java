package br.com.dbc.Factory;

import br.com.dbc.DTO.BairroDTO;
import br.com.dbc.Entity.Bairro;

public abstract class BairroFactory {

    public static BairroDTO getInstance(Bairro bairro){
        BairroDTO bairroDTO = new BairroDTO();

        bairroDTO.setId( bairro.getId() );
        bairroDTO.setNome( bairro.getNome() );
        bairroDTO.setCidadeDTO( CidadeFactory.getInstance(bairro.getCidade()) );

        return bairroDTO;
    }

    public static Bairro getInstance(BairroDTO bairroDTO){
        Bairro bairro = new Bairro();

        bairro.setId( bairroDTO.getId() );
        bairro.setNome( bairroDTO.getNome() );
        bairro.setCidade( CidadeFactory.getInstance(bairroDTO.getCidadeDTO()) );

        return bairro;
    }
}
