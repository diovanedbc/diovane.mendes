package br.com.dbc.Repository;

import br.com.dbc.Entity.Endereco;
import br.com.dbc.Entity.Pessoa;
import org.springframework.data.repository.CrudRepository;

public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

    Pessoa findByNome(String nome);
    Pessoa findByCpf(long cpf);
    Pessoa findByPai(String pai);
    Pessoa findByMae(String mae);
    Pessoa findByTelefone(String numero);
    Pessoa findByEmail(String email);
    Pessoa findByEnderecos(Endereco... enderecos);

}
