package br.com.dbc.Service.contrato;

import br.com.dbc.Entity.Estado;
import br.com.dbc.Service.PadraoService;

public interface EstadoService extends PadraoService<Estado> {
}
