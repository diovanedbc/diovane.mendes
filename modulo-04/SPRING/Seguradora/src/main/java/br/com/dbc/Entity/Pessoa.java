package br.com.dbc.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "PESSOAS")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Pessoa implements Serializable {

    @Id
    @GeneratedValue(generator = "PESSOA_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @Column(name = "CPF", unique = true, nullable = false)
    private long cpf;

    @Column(name = "PAI", nullable = false)
    private String pai;

    @Column(name = "MAE", nullable = false)
    private String mae;

    @Column(name = "TELEFONE", unique = true, nullable = false)
    private String telefone;

    @Column(name = "EMAIL", unique = true, nullable = false)
    private String email;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ENDERECO_PESSOAS",
            joinColumns = {
                    @JoinColumn(name = "ID_PESSOA", nullable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "ID_ENDERECO", nullable = false)})
    private List<Endereco> enderecos = new ArrayList<>();

    public Pessoa() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getPai() {
        return pai;
    }

    public void setPai(String pai) {
        this.pai = pai;
    }

    public String getMae() {
        return mae;
    }

    public void setMae(String mae) {
        this.mae = mae;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        this.enderecos.addAll( Arrays.asList(enderecos) );
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pessoa)) return false;
        Pessoa pessoa = (Pessoa) o;
        return Objects.equals(getId(), pessoa.getId()) &&
                Objects.equals(getCpf(), pessoa.getCpf()) &&
                Objects.equals(getNome(), pessoa.getNome()) &&
                Objects.equals(getPai(), pessoa.getPai()) &&
                Objects.equals(getMae(), pessoa.getMae()) &&
                Objects.equals(getTelefone(), pessoa.getTelefone()) &&
                Objects.equals(getEmail(), pessoa.getEmail()) &&
                Objects.equals(getEnderecos(), pessoa.getEnderecos());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, cpf, pai, mae, telefone, email, enderecos);
    }
}