package br.com.dbc.Controller;

import br.com.dbc.DTO.CorretorDTO;
import br.com.dbc.Entity.Corretor;
import br.com.dbc.Factory.CorretorFactory;
import br.com.dbc.Service.contrato.CorretorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/corretor")
public class CorretorController implements PadraoController<CorretorDTO, Corretor>{

    @Autowired
    private CorretorService corretorService;

    public CorretorDTO salvar(Corretor corretor) {
        return CorretorFactory.getInstance( corretorService.salvar(corretor) );
    }

    public CorretorDTO buscarPorId(long id) {
        return CorretorFactory.getInstance( corretorService.buscarPorId(id) );
    }

    public List<CorretorDTO> buscarTodos() {
        return corretorService.buscarTodos()
                .stream()
                .map(CorretorFactory::getInstance)
                .collect(Collectors.toList());
    }

    public CorretorDTO atualizar(long id, Corretor corretor) {
        return CorretorFactory.getInstance( corretorService.atualizar(id, corretor) );
    }

    public void deletar(long id) {
        corretorService.deletar(id);
    }
}
