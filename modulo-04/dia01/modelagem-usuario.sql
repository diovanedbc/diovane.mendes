/*
CREATE TABLE TESTE(
  ID NUMBER NOT NULL PRIMARY KEY
);

DROP TABLE TESTE
*/

CREATE TABLE USUARIO(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  APELIDO VARCHAR(15) NOT NULL,
  SENHA VARCHAR(15) NOT NULL,
  CPF NUMBER NOT NULL
);

CREATE TABLE CONTATO(
  ID NUMBER NOT NULL PRIMARY KEY,
  VALOR VARCHAR(100),
  ID_USUARIO NUMBER NOT NULL REFERENCES USUARIO(ID)
);

CREATE TABLE TIPO_CONTATO(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  QUANTIDADE NUMBER NOT NULL
);

ALTER TABLE CONTATO ADD ID_TPO_CONTATO NUMBER NOT NULL REFERENCES TIPO_CONTATO(ID);

CREATE SEQUENCE USUARIO_SEQ
START WITH 1
INCREMENT BY 1;

USUARIO_SEQ.NEXTVAL;

CREATE SEQUENCE CONTATO_SEQ
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE TIPO_CONTATO_SEQ
START WITH 1
INCREMENT BY 1;

INSERT INTO USUARIO 
  (ID, NOME, APELIDO, SENHA, CPF) 
VALUES
  (USUARIO_SEQ.NEXTVAL, 'diovane', 'dio', 'totodaio', 08906574069);

SELECT * FROM USUARIO;

UPDATE USUARIO SET SENHA='123456' WHERE ID=1;

DELETE FROM USUARIO WHERE ID=21;