CREATE TABLE usuario (
  id NUMBER NOT NULL PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  apelido VARCHAR(20) NOT NULL,
  senha VARCHAR(8) NOT NULL,
  cpf NUMBER NOT NULL
);

CREATE TABLE endereco (
  id NUMBER NOT NULL PRIMARY KEY,
  logradouro VARCHAR(100) NOT NULL,
  numero NUMBER NOT NULL,
  complemento VARCHAR(50) NULL,
  bairro VARCHAR(100) NULL,
  cidade VARCHAR(100) NULL
);

CREATE TABLE usuario_endereco (
  id NUMBER NOT NULL PRIMARY KEY
);

CREATE TABLE contato (
  id NUMBER NOT NULL PRIMARY KEY,
  valor VARCHAR(100) NOT NULL
);

CREATE TABLE tipo_contato (
  id NUMBER NOT NULL PRIMARY KEY,
  nome VARCHAR(100) NULL,
  quantidade NUMBER DEFAULT 1
);

CREATE TABLE cartao_credito (
  id NUMBER NOT NULL PRIMARY KEY,
  numero NUMBER NOT NULL,
  bandeira VARCHAR(50) NOT NULL,
  nome_escrito VARCHAR(100) NOT NULL,
  codigo_seguranca NUMBER NOT NULL,
  validade VARCHAR(50) NOT NULL
);

CREATE TABLE personagem (
  id NUMBER NOT NULL PRIMARY KEY,
  nome VARCHAR(100) NOT NULL,
  vida SMALLINT DEFAULT 100,
  dano SMALLINT DEFAULT 20,
  experiencia SMALLINT DEFAULT 0
);

CREATE TABLE status (
  id NUMBER NOT NULL PRIMARY KEY
);

CREATE TABLE inventario (
  id NUMBER NOT NULL PRIMARY KEY
);

CREATE TABLE raca (
  id NUMBER NOT NULL PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  limitador NUMBER NOT NULL,
  vida_inicio NUMBER DEFAULT 100,
  dano_inicio NUMBER DEFAULT 0
);

------------------ FK's ------------------
ALTER TABLE usuario_endereco ADD id_usuario NUMBER NOT NULL REFERENCES usuario(id);
ALTER TABLE usuario_endereco ADD id_endereco NUMBER NOT NULL REFERENCES endereco(id);
ALTER TABLE contato ADD id_usuario NUMBER NOT NULL REFERENCES usuario(id);
ALTER TABLE contato ADD id_tipo_contato NUMBER NOT NULL REFERENCES tipo_contato(id);
ALTER TABLE cartao_credito ADD id_usuario NUMBER NOT NULL REFERENCES usuario(id);
ALTER TABLE personagem ADD id_usuario NUMBER NOT NULL REFERENCES usuario(id);
ALTER TABLE personagem ADD id_inventario NUMBER NOT NULL REFERENCES inventario(id);
ALTER TABLE personagem ADD id_raca NUMBER NOT NULL REFERENCES raca(id);
ALTER TABLE status ADD id_personagem NUMBER NOT NULL REFERENCES personagem(id);

---------------- SEQUENCES ----------------
--usuario_seq.NEXTVAL;
CREATE SEQUENCE usuario_seq
START WITH 1
INCREMENT BY 1;
--endereco_seq.NEXTVAL;
CREATE SEQUENCE endereco_seq
START WITH 1
INCREMENT BY 1;
--usuario_endereco_seq.NEXTVAL;
CREATE SEQUENCE usuario_endereco_seq
START WITH 1
INCREMENT BY 1;
--contato_seq.NEXTVAL;
CREATE SEQUENCE contato_seq
START WITH 1
INCREMENT BY 1;
--tipo_contato_seq.NEXTVAL;
CREATE SEQUENCE tipo_contato_seq
START WITH 1
INCREMENT BY 1;
--cartao_credito_seq.NEXTVAL;
CREATE SEQUENCE cartao_credito_seq
START WITH 1
INCREMENT BY 1;
--personagem_seq.NEXTVAL;
CREATE SEQUENCE personagem_seq
START WITH 1
INCREMENT BY 1;
--status_seq.NEXTVAL;
CREATE SEQUENCE status_seq
START WITH 1
INCREMENT BY 1;
--inventario_seq.NEXTVAL;
CREATE SEQUENCE inventario_seq
START WITH 1
INCREMENT BY 1;
--raca_seq.NEXTVAL;
CREATE SEQUENCE raca_seq
START WITH 1
INCREMENT BY 1;

------------------ DROPS ------------------
--Apagando usuario e suas referencias
ALTER TABLE CONTATO DROP COLUMN ID_USUARIO;
ALTER TABLE USUARIO_ENDERECO DROP COLUMN ID_USUARIO;
ALTER TABLE CARTAO_CREDITO DROP COLUMN ID_USUARIO;
ALTER TABLE PERSONAGEM DROP COLUMN ID_USUARIO;
DROP TABLE USUARIO;
--Apagando endereco e suas referencias
ALTER TABLE USUARIO_ENDERECO DROP COLUMN ID_ENDERECO;
DROP TABLE ENDERECO;
--Apagando usuario_endereco
DROP TABLE USUARIO_ENDERECO;
--Apagando contato
DROP TABLE CONTATO;
--Apagando tipo_contato e suas referencias
ALTER TABLE CONTATO DROP COLUMN ID_TIPO_CONTATO;
DROP TABLE TIPO_CONTATO;
--Apagando cartao_credito
DROP TABLE CARTAO_CREDITO;
--Apagando personagem e suas referencias
ALTER TABLE STATUS DROP COLUMN ID_PERSONAGEM;
DROP TABLE PERSONAGEM;
--Apagando status
DROP TABLE STATUS;
--Apagando inventario
DROP TABLE INVENTARIO;
--Apagando raca
DROP TABLE RACA;

------------------ INSERT's ------------------
-- usuario
INSERT INTO usuario (id, nome, email, apelido, senha, cpf) VALUES
(usuario_seq.NEXTVAL,'Amanda','amanda.nunes@gmail.com','amands','123456',45698711334);
INSERT INTO usuario (id, nome, email, apelido, senha, cpf) VALUES
(usuario_seq.NEXTVAL,'Everton','everton.alberto@gmail.com','cebolinha','654123',91874658302);
INSERT INTO usuario (id, nome, email, apelido, senha, cpf) VALUES
(usuario_seq.NEXTVAL,'Camila','cami.azevedo@gmail.com','camilaaa','790890',035040081952);
-- endereco
INSERT INTO endereco (id, logradouro, numero, complemento, bairro, cidade) VALUES
(endereco_seq.NEXTVAL,'.',150,'casa','Moinhos de Vento','POA');
INSERT INTO endereco (id, logradouro, numero, complemento, bairro, cidade) VALUES
(endereco_seq.NEXTVAL,'.',2167,'bloco-A','Partenon','POA');
INSERT INTO endereco (id, logradouro, numero, complemento, bairro, cidade) VALUES
(endereco_seq.NEXTVAL,'.',838,'casa 5','Auxiliadora','POA');
-- usuario_endereco 
INSERT INTO usuario_endereco (id, id_usuario, id_endereco) VALUES
(usuario_endereco_seq.NEXTVAL,1,3);
INSERT INTO usuario_endereco (id, id_usuario, id_endereco) VALUES
(usuario_endereco_seq.NEXTVAL,3,2);
INSERT INTO usuario_endereco (id, id_usuario, id_endereco) VALUES
(usuario_endereco_seq.NEXTVAL,2,1);
-- tipo_contato
INSERT INTO tipo_contato (id, nome ,quantidade) VALUES
(tipo_contato_seq.NEXTVAL,'Email',1);
INSERT INTO tipo_contato (id, nome ,quantidade) VALUES
(tipo_contato_seq.NEXTVAL,'WhatsApp',1);
INSERT INTO tipo_contato (id, nome ,quantidade) VALUES
(tipo_contato_seq.NEXTVAL,'Telefone',2);
-- contato
INSERT INTO contato (id, id_usuario, id_tipo_contato, valor) VALUES
(contato_seq.NEXTVAL,3,1,'amanda.nunes@gmail.com');
INSERT INTO contato (id, id_usuario, id_tipo_contato, valor) VALUES
(contato_seq.NEXTVAL,2,3,'(51) 98560-3120');
INSERT INTO contato (id, id_usuario, id_tipo_contato, valor) VALUES
(contato_seq.NEXTVAL,1,2,'(51) 99605-7408');
-- cartao_credito
INSERT INTO cartao_credito (id, id_usuario, numero, bandeira, nome_escrito, codigo_seguranca, validade) VALUES
(cartao_credito_seq.NEXTVAL,2,8945348463,'Visa','EVERTON B. ALBERTO',890,'08/2024');
INSERT INTO cartao_credito (id, id_usuario, numero, bandeira, nome_escrito, codigo_seguranca, validade) VALUES
(cartao_credito_seq.NEXTVAL,1,7856402354,'Hipercard','AMANDA Z. NUNES',003,'10/2022');
INSERT INTO cartao_credito (id, id_usuario, numero, bandeira, nome_escrito, codigo_seguranca, validade) VALUES
(cartao_credito_seq.NEXTVAL,3,1742983047,'Master','CAMILA S. AZEVEDO',115,'03/2019');
-- inventario
INSERT INTO inventario (id) VALUES
(inventario_seq.NEXTVAL);
INSERT INTO inventario (id) VALUES
(inventario_seq.NEXTVAL);
INSERT INTO inventario (id) VALUES
(inventario_seq.NEXTVAL);
-- raca
INSERT INTO raca (id,nome,limitador) VALUES
(raca_seq.NEXTVAL,'Dwarf',1);
INSERT INTO raca (id,nome,limitador) VALUES
(raca_seq.NEXTVAL,'Elfo Verde',1);
INSERT INTO raca (id,nome,limitador) VALUES
(raca_seq.NEXTVAL,'Elfo da Luz',1);
-- personagem
INSERT INTO personagem (id,id_usuario,id_inventario,nome,id_raca) VALUES
(personagem_seq.NEXTVAL,2,1,'Totodaio',2);
INSERT INTO personagem (id,id_usuario,id_inventario,nome,id_raca) VALUES
(personagem_seq.NEXTVAL,3,2,'Masquerano',1);
INSERT INTO personagem (id,id_usuario,id_inventario,nome,id_raca) VALUES
(personagem_seq.NEXTVAL,1,3,'Tobias',3);
-- status
INSERT INTO status (id,id_personagem) VALUES
(status_seq.NEXTVAL,1);
INSERT INTO status (id,id_personagem) VALUES
(status_seq.NEXTVAL,2);
INSERT INTO status (id,id_personagem) VALUES
(status_seq.NEXTVAL,3);

------------------ UPDATE's ------------------
UPDATE PERSONAGEM SET NOME='Naeryndam' where ID=1;
UPDATE PERSONAGEM SET NOME='Ilmadia' where ID=3;

------------------ SELECT's ------------------
SELECT * FROM USUARIO;
SELECT * FROM ENDERECO;
SELECT * FROM USUARIO_ENDERECO;
SELECT * FROM CONTATO;
SELECT * FROM TIPO_CONTATO;
SELECT * FROM CARTAO_CREDITO;
SELECT * FROM PERSONAGEM;
SELECT * FROM STATUS;
SELECT * FROM INVENTARIO;
SELECT * FROM RACA;