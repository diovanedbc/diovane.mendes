-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'pais'
-- 
-- ---

DROP TABLE IF EXISTS `pais`;
		
CREATE TABLE `pais` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'estados'
-- 
-- ---

DROP TABLE IF EXISTS `estados`;
		
CREATE TABLE `estados` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `id_pais` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'cidades'
-- 
-- ---

DROP TABLE IF EXISTS `cidades`;
		
CREATE TABLE `cidades` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `id_estados` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'banco'
-- 
-- ---

DROP TABLE IF EXISTS `banco`;
		
CREATE TABLE `banco` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `codigo` SMALLINT NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'cliente'
-- 
-- ---

DROP TABLE IF EXISTS `cliente`;
		
CREATE TABLE `cliente` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `cpf` BIGINT NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `telefone` BIGINT NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'conta'
-- 
-- ---

DROP TABLE IF EXISTS `conta`;
		
CREATE TABLE `conta` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `numero` SMALLINT NOT NULL,
  `senha` SMALLINT NOT NULL,
  `tipo` VARCHAR(100) NOT NULL,
  `id_banco` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'lancamento'
-- 
-- ---

DROP TABLE IF EXISTS `lancamento`;
		
CREATE TABLE `lancamento` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `id_contas_origem` INTEGER NOT NULL,
  `data` DATE NOT NULL,
  `valor` DECIMAL NOT NULL,
  `id_conta` DECIMAL NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'sede'
-- 
-- ---

DROP TABLE IF EXISTS `sede`;
		
CREATE TABLE `sede` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `capacidade` SMALLINT NOT NULL,
  `id_cidades` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'conta_cliente'
-- 
-- ---

DROP TABLE IF EXISTS `conta_cliente`;
		
CREATE TABLE `conta_cliente` (
  `id` INTEGER NULL AUTO_INCREMENT DEFAULT NULL,
  `id_conta` INTEGER NULL DEFAULT NULL,
  `id_cliente` INTEGER NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `estados` ADD FOREIGN KEY (id_pais) REFERENCES `pais` (`id`);
ALTER TABLE `cidades` ADD FOREIGN KEY (id_estados) REFERENCES `estados` (`id`);
ALTER TABLE `conta` ADD FOREIGN KEY (id_banco) REFERENCES `banco` (`id`);
ALTER TABLE `lancamento` ADD FOREIGN KEY (id_contas_origem) REFERENCES `conta` (`id`);
ALTER TABLE `lancamento` ADD FOREIGN KEY (id_conta) REFERENCES `conta` (`id`);
ALTER TABLE `sede` ADD FOREIGN KEY (id_cidades) REFERENCES `cidades` (`id`);
ALTER TABLE `conta_cliente` ADD FOREIGN KEY (id_conta) REFERENCES `conta` (`id`);
ALTER TABLE `conta_cliente` ADD FOREIGN KEY (id_cliente) REFERENCES `cliente` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `pais` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `estados` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cidades` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `banco` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cliente` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `conta` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `lancamento` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `sede` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `conta_cliente` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `pais` (`id`,`nome`) VALUES
-- ('','');
-- INSERT INTO `estados` (`id`,`nome`,`id_pais`) VALUES
-- ('','','');
-- INSERT INTO `cidades` (`id`,`nome`,`id_estados`) VALUES
-- ('','','');
-- INSERT INTO `banco` (`id`,`nome`,`codigo`) VALUES
-- ('','','');
-- INSERT INTO `cliente` (`id`,`nome`,`cpf`,`email`,`telefone`) VALUES
-- ('','','','','');
-- INSERT INTO `conta` (`id`,`numero`,`senha`,`tipo`,`id_banco`) VALUES
-- ('','','','','');
-- INSERT INTO `lancamento` (`id`,`id_contas_origem`,`data`,`valor`,`id_conta`) VALUES
-- ('','','','','');
-- INSERT INTO `sede` (`id`,`nome`,`capacidade`,`id_cidades`) VALUES
-- ('','','','');
-- INSERT INTO `conta_cliente` (`id`,`id_conta`,`id_cliente`) VALUES
-- ('','','');

