package br.com.dbc.lotr;

import br.com.dbc.lotr.model.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    
    public static void main(String[] args) {        
        Session session = null;        
        Transaction transaction = null; 
        
        try{
            session = HibernateUtil.getSessionFactory().openSession(); 
            transaction = session.beginTransaction();
            
            session.save( new Usuario("Diovane", "Dio", "lotr", 123456) );
            
            session.save( new ElfoTabelao("Totodaio", 20.0) );
            
            session.save( new HobbitTabelao("Totodaio Hobbit", 30.0) );
            
            transaction.commit();
        } catch(Exception e){
            if( transaction != null )
                transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            if( session != null)
                session.close();
        }       
    }
}
