
package br.com.dbc.lotr.model;

import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;

/*
 * @author diovane.mendes
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Erro na ligação com o banco!" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
