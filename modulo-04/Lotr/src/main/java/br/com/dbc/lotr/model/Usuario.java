
package br.com.dbc.lotr.model;

import javax.persistence.*;

/**
 *
 * @author diovane.mendes
 */

@Entity
@Table(name = "USUARIOS")
public class Usuario {    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "usuario_seq", sequenceName = "usuario_seq")
    @GeneratedValue(generator = "usuario_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;    
    private String apelido;
    private String senha;
    private Integer cpf;

    public Usuario( String nome, String apelido, String senha, Integer cpf) {
        this.nome = nome;
        this.apelido = apelido;
        this.senha = senha;
        this.cpf = cpf;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Integer getCpf() {
        return cpf;
    }

    public void setCpf(Integer cpf) {
        this.cpf = cpf;
    }    
}
