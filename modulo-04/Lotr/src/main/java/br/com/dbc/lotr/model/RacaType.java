
package br.com.dbc.lotr.model;

/*
 * @author diovane.mendes
 */

public enum RacaType {
    ELFO, HOBBIT, DWARF;
}
