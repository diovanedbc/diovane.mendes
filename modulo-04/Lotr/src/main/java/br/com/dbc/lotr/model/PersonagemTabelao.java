
package br.com.dbc.lotr.model;

import javax.persistence.*;

/*
 * @author diovane.mendes
 */

@Entity
@Table(name = "PERSONAGEM_TABELAO")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "raca", discriminatorType = DiscriminatorType.STRING)
public class PersonagemTabelao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_TAB_SEQ", sequenceName = "PERSONAGEM_TAB_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_TAB_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;

    public PersonagemTabelao(String nome) {
        this.nome = nome;
    }   
}
