
package br.com.dbc.lotr.model;

import javax.persistence.*;

/*
 * @author diovane.mendes
 */
@Entity
@Table
@DiscriminatorValue("ELFO")
public class ElfoTabelao extends PersonagemTabelao{
    
    @Column(name = "DANO_ELFO")
    private Double danoElfo;

    public ElfoTabelao(String nome, Double danoElfo) {
        super(nome);
        this.danoElfo = danoElfo;
    }
    
    public Double getDanoElfo() {
        return danoElfo;
    }

    public void setDanoElfo(Double danoElfo) {
        this.danoElfo = danoElfo;
    }
}