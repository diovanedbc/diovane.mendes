
package br.com.dbc.lotr.model;

import javax.persistence.*;

/*
 * @author diovane.mendes
 */
@Entity
@Table
@DiscriminatorValue("HOBBIT")
public class HobbitTabelao extends PersonagemTabelao{
    private Double danoHobbit;

    public HobbitTabelao(String nome, Double danoHobbit) {
        super(nome);
        this.danoHobbit = danoHobbit;
    }

    public Double getDanoHobbit() {
        return danoHobbit;
    }

    public void setDanoHobbit(Double danoHobbit) {
        this.danoHobbit = danoHobbit;
    }
}
