import javax.persistence.*;

@Entity
@Table(name = "CHEQUE_ESPECIAL")
public class ChequeEspecial {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cheque_especial_seq", sequenceName = "cheque_especial_seq")
    @GeneratedValue(generator = "cheque_especial_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToOne(mappedBy = "chequeEspecial")
    private Conta conta;
}
