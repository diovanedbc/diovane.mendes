import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "AGENCIA")
public class Agencia {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "agencia_seq", sequenceName = "agencia_seq")
    @GeneratedValue(generator = "agencia_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToMany(mappedBy = "agencia")
    private List<Conta3> listaContas3;
}
