import javax.persistence.*;

@Entity
@Table(name = "CONTA_3")
public class Conta3 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta3_seq", sequenceName = "conta3_seq")
    @GeneratedValue(generator = "conta3_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_agencia")
    private Agencia agencia;
}
