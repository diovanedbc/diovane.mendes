import javax.persistence.*;

@Entity
@Table(name = "CONTA")
public class Conta {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta_seq", sequenceName = "conta_seq")
    @GeneratedValue(generator = "conta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id_cheque_especial")
    private ChequeEspecial chequeEspecial;
}
