
package br.com.dbc;

import br.com.dbc.model.Cliente;
import br.com.dbc.model.Lancamento;
import br.com.dbc.model.Loja;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author diovane.mendes
 */
public class InsertsBanco {
    private static final Connection conn = Connector.connect();
    
    public static void inserirLoja(Loja loja){        
        try {
            String sql = "INSERT INTO LOJA (ID, NOME) "
                    + "VALUES (LOJA_SEQ.NEXTVAL, ?)";
            
            PreparedStatement pst = conn.prepareStatement(sql);            
            pst.setString(1, loja.getNome());
            
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, "Erro na inserção da loja!", ex);
        }
    }
    
    public static void inserirCliente(Cliente cliente){
        try {
            String sql = "INSERT INTO CLIENTE (ID, NOME) "
                    + "VALUES (CLIENTE_SEQ.NEXTVAL, ?)";
            
            PreparedStatement pst = conn.prepareStatement(sql);            
            pst.setString(1, cliente.getNome());
            
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, "Erro na inserção do cliente!", ex);
        }
    }
    
    public static void efetuaVenda(Lancamento lancamento){
        
    }
}
