
package br.com.dbc.model;

/**
 *
 * @author diovane.mendes
 */
public class Loja {
    private String nome;

    public Loja(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
