
package br.com.dbc.model;

import java.time.LocalDate;

/*
 * @author diovane.mendes
 */
public class Lancamento {
    private String descricao;
    private double valor;
    private LocalDate data;

    public Lancamento(String descricao, double valor, LocalDate data) {
        this.descricao = descricao;
        this.valor = valor;
        this.data = data;
    }
}
