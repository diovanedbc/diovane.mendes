
package br.com.dbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author diovane.mendes
 */
public class Main {
    public static void main(String[] args) {
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'USUARIO'")
                    .executeQuery();
            
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE USUARIO(\n" +
                    "  ID NUMBER NOT NULL PRIMARY KEY,\n" +
                    "  NOME VARCHAR(100) NOT NULL,\n" +
                    "  APELIDO VARCHAR(15) NOT NULL,\n" +
                    "  SENHA VARCHAR(15) NOT NULL,\n" +
                    "  CPF NUMBER NOT NULL\n" +
                    ")").execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, "Erro na consulta do Main", ex);
        }
    }
}
