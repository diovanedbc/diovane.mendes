package br.com.dbc.model;

import javax.persistence.*;

@Entity
@Table(name = "EMISSOR")
public class Emissor {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "emissor_seq", sequenceName = "emissor_seq")
    @GeneratedValue(generator = "emissor_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String nome;

    private double taxa;

    public Emissor() {

    }

    public Emissor(String nome, double taxa) {
        this.nome = nome;
        this.taxa = taxa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTaxa() {
        return taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    @Override
    public String toString() {
        return "Emissor{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", taxa=" + taxa +
                '}';
    }
}