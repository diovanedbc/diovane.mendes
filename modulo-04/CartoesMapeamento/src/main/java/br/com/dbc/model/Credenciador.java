/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author diovane.mendes
 */
@Entity
@Table(name = "CREDENCIADORES")
public class Credenciador {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "credenciador_seq", sequenceName = "credenciador_seq")
    @GeneratedValue(generator = "credenciador_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToMany(mappedBy = "credenciadores")
    private List<Loja> lojas = new ArrayList<>();
       
    private String nome;

    public Credenciador() {

    }

    public Credenciador(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Loja> getCredenciadores(){
        return lojas;
    }

    public void pushCredenciadores(Loja... lojas){
        this.lojas.addAll(Arrays.asList(lojas));
    }

    @Override
    public String toString() {
        return "Credenciador{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                '}';
    }
}