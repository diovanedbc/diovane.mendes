package br.com.dbc.model;

import javax.persistence.*;

@Entity
@Table(name = "BANDEIRAS")
public class Bandeira {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "bandeira_seq", sequenceName = "bandeira_seq")
    @GeneratedValue(generator = "bandeira_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String nome;
    private double taxa;

    public Bandeira() {

    }

    public Bandeira(String nome, double taxa) {
        this.nome = nome;
        this.taxa = taxa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTaxa() {
        return taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    @Override
    public String toString() {
        return "Bandeira{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", taxa=" + taxa +
                '}';
    }
}