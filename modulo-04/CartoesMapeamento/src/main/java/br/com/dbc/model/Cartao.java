package br.com.dbc.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "CARTAO")
public class Cartao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cartao_seq", sequenceName = "cartao_seq")
    @GeneratedValue(generator = "cartao_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String chip;

    @OneToOne(cascade = CascadeType.ALL)
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    private Bandeira bandeira;

    @ManyToOne(cascade = CascadeType.ALL)
    private Emissor emissor;

    private LocalDate vencimento;

    public Cartao() {

    }

    public Cartao(String chip,
                  Cliente cliente,
                  Bandeira bandeira,
                  Emissor emissor,
                  LocalDate vencimento) {
        this.chip = chip;
        this.cliente = cliente;
        this.bandeira = bandeira;
        this.emissor = emissor;
        this.vencimento = vencimento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Bandeira getBandeira() {
        return bandeira;
    }

    public void setBandeira(Bandeira bandeira) {
        this.bandeira = bandeira;
    }

    public Emissor getEmissor() {
        return emissor;
    }

    public void setEmissor(Emissor emissor) {
        this.emissor = emissor;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    @Override
    public String toString() {
        return "Cartao{" +
                "id=" + id +
                ", chip='" + chip + '\'' +
                ", cliente=" + cliente +
                ", bandeira=" + bandeira +
                ", emissor=" + emissor +
                ", vencimento=" + vencimento +
                '}';
    }
}
