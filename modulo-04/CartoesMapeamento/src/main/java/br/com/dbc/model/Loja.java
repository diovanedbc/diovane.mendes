/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "LOJAS")
public class Loja {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "loja_seq", sequenceName = "loja_seq")
    @GeneratedValue(generator = "loja_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "LOJA_CREDENCIADOR",
            joinColumns = {
                    @JoinColumn(name = "id_loja")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_credenciador")})
    private List<Credenciador> credenciadores = new ArrayList<>();

    private String nome;

    public Loja() {

    }

    public Loja(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Credenciador> getCredenciadores(){
        return credenciadores;
    }

    public void pushCredenciadores(Credenciador... credenciadores){
        this.credenciadores.addAll(Arrays.asList(credenciadores));
    }

    @Override
    public String toString() {
        return "Loja{" +
                "id=" + id +
                ", credenciadores=" + credenciadores +
                ", nome='" + nome + '\'' +
                '}';
    }
}
