package br.com.dbc.service;

import br.com.dbc.app.HibernateUtil;
import br.com.dbc.app.Main;
import br.com.dbc.model.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Consulta {
    private static final Logger LOG = Logger.getLogger( Main.class.getName() );
    private static Session session = null;
    private static Transaction transaction = null;
    private static Query query = null;

    public static void consultarDados() {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            buscaPorTabelaEId( Bandeira.class,1 );
            buscaPorTabelaEId( Cartao.class,10 );

            buscaPorTabela( Bandeira.class );
            buscaPorTabela( Cartao.class );
            buscaPorTabela( Cliente.class );
            buscaPorTabela( Credenciador.class );
            buscaPorTabela( Lancamento.class );
            buscaPorTabela( Emissor.class );
            buscaPorTabela( Loja.class );

        } catch ( Exception e ) {
            if (transaction != null)
                transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            if (session != null)
                session.close();
        }
    }

    private static void buscaPorTabelaEId( Class classe, Integer id ){
        query = session.createQuery( String.format("from %s where id = :id", classe.getSimpleName()) );
        query.setParameter("id", id);

        Object objeto = query.uniqueResult();

        if(objeto != null){
            System.out.println( objeto + "\n");
        }else{
            System.out.println("Não há registro na tabela " + classe + " com ID solicitado!\n");
        }
    }

    private static void buscaPorTabela( Class classe ){
        System.out.println();
        query = session.createQuery( String.format("select e from %s e", classe.getSimpleName()) );
        List list = query.list();
        list.forEach(System.out::println);
    }
}
