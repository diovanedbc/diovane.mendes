package br.com.dbc.service;

import br.com.dbc.app.HibernateUtil;
import br.com.dbc.app.Main;
import br.com.dbc.model.*;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Inserir {

    private static final Logger LOG = Logger.getLogger( Main.class.getName() );

    public static void inserindoDados(){
        Session session = null;
        Transaction transaction = null;

        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Credenciador credenciador = new Credenciador("Vilmar");
            Loja loja = new Loja("Renner");
            loja.pushCredenciadores( credenciador );

            Bandeira bandeira = new Bandeira("Visa", 5.0);

            Emissor emissor = new Emissor("Santander", 5.0);

            Cliente cliente = new Cliente("Diovane");

            Cartao cartao = new Cartao("oi", cliente, bandeira, emissor, LocalDate.of(2024, 3, 3) );

            session.save( new Lancamento(cartao, loja,  "Bermuda", 149.90, LocalDate.of(2019, 03, 8) ) );

            transaction.commit();
        } catch(Exception e){
            if( transaction != null )
                transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            if( session != null)
                session.close();
        }
    }
}
