package br.com.dbc.model;

import javax.persistence.*;
import java.util.Date;
import java.time.LocalDate;

@Entity
@Table(name = "LANCAMENTOS")
public class Lancamento {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "lancamento_seq", sequenceName = "lancamento_seq")
    @GeneratedValue(generator = "lancamento_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    private Cartao cartao;

    @ManyToOne(cascade =  CascadeType.ALL)
    private Loja loja;

    private String descricao;

    private double valor;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "DATA_COMPRA")
    private Date dataCompraDate;

    private transient LocalDate dataCompra;

    public Lancamento() {

    }

    public Lancamento(Cartao cartao,
                      Loja loja,
                      String descricao,
                      double valor,
                      LocalDate dataCompra) {
        this.cartao = cartao;
        this.loja = loja;
        this.descricao = descricao;
        this.valor = valor;
        setDataCompra(dataCompra);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public LocalDate getDataCompra() {
        return ((java.sql.Date)dataCompraDate).toLocalDate();
    }

    public void setDataCompra(LocalDate dataCompra) {
        this.dataCompra = dataCompra;  dataCompraDate = java.sql.Date.valueOf(dataCompra);
    }

    @Override
    public String toString() {
        return "Lancamento{" +
                "id=" + id +
                ", cartao=" + cartao +
                ", loja=" + loja +
                ", descricao='" + descricao + '\'' +
                ", valor=" + valor +
                ", dataCompraDate=" + dataCompraDate +
                ", dataCompra=" + dataCompra +
                '}';
    }
}
